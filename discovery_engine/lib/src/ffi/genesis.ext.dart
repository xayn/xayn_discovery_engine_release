
import 'dart:ffi' show NativeApi;
import 'dart:ffi' as ffi;

import 'package:async_bindgen_dart_utils/async_bindgen_dart_utils.dart'
    show CouldNotInitializeDartApiError, FfiCompleterRegistry;
// ignore: always_use_package_imports
import './genesis.ffigen.dart';

class XaynDiscoveryEngineAsyncFfi {
    final XaynDiscoveryEngineBindingsFfi _inner;

    XaynDiscoveryEngineAsyncFfi(this._inner) {
        final status = _inner.async_bindgen_dart_init_api__xayn_discovery_engine_async_ffi(NativeApi.initializeApiDLData);
        if (status != 1) {
            throw CouldNotInitializeDartApiError();
        }
    }

        Future<ffi.Pointer<RustResultVecDocumentString>> searchByQuery(
                ffi.Pointer<RustSharedEngine> engine,
                ffi.Pointer<RustString> query,
                int page,
                int page_size,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__search_by_query,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__search_by_query(
                    engine,
                    query,
                    page,
                    page_size,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVecDocumentString>> searchByTopic(
                ffi.Pointer<RustSharedEngine> engine,
                ffi.Pointer<RustString> topic,
                int page,
                int page_size,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__search_by_topic,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__search_by_topic(
                    engine,
                    topic,
                    page,
                    page_size,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVecTrendingTopicString>> trendingTopics(
                ffi.Pointer<RustSharedEngine> engine,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__trending_topics,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__trending_topics(
                    engine,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVecDocumentString>> getFeedDocuments(
                ffi.Pointer<RustSharedEngine> engine,
                ffi.Pointer<RustVecHistoricDocument> history,
                ffi.Pointer<RustVecWeightedSource> sources,
                int max_documents,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__get_feed_documents,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__get_feed_documents(
                    engine,
                    history,
                    sources,
                    max_documents,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVoidString>> userReacted(
                ffi.Pointer<RustSharedEngine> engine,
                ffi.Pointer<RustVecHistoricDocument> history,
                ffi.Pointer<RustVecWeightedSource> sources,
                ffi.Pointer<RustUserReacted> reacted,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__user_reacted,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__user_reacted(
                    engine,
                    history,
                    sources,
                    reacted,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVecDocumentString>> deepSearch(
                ffi.Pointer<RustSharedEngine> engine,
                ffi.Pointer<RustString> term,
                ffi.Pointer<RustMarket> market,
                ffi.Pointer<RustEmbedding> embedding,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__deep_search,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__deep_search(
                    engine,
                    term,
                    market,
                    embedding,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVoidString>> setTrustedSources(
                ffi.Pointer<RustSharedEngine> engine,
                ffi.Pointer<RustVecHistoricDocument> history,
                ffi.Pointer<RustVecWeightedSource> sources,
                ffi.Pointer<RustVecString> trusted,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__set_trusted_sources,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__set_trusted_sources(
                    engine,
                    history,
                    sources,
                    trusted,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVoidString>> setExcludedSources(
                ffi.Pointer<RustSharedEngine> engine,
                ffi.Pointer<RustVecHistoricDocument> history,
                ffi.Pointer<RustVecWeightedSource> sources,
                ffi.Pointer<RustVecString> excluded,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__set_excluded_sources,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__set_excluded_sources(
                    engine,
                    history,
                    sources,
                    excluded,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<void> dispose(
                ffi.Pointer<RustSharedEngine> engine,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__dispose,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__dispose(
                    engine,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultSharedEngineString>> initialize(
                ffi.Pointer<RustInitConfig> config,
                ffi.Pointer<RustVecU8> state,
                ffi.Pointer<RustVecHistoricDocument> history,
                ffi.Pointer<RustVecWeightedSource> sources,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__initialize,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__initialize(
                    config,
                    state,
                    history,
                    sources,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVoidString>> timeSpent(
                ffi.Pointer<RustSharedEngine> engine,
                ffi.Pointer<RustTimeSpent> time_spent,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__time_spent,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__time_spent(
                    engine,
                    time_spent,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVecU8String>> serialize(
                ffi.Pointer<RustSharedEngine> engine,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__serialize,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__serialize(
                    engine,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVoidString>> setMarkets(
                ffi.Pointer<RustSharedEngine> engine,
                ffi.Pointer<RustVecMarket> markets,
                ffi.Pointer<RustVecHistoricDocument> history,
                ffi.Pointer<RustVecWeightedSource> sources,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__set_markets,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__set_markets(
                    engine,
                    markets,
                    history,
                    sources,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
        Future<ffi.Pointer<RustResultVoidString>> resetAi(
                ffi.Pointer<RustSharedEngine> engine,
        ) {
            final setup = FfiCompleterRegistry.newCompleter(
                extractor: _inner.async_bindgen_dart_return__xayn_discovery_engine_async_ffi__reset_ai,
            );
            final callOk = _inner.async_bindgen_dart_call__xayn_discovery_engine_async_ffi__reset_ai(
                    engine,
                setup.portId,
                setup.completerId,
            );
            if (callOk == 0) {
                //TODO
                throw Exception('failed to setup callbacks');
            }
            return setup.future;
        }
}
