// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trending_topic.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TrendingTopic _$$_TrendingTopicFromJson(Map json) => _$_TrendingTopic(
      name: json['name'] as String,
      query: json['query'] as String,
      image: json['image'] == null ? null : Uri.parse(json['image'] as String),
    );

Map<String, dynamic> _$$_TrendingTopicToJson(_$_TrendingTopic instance) =>
    <String, dynamic>{
      'name': instance.name,
      'query': instance.query,
      'image': instance.image?.toString(),
    };
