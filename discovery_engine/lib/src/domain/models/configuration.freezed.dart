// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'configuration.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Configuration _$ConfigurationFromJson(Map<String, dynamic> json) {
  return _Configuration.fromJson(json);
}

/// @nodoc
class _$ConfigurationTearOff {
  const _$ConfigurationTearOff();

  _Configuration call(
      {required String apiKey,
      required String apiBaseUrl,
      required String assetsUrl,
      required String newsProviderPath,
      required String headlinesProviderPath,
      required int maxItemsPerFeedBatch,
      required int maxItemsPerSearchBatch,
      required String applicationDirectoryPath,
      required Set<FeedMarket> feedMarkets,
      required Manifest manifest,
      String? logFile}) {
    return _Configuration(
      apiKey: apiKey,
      apiBaseUrl: apiBaseUrl,
      assetsUrl: assetsUrl,
      newsProviderPath: newsProviderPath,
      headlinesProviderPath: headlinesProviderPath,
      maxItemsPerFeedBatch: maxItemsPerFeedBatch,
      maxItemsPerSearchBatch: maxItemsPerSearchBatch,
      applicationDirectoryPath: applicationDirectoryPath,
      feedMarkets: feedMarkets,
      manifest: manifest,
      logFile: logFile,
    );
  }

  Configuration fromJson(Map<String, Object?> json) {
    return Configuration.fromJson(json);
  }
}

/// @nodoc
const $Configuration = _$ConfigurationTearOff();

/// @nodoc
mixin _$Configuration {
  String get apiKey => throw _privateConstructorUsedError;
  String get apiBaseUrl => throw _privateConstructorUsedError;
  String get assetsUrl => throw _privateConstructorUsedError;
  String get newsProviderPath => throw _privateConstructorUsedError;
  String get headlinesProviderPath => throw _privateConstructorUsedError;
  int get maxItemsPerFeedBatch => throw _privateConstructorUsedError;
  int get maxItemsPerSearchBatch => throw _privateConstructorUsedError;
  String get applicationDirectoryPath => throw _privateConstructorUsedError;
  Set<FeedMarket> get feedMarkets => throw _privateConstructorUsedError;
  Manifest get manifest => throw _privateConstructorUsedError;
  String? get logFile => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ConfigurationCopyWith<Configuration> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ConfigurationCopyWith<$Res> {
  factory $ConfigurationCopyWith(
          Configuration value, $Res Function(Configuration) then) =
      _$ConfigurationCopyWithImpl<$Res>;
  $Res call(
      {String apiKey,
      String apiBaseUrl,
      String assetsUrl,
      String newsProviderPath,
      String headlinesProviderPath,
      int maxItemsPerFeedBatch,
      int maxItemsPerSearchBatch,
      String applicationDirectoryPath,
      Set<FeedMarket> feedMarkets,
      Manifest manifest,
      String? logFile});
}

/// @nodoc
class _$ConfigurationCopyWithImpl<$Res>
    implements $ConfigurationCopyWith<$Res> {
  _$ConfigurationCopyWithImpl(this._value, this._then);

  final Configuration _value;
  // ignore: unused_field
  final $Res Function(Configuration) _then;

  @override
  $Res call({
    Object? apiKey = freezed,
    Object? apiBaseUrl = freezed,
    Object? assetsUrl = freezed,
    Object? newsProviderPath = freezed,
    Object? headlinesProviderPath = freezed,
    Object? maxItemsPerFeedBatch = freezed,
    Object? maxItemsPerSearchBatch = freezed,
    Object? applicationDirectoryPath = freezed,
    Object? feedMarkets = freezed,
    Object? manifest = freezed,
    Object? logFile = freezed,
  }) {
    return _then(_value.copyWith(
      apiKey: apiKey == freezed
          ? _value.apiKey
          : apiKey // ignore: cast_nullable_to_non_nullable
              as String,
      apiBaseUrl: apiBaseUrl == freezed
          ? _value.apiBaseUrl
          : apiBaseUrl // ignore: cast_nullable_to_non_nullable
              as String,
      assetsUrl: assetsUrl == freezed
          ? _value.assetsUrl
          : assetsUrl // ignore: cast_nullable_to_non_nullable
              as String,
      newsProviderPath: newsProviderPath == freezed
          ? _value.newsProviderPath
          : newsProviderPath // ignore: cast_nullable_to_non_nullable
              as String,
      headlinesProviderPath: headlinesProviderPath == freezed
          ? _value.headlinesProviderPath
          : headlinesProviderPath // ignore: cast_nullable_to_non_nullable
              as String,
      maxItemsPerFeedBatch: maxItemsPerFeedBatch == freezed
          ? _value.maxItemsPerFeedBatch
          : maxItemsPerFeedBatch // ignore: cast_nullable_to_non_nullable
              as int,
      maxItemsPerSearchBatch: maxItemsPerSearchBatch == freezed
          ? _value.maxItemsPerSearchBatch
          : maxItemsPerSearchBatch // ignore: cast_nullable_to_non_nullable
              as int,
      applicationDirectoryPath: applicationDirectoryPath == freezed
          ? _value.applicationDirectoryPath
          : applicationDirectoryPath // ignore: cast_nullable_to_non_nullable
              as String,
      feedMarkets: feedMarkets == freezed
          ? _value.feedMarkets
          : feedMarkets // ignore: cast_nullable_to_non_nullable
              as Set<FeedMarket>,
      manifest: manifest == freezed
          ? _value.manifest
          : manifest // ignore: cast_nullable_to_non_nullable
              as Manifest,
      logFile: logFile == freezed
          ? _value.logFile
          : logFile // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$ConfigurationCopyWith<$Res>
    implements $ConfigurationCopyWith<$Res> {
  factory _$ConfigurationCopyWith(
          _Configuration value, $Res Function(_Configuration) then) =
      __$ConfigurationCopyWithImpl<$Res>;
  @override
  $Res call(
      {String apiKey,
      String apiBaseUrl,
      String assetsUrl,
      String newsProviderPath,
      String headlinesProviderPath,
      int maxItemsPerFeedBatch,
      int maxItemsPerSearchBatch,
      String applicationDirectoryPath,
      Set<FeedMarket> feedMarkets,
      Manifest manifest,
      String? logFile});
}

/// @nodoc
class __$ConfigurationCopyWithImpl<$Res>
    extends _$ConfigurationCopyWithImpl<$Res>
    implements _$ConfigurationCopyWith<$Res> {
  __$ConfigurationCopyWithImpl(
      _Configuration _value, $Res Function(_Configuration) _then)
      : super(_value, (v) => _then(v as _Configuration));

  @override
  _Configuration get _value => super._value as _Configuration;

  @override
  $Res call({
    Object? apiKey = freezed,
    Object? apiBaseUrl = freezed,
    Object? assetsUrl = freezed,
    Object? newsProviderPath = freezed,
    Object? headlinesProviderPath = freezed,
    Object? maxItemsPerFeedBatch = freezed,
    Object? maxItemsPerSearchBatch = freezed,
    Object? applicationDirectoryPath = freezed,
    Object? feedMarkets = freezed,
    Object? manifest = freezed,
    Object? logFile = freezed,
  }) {
    return _then(_Configuration(
      apiKey: apiKey == freezed
          ? _value.apiKey
          : apiKey // ignore: cast_nullable_to_non_nullable
              as String,
      apiBaseUrl: apiBaseUrl == freezed
          ? _value.apiBaseUrl
          : apiBaseUrl // ignore: cast_nullable_to_non_nullable
              as String,
      assetsUrl: assetsUrl == freezed
          ? _value.assetsUrl
          : assetsUrl // ignore: cast_nullable_to_non_nullable
              as String,
      newsProviderPath: newsProviderPath == freezed
          ? _value.newsProviderPath
          : newsProviderPath // ignore: cast_nullable_to_non_nullable
              as String,
      headlinesProviderPath: headlinesProviderPath == freezed
          ? _value.headlinesProviderPath
          : headlinesProviderPath // ignore: cast_nullable_to_non_nullable
              as String,
      maxItemsPerFeedBatch: maxItemsPerFeedBatch == freezed
          ? _value.maxItemsPerFeedBatch
          : maxItemsPerFeedBatch // ignore: cast_nullable_to_non_nullable
              as int,
      maxItemsPerSearchBatch: maxItemsPerSearchBatch == freezed
          ? _value.maxItemsPerSearchBatch
          : maxItemsPerSearchBatch // ignore: cast_nullable_to_non_nullable
              as int,
      applicationDirectoryPath: applicationDirectoryPath == freezed
          ? _value.applicationDirectoryPath
          : applicationDirectoryPath // ignore: cast_nullable_to_non_nullable
              as String,
      feedMarkets: feedMarkets == freezed
          ? _value.feedMarkets
          : feedMarkets // ignore: cast_nullable_to_non_nullable
              as Set<FeedMarket>,
      manifest: manifest == freezed
          ? _value.manifest
          : manifest // ignore: cast_nullable_to_non_nullable
              as Manifest,
      logFile: logFile == freezed
          ? _value.logFile
          : logFile // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Configuration extends _Configuration {
  const _$_Configuration(
      {required this.apiKey,
      required this.apiBaseUrl,
      required this.assetsUrl,
      required this.newsProviderPath,
      required this.headlinesProviderPath,
      required this.maxItemsPerFeedBatch,
      required this.maxItemsPerSearchBatch,
      required this.applicationDirectoryPath,
      required this.feedMarkets,
      required this.manifest,
      this.logFile})
      : assert(feedMarkets.length > 0),
        super._();

  factory _$_Configuration.fromJson(Map<String, dynamic> json) =>
      _$$_ConfigurationFromJson(json);

  @override
  final String apiKey;
  @override
  final String apiBaseUrl;
  @override
  final String assetsUrl;
  @override
  final String newsProviderPath;
  @override
  final String headlinesProviderPath;
  @override
  final int maxItemsPerFeedBatch;
  @override
  final int maxItemsPerSearchBatch;
  @override
  final String applicationDirectoryPath;
  @override
  final Set<FeedMarket> feedMarkets;
  @override
  final Manifest manifest;
  @override
  final String? logFile;

  @override
  String toString() {
    return 'Configuration(apiKey: $apiKey, apiBaseUrl: $apiBaseUrl, assetsUrl: $assetsUrl, newsProviderPath: $newsProviderPath, headlinesProviderPath: $headlinesProviderPath, maxItemsPerFeedBatch: $maxItemsPerFeedBatch, maxItemsPerSearchBatch: $maxItemsPerSearchBatch, applicationDirectoryPath: $applicationDirectoryPath, feedMarkets: $feedMarkets, manifest: $manifest, logFile: $logFile)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Configuration &&
            const DeepCollectionEquality().equals(other.apiKey, apiKey) &&
            const DeepCollectionEquality()
                .equals(other.apiBaseUrl, apiBaseUrl) &&
            const DeepCollectionEquality().equals(other.assetsUrl, assetsUrl) &&
            const DeepCollectionEquality()
                .equals(other.newsProviderPath, newsProviderPath) &&
            const DeepCollectionEquality()
                .equals(other.headlinesProviderPath, headlinesProviderPath) &&
            const DeepCollectionEquality()
                .equals(other.maxItemsPerFeedBatch, maxItemsPerFeedBatch) &&
            const DeepCollectionEquality()
                .equals(other.maxItemsPerSearchBatch, maxItemsPerSearchBatch) &&
            const DeepCollectionEquality().equals(
                other.applicationDirectoryPath, applicationDirectoryPath) &&
            const DeepCollectionEquality()
                .equals(other.feedMarkets, feedMarkets) &&
            const DeepCollectionEquality().equals(other.manifest, manifest) &&
            const DeepCollectionEquality().equals(other.logFile, logFile));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(apiKey),
      const DeepCollectionEquality().hash(apiBaseUrl),
      const DeepCollectionEquality().hash(assetsUrl),
      const DeepCollectionEquality().hash(newsProviderPath),
      const DeepCollectionEquality().hash(headlinesProviderPath),
      const DeepCollectionEquality().hash(maxItemsPerFeedBatch),
      const DeepCollectionEquality().hash(maxItemsPerSearchBatch),
      const DeepCollectionEquality().hash(applicationDirectoryPath),
      const DeepCollectionEquality().hash(feedMarkets),
      const DeepCollectionEquality().hash(manifest),
      const DeepCollectionEquality().hash(logFile));

  @JsonKey(ignore: true)
  @override
  _$ConfigurationCopyWith<_Configuration> get copyWith =>
      __$ConfigurationCopyWithImpl<_Configuration>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ConfigurationToJson(this);
  }
}

abstract class _Configuration extends Configuration {
  const factory _Configuration(
      {required String apiKey,
      required String apiBaseUrl,
      required String assetsUrl,
      required String newsProviderPath,
      required String headlinesProviderPath,
      required int maxItemsPerFeedBatch,
      required int maxItemsPerSearchBatch,
      required String applicationDirectoryPath,
      required Set<FeedMarket> feedMarkets,
      required Manifest manifest,
      String? logFile}) = _$_Configuration;
  const _Configuration._() : super._();

  factory _Configuration.fromJson(Map<String, dynamic> json) =
      _$_Configuration.fromJson;

  @override
  String get apiKey;
  @override
  String get apiBaseUrl;
  @override
  String get assetsUrl;
  @override
  String get newsProviderPath;
  @override
  String get headlinesProviderPath;
  @override
  int get maxItemsPerFeedBatch;
  @override
  int get maxItemsPerSearchBatch;
  @override
  String get applicationDirectoryPath;
  @override
  Set<FeedMarket> get feedMarkets;
  @override
  Manifest get manifest;
  @override
  String? get logFile;
  @override
  @JsonKey(ignore: true)
  _$ConfigurationCopyWith<_Configuration> get copyWith =>
      throw _privateConstructorUsedError;
}
