// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'source.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AvailableSource _$AvailableSourceFromJson(Map<String, dynamic> json) {
  return _AvailableSource.fromJson(json);
}

/// @nodoc
class _$AvailableSourceTearOff {
  const _$AvailableSourceTearOff();

  _AvailableSource call({required String name, required String domain}) {
    return _AvailableSource(
      name: name,
      domain: domain,
    );
  }

  AvailableSource fromJson(Map<String, Object?> json) {
    return AvailableSource.fromJson(json);
  }
}

/// @nodoc
const $AvailableSource = _$AvailableSourceTearOff();

/// @nodoc
mixin _$AvailableSource {
  String get name => throw _privateConstructorUsedError;
  String get domain => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AvailableSourceCopyWith<AvailableSource> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AvailableSourceCopyWith<$Res> {
  factory $AvailableSourceCopyWith(
          AvailableSource value, $Res Function(AvailableSource) then) =
      _$AvailableSourceCopyWithImpl<$Res>;
  $Res call({String name, String domain});
}

/// @nodoc
class _$AvailableSourceCopyWithImpl<$Res>
    implements $AvailableSourceCopyWith<$Res> {
  _$AvailableSourceCopyWithImpl(this._value, this._then);

  final AvailableSource _value;
  // ignore: unused_field
  final $Res Function(AvailableSource) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? domain = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      domain: domain == freezed
          ? _value.domain
          : domain // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$AvailableSourceCopyWith<$Res>
    implements $AvailableSourceCopyWith<$Res> {
  factory _$AvailableSourceCopyWith(
          _AvailableSource value, $Res Function(_AvailableSource) then) =
      __$AvailableSourceCopyWithImpl<$Res>;
  @override
  $Res call({String name, String domain});
}

/// @nodoc
class __$AvailableSourceCopyWithImpl<$Res>
    extends _$AvailableSourceCopyWithImpl<$Res>
    implements _$AvailableSourceCopyWith<$Res> {
  __$AvailableSourceCopyWithImpl(
      _AvailableSource _value, $Res Function(_AvailableSource) _then)
      : super(_value, (v) => _then(v as _AvailableSource));

  @override
  _AvailableSource get _value => super._value as _AvailableSource;

  @override
  $Res call({
    Object? name = freezed,
    Object? domain = freezed,
  }) {
    return _then(_AvailableSource(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      domain: domain == freezed
          ? _value.domain
          : domain // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AvailableSource implements _AvailableSource {
  _$_AvailableSource({required this.name, required this.domain})
      : assert(name.isNotEmpty),
        assert(domain.isNotEmpty);

  factory _$_AvailableSource.fromJson(Map<String, dynamic> json) =>
      _$$_AvailableSourceFromJson(json);

  @override
  final String name;
  @override
  final String domain;

  @override
  String toString() {
    return 'AvailableSource(name: $name, domain: $domain)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AvailableSource &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.domain, domain));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(domain));

  @JsonKey(ignore: true)
  @override
  _$AvailableSourceCopyWith<_AvailableSource> get copyWith =>
      __$AvailableSourceCopyWithImpl<_AvailableSource>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AvailableSourceToJson(this);
  }
}

abstract class _AvailableSource implements AvailableSource {
  factory _AvailableSource({required String name, required String domain}) =
      _$_AvailableSource;

  factory _AvailableSource.fromJson(Map<String, dynamic> json) =
      _$_AvailableSource.fromJson;

  @override
  String get name;
  @override
  String get domain;
  @override
  @JsonKey(ignore: true)
  _$AvailableSourceCopyWith<_AvailableSource> get copyWith =>
      throw _privateConstructorUsedError;
}
