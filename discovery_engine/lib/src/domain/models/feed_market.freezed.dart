// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'feed_market.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FeedMarket _$FeedMarketFromJson(Map<String, dynamic> json) {
  return _FeedMarket.fromJson(json);
}

/// @nodoc
class _$FeedMarketTearOff {
  const _$FeedMarketTearOff();

  _FeedMarket call(
      {@HiveField(1) required String langCode,
      @HiveField(0) required String countryCode}) {
    return _FeedMarket(
      langCode: langCode,
      countryCode: countryCode,
    );
  }

  FeedMarket fromJson(Map<String, Object?> json) {
    return FeedMarket.fromJson(json);
  }
}

/// @nodoc
const $FeedMarket = _$FeedMarketTearOff();

/// @nodoc
mixin _$FeedMarket {
  /// Language code as per ISO ISO 639-1 definition.
  @HiveField(1)
  String get langCode => throw _privateConstructorUsedError;

  /// Country code as per ISO 3166-1 alpha-2 definition.
  @HiveField(0)
  String get countryCode => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FeedMarketCopyWith<FeedMarket> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FeedMarketCopyWith<$Res> {
  factory $FeedMarketCopyWith(
          FeedMarket value, $Res Function(FeedMarket) then) =
      _$FeedMarketCopyWithImpl<$Res>;
  $Res call({@HiveField(1) String langCode, @HiveField(0) String countryCode});
}

/// @nodoc
class _$FeedMarketCopyWithImpl<$Res> implements $FeedMarketCopyWith<$Res> {
  _$FeedMarketCopyWithImpl(this._value, this._then);

  final FeedMarket _value;
  // ignore: unused_field
  final $Res Function(FeedMarket) _then;

  @override
  $Res call({
    Object? langCode = freezed,
    Object? countryCode = freezed,
  }) {
    return _then(_value.copyWith(
      langCode: langCode == freezed
          ? _value.langCode
          : langCode // ignore: cast_nullable_to_non_nullable
              as String,
      countryCode: countryCode == freezed
          ? _value.countryCode
          : countryCode // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$FeedMarketCopyWith<$Res> implements $FeedMarketCopyWith<$Res> {
  factory _$FeedMarketCopyWith(
          _FeedMarket value, $Res Function(_FeedMarket) then) =
      __$FeedMarketCopyWithImpl<$Res>;
  @override
  $Res call({@HiveField(1) String langCode, @HiveField(0) String countryCode});
}

/// @nodoc
class __$FeedMarketCopyWithImpl<$Res> extends _$FeedMarketCopyWithImpl<$Res>
    implements _$FeedMarketCopyWith<$Res> {
  __$FeedMarketCopyWithImpl(
      _FeedMarket _value, $Res Function(_FeedMarket) _then)
      : super(_value, (v) => _then(v as _FeedMarket));

  @override
  _FeedMarket get _value => super._value as _FeedMarket;

  @override
  $Res call({
    Object? langCode = freezed,
    Object? countryCode = freezed,
  }) {
    return _then(_FeedMarket(
      langCode: langCode == freezed
          ? _value.langCode
          : langCode // ignore: cast_nullable_to_non_nullable
              as String,
      countryCode: countryCode == freezed
          ? _value.countryCode
          : countryCode // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@HiveType(typeId: feedMarketTypeId)
class _$_FeedMarket implements _FeedMarket {
  const _$_FeedMarket(
      {@HiveField(1) required this.langCode,
      @HiveField(0) required this.countryCode});

  factory _$_FeedMarket.fromJson(Map<String, dynamic> json) =>
      _$$_FeedMarketFromJson(json);

  @override

  /// Language code as per ISO ISO 639-1 definition.
  @HiveField(1)
  final String langCode;
  @override

  /// Country code as per ISO 3166-1 alpha-2 definition.
  @HiveField(0)
  final String countryCode;

  @override
  String toString() {
    return 'FeedMarket(langCode: $langCode, countryCode: $countryCode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _FeedMarket &&
            const DeepCollectionEquality().equals(other.langCode, langCode) &&
            const DeepCollectionEquality()
                .equals(other.countryCode, countryCode));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(langCode),
      const DeepCollectionEquality().hash(countryCode));

  @JsonKey(ignore: true)
  @override
  _$FeedMarketCopyWith<_FeedMarket> get copyWith =>
      __$FeedMarketCopyWithImpl<_FeedMarket>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FeedMarketToJson(this);
  }
}

abstract class _FeedMarket implements FeedMarket {
  const factory _FeedMarket(
      {@HiveField(1) required String langCode,
      @HiveField(0) required String countryCode}) = _$_FeedMarket;

  factory _FeedMarket.fromJson(Map<String, dynamic> json) =
      _$_FeedMarket.fromJson;

  @override

  /// Language code as per ISO ISO 639-1 definition.
  @HiveField(1)
  String get langCode;
  @override

  /// Country code as per ISO 3166-1 alpha-2 definition.
  @HiveField(0)
  String get countryCode;
  @override
  @JsonKey(ignore: true)
  _$FeedMarketCopyWith<_FeedMarket> get copyWith =>
      throw _privateConstructorUsedError;
}
