// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'news_resource.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

NewsResource _$NewsResourceFromJson(Map<String, dynamic> json) {
  return _NewsResource.fromJson(json);
}

/// @nodoc
class _$NewsResourceTearOff {
  const _$NewsResourceTearOff();

  _NewsResource call(
      {@HiveField(0) required String title,
      @HiveField(1) required String snippet,
      @HiveField(2) required Uri url,
      @HiveField(3) required Source sourceDomain,
      @HiveField(4) required Uri? image,
      @HiveField(5) required DateTime datePublished,
      @HiveField(6) required int rank,
      @HiveField(7) required double? score,
      @HiveField(8) required String country,
      @HiveField(9) required String language,
      @HiveField(10) required String topic}) {
    return _NewsResource(
      title: title,
      snippet: snippet,
      url: url,
      sourceDomain: sourceDomain,
      image: image,
      datePublished: datePublished,
      rank: rank,
      score: score,
      country: country,
      language: language,
      topic: topic,
    );
  }

  NewsResource fromJson(Map<String, Object?> json) {
    return NewsResource.fromJson(json);
  }
}

/// @nodoc
const $NewsResource = _$NewsResourceTearOff();

/// @nodoc
mixin _$NewsResource {
  @HiveField(0)
  String get title => throw _privateConstructorUsedError;
  @HiveField(1)
  String get snippet => throw _privateConstructorUsedError;
  @HiveField(2)
  Uri get url => throw _privateConstructorUsedError;
  @HiveField(3)
  Source get sourceDomain => throw _privateConstructorUsedError;
  @HiveField(4)
  Uri? get image => throw _privateConstructorUsedError;
  @HiveField(5)
  DateTime get datePublished => throw _privateConstructorUsedError;
  @HiveField(6)
  int get rank => throw _privateConstructorUsedError;
  @HiveField(7)
  double? get score => throw _privateConstructorUsedError;
  @HiveField(8)
  String get country => throw _privateConstructorUsedError;
  @HiveField(9)
  String get language => throw _privateConstructorUsedError;
  @HiveField(10)
  String get topic => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NewsResourceCopyWith<NewsResource> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NewsResourceCopyWith<$Res> {
  factory $NewsResourceCopyWith(
          NewsResource value, $Res Function(NewsResource) then) =
      _$NewsResourceCopyWithImpl<$Res>;
  $Res call(
      {@HiveField(0) String title,
      @HiveField(1) String snippet,
      @HiveField(2) Uri url,
      @HiveField(3) Source sourceDomain,
      @HiveField(4) Uri? image,
      @HiveField(5) DateTime datePublished,
      @HiveField(6) int rank,
      @HiveField(7) double? score,
      @HiveField(8) String country,
      @HiveField(9) String language,
      @HiveField(10) String topic});
}

/// @nodoc
class _$NewsResourceCopyWithImpl<$Res> implements $NewsResourceCopyWith<$Res> {
  _$NewsResourceCopyWithImpl(this._value, this._then);

  final NewsResource _value;
  // ignore: unused_field
  final $Res Function(NewsResource) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? snippet = freezed,
    Object? url = freezed,
    Object? sourceDomain = freezed,
    Object? image = freezed,
    Object? datePublished = freezed,
    Object? rank = freezed,
    Object? score = freezed,
    Object? country = freezed,
    Object? language = freezed,
    Object? topic = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      snippet: snippet == freezed
          ? _value.snippet
          : snippet // ignore: cast_nullable_to_non_nullable
              as String,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as Uri,
      sourceDomain: sourceDomain == freezed
          ? _value.sourceDomain
          : sourceDomain // ignore: cast_nullable_to_non_nullable
              as Source,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as Uri?,
      datePublished: datePublished == freezed
          ? _value.datePublished
          : datePublished // ignore: cast_nullable_to_non_nullable
              as DateTime,
      rank: rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as int,
      score: score == freezed
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as double?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      language: language == freezed
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      topic: topic == freezed
          ? _value.topic
          : topic // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$NewsResourceCopyWith<$Res>
    implements $NewsResourceCopyWith<$Res> {
  factory _$NewsResourceCopyWith(
          _NewsResource value, $Res Function(_NewsResource) then) =
      __$NewsResourceCopyWithImpl<$Res>;
  @override
  $Res call(
      {@HiveField(0) String title,
      @HiveField(1) String snippet,
      @HiveField(2) Uri url,
      @HiveField(3) Source sourceDomain,
      @HiveField(4) Uri? image,
      @HiveField(5) DateTime datePublished,
      @HiveField(6) int rank,
      @HiveField(7) double? score,
      @HiveField(8) String country,
      @HiveField(9) String language,
      @HiveField(10) String topic});
}

/// @nodoc
class __$NewsResourceCopyWithImpl<$Res> extends _$NewsResourceCopyWithImpl<$Res>
    implements _$NewsResourceCopyWith<$Res> {
  __$NewsResourceCopyWithImpl(
      _NewsResource _value, $Res Function(_NewsResource) _then)
      : super(_value, (v) => _then(v as _NewsResource));

  @override
  _NewsResource get _value => super._value as _NewsResource;

  @override
  $Res call({
    Object? title = freezed,
    Object? snippet = freezed,
    Object? url = freezed,
    Object? sourceDomain = freezed,
    Object? image = freezed,
    Object? datePublished = freezed,
    Object? rank = freezed,
    Object? score = freezed,
    Object? country = freezed,
    Object? language = freezed,
    Object? topic = freezed,
  }) {
    return _then(_NewsResource(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      snippet: snippet == freezed
          ? _value.snippet
          : snippet // ignore: cast_nullable_to_non_nullable
              as String,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as Uri,
      sourceDomain: sourceDomain == freezed
          ? _value.sourceDomain
          : sourceDomain // ignore: cast_nullable_to_non_nullable
              as Source,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as Uri?,
      datePublished: datePublished == freezed
          ? _value.datePublished
          : datePublished // ignore: cast_nullable_to_non_nullable
              as DateTime,
      rank: rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as int,
      score: score == freezed
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as double?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      language: language == freezed
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      topic: topic == freezed
          ? _value.topic
          : topic // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@HiveType(typeId: newsResourceTypeId)
class _$_NewsResource implements _NewsResource {
  const _$_NewsResource(
      {@HiveField(0) required this.title,
      @HiveField(1) required this.snippet,
      @HiveField(2) required this.url,
      @HiveField(3) required this.sourceDomain,
      @HiveField(4) required this.image,
      @HiveField(5) required this.datePublished,
      @HiveField(6) required this.rank,
      @HiveField(7) required this.score,
      @HiveField(8) required this.country,
      @HiveField(9) required this.language,
      @HiveField(10) required this.topic});

  factory _$_NewsResource.fromJson(Map<String, dynamic> json) =>
      _$$_NewsResourceFromJson(json);

  @override
  @HiveField(0)
  final String title;
  @override
  @HiveField(1)
  final String snippet;
  @override
  @HiveField(2)
  final Uri url;
  @override
  @HiveField(3)
  final Source sourceDomain;
  @override
  @HiveField(4)
  final Uri? image;
  @override
  @HiveField(5)
  final DateTime datePublished;
  @override
  @HiveField(6)
  final int rank;
  @override
  @HiveField(7)
  final double? score;
  @override
  @HiveField(8)
  final String country;
  @override
  @HiveField(9)
  final String language;
  @override
  @HiveField(10)
  final String topic;

  @override
  String toString() {
    return 'NewsResource(title: $title, snippet: $snippet, url: $url, sourceDomain: $sourceDomain, image: $image, datePublished: $datePublished, rank: $rank, score: $score, country: $country, language: $language, topic: $topic)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _NewsResource &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.snippet, snippet) &&
            const DeepCollectionEquality().equals(other.url, url) &&
            const DeepCollectionEquality()
                .equals(other.sourceDomain, sourceDomain) &&
            const DeepCollectionEquality().equals(other.image, image) &&
            const DeepCollectionEquality()
                .equals(other.datePublished, datePublished) &&
            const DeepCollectionEquality().equals(other.rank, rank) &&
            const DeepCollectionEquality().equals(other.score, score) &&
            const DeepCollectionEquality().equals(other.country, country) &&
            const DeepCollectionEquality().equals(other.language, language) &&
            const DeepCollectionEquality().equals(other.topic, topic));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(snippet),
      const DeepCollectionEquality().hash(url),
      const DeepCollectionEquality().hash(sourceDomain),
      const DeepCollectionEquality().hash(image),
      const DeepCollectionEquality().hash(datePublished),
      const DeepCollectionEquality().hash(rank),
      const DeepCollectionEquality().hash(score),
      const DeepCollectionEquality().hash(country),
      const DeepCollectionEquality().hash(language),
      const DeepCollectionEquality().hash(topic));

  @JsonKey(ignore: true)
  @override
  _$NewsResourceCopyWith<_NewsResource> get copyWith =>
      __$NewsResourceCopyWithImpl<_NewsResource>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_NewsResourceToJson(this);
  }
}

abstract class _NewsResource implements NewsResource {
  const factory _NewsResource(
      {@HiveField(0) required String title,
      @HiveField(1) required String snippet,
      @HiveField(2) required Uri url,
      @HiveField(3) required Source sourceDomain,
      @HiveField(4) required Uri? image,
      @HiveField(5) required DateTime datePublished,
      @HiveField(6) required int rank,
      @HiveField(7) required double? score,
      @HiveField(8) required String country,
      @HiveField(9) required String language,
      @HiveField(10) required String topic}) = _$_NewsResource;

  factory _NewsResource.fromJson(Map<String, dynamic> json) =
      _$_NewsResource.fromJson;

  @override
  @HiveField(0)
  String get title;
  @override
  @HiveField(1)
  String get snippet;
  @override
  @HiveField(2)
  Uri get url;
  @override
  @HiveField(3)
  Source get sourceDomain;
  @override
  @HiveField(4)
  Uri? get image;
  @override
  @HiveField(5)
  DateTime get datePublished;
  @override
  @HiveField(6)
  int get rank;
  @override
  @HiveField(7)
  double? get score;
  @override
  @HiveField(8)
  String get country;
  @override
  @HiveField(9)
  String get language;
  @override
  @HiveField(10)
  String get topic;
  @override
  @JsonKey(ignore: true)
  _$NewsResourceCopyWith<_NewsResource> get copyWith =>
      throw _privateConstructorUsedError;
}
