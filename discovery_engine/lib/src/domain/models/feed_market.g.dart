// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_market.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FeedMarketAdapter extends TypeAdapter<_$_FeedMarket> {
  @override
  final int typeId = 10;

  @override
  _$_FeedMarket read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return _$_FeedMarket(
      langCode: fields[1] as String,
      countryCode: fields[0] as String,
    );
  }

  @override
  void write(BinaryWriter writer, _$_FeedMarket obj) {
    writer
      ..writeByte(2)
      ..writeByte(1)
      ..write(obj.langCode)
      ..writeByte(0)
      ..write(obj.countryCode);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FeedMarketAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FeedMarket _$$_FeedMarketFromJson(Map json) => _$_FeedMarket(
      langCode: json['langCode'] as String,
      countryCode: json['countryCode'] as String,
    );

Map<String, dynamic> _$$_FeedMarketToJson(_$_FeedMarket instance) =>
    <String, dynamic>{
      'langCode': instance.langCode,
      'countryCode': instance.countryCode,
    };
