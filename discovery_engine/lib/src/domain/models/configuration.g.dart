// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'configuration.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Configuration _$$_ConfigurationFromJson(Map json) => _$_Configuration(
      apiKey: json['apiKey'] as String,
      apiBaseUrl: json['apiBaseUrl'] as String,
      assetsUrl: json['assetsUrl'] as String,
      newsProviderPath: json['newsProviderPath'] as String,
      headlinesProviderPath: json['headlinesProviderPath'] as String,
      maxItemsPerFeedBatch: json['maxItemsPerFeedBatch'] as int,
      maxItemsPerSearchBatch: json['maxItemsPerSearchBatch'] as int,
      applicationDirectoryPath: json['applicationDirectoryPath'] as String,
      feedMarkets: (json['feedMarkets'] as List<dynamic>)
          .map((e) => FeedMarket.fromJson(Map<String, Object?>.from(e as Map)))
          .toSet(),
      manifest:
          Manifest.fromJson(Map<String, Object?>.from(json['manifest'] as Map)),
      logFile: json['logFile'] as String?,
    );

Map<String, dynamic> _$$_ConfigurationToJson(_$_Configuration instance) =>
    <String, dynamic>{
      'apiKey': instance.apiKey,
      'apiBaseUrl': instance.apiBaseUrl,
      'assetsUrl': instance.assetsUrl,
      'newsProviderPath': instance.newsProviderPath,
      'headlinesProviderPath': instance.headlinesProviderPath,
      'maxItemsPerFeedBatch': instance.maxItemsPerFeedBatch,
      'maxItemsPerSearchBatch': instance.maxItemsPerSearchBatch,
      'applicationDirectoryPath': instance.applicationDirectoryPath,
      'feedMarkets': instance.feedMarkets.map((e) => e.toJson()).toList(),
      'manifest': instance.manifest.toJson(),
      'logFile': instance.logFile,
    };
