// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'source.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AvailableSource _$$_AvailableSourceFromJson(Map json) => _$_AvailableSource(
      name: json['name'] as String,
      domain: json['domain'] as String,
    );

Map<String, dynamic> _$$_AvailableSourceToJson(_$_AvailableSource instance) =>
    <String, dynamic>{
      'name': instance.name,
      'domain': instance.domain,
    };
