// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'trending_topic.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TrendingTopic _$TrendingTopicFromJson(Map<String, dynamic> json) {
  return _TrendingTopic.fromJson(json);
}

/// @nodoc
class _$TrendingTopicTearOff {
  const _$TrendingTopicTearOff();

  _TrendingTopic call(
      {required String name, required String query, required Uri? image}) {
    return _TrendingTopic(
      name: name,
      query: query,
      image: image,
    );
  }

  TrendingTopic fromJson(Map<String, Object?> json) {
    return TrendingTopic.fromJson(json);
  }
}

/// @nodoc
const $TrendingTopic = _$TrendingTopicTearOff();

/// @nodoc
mixin _$TrendingTopic {
  /// Description of a topic.
  String get name => throw _privateConstructorUsedError;

  /// Query that can/should be used to perform a new search.
  String get query => throw _privateConstructorUsedError;

  /// Image representation of a topic.
  Uri? get image => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TrendingTopicCopyWith<TrendingTopic> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrendingTopicCopyWith<$Res> {
  factory $TrendingTopicCopyWith(
          TrendingTopic value, $Res Function(TrendingTopic) then) =
      _$TrendingTopicCopyWithImpl<$Res>;
  $Res call({String name, String query, Uri? image});
}

/// @nodoc
class _$TrendingTopicCopyWithImpl<$Res>
    implements $TrendingTopicCopyWith<$Res> {
  _$TrendingTopicCopyWithImpl(this._value, this._then);

  final TrendingTopic _value;
  // ignore: unused_field
  final $Res Function(TrendingTopic) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? query = freezed,
    Object? image = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      query: query == freezed
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as Uri?,
    ));
  }
}

/// @nodoc
abstract class _$TrendingTopicCopyWith<$Res>
    implements $TrendingTopicCopyWith<$Res> {
  factory _$TrendingTopicCopyWith(
          _TrendingTopic value, $Res Function(_TrendingTopic) then) =
      __$TrendingTopicCopyWithImpl<$Res>;
  @override
  $Res call({String name, String query, Uri? image});
}

/// @nodoc
class __$TrendingTopicCopyWithImpl<$Res>
    extends _$TrendingTopicCopyWithImpl<$Res>
    implements _$TrendingTopicCopyWith<$Res> {
  __$TrendingTopicCopyWithImpl(
      _TrendingTopic _value, $Res Function(_TrendingTopic) _then)
      : super(_value, (v) => _then(v as _TrendingTopic));

  @override
  _TrendingTopic get _value => super._value as _TrendingTopic;

  @override
  $Res call({
    Object? name = freezed,
    Object? query = freezed,
    Object? image = freezed,
  }) {
    return _then(_TrendingTopic(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      query: query == freezed
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as Uri?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TrendingTopic implements _TrendingTopic {
  const _$_TrendingTopic(
      {required this.name, required this.query, required this.image});

  factory _$_TrendingTopic.fromJson(Map<String, dynamic> json) =>
      _$$_TrendingTopicFromJson(json);

  @override

  /// Description of a topic.
  final String name;
  @override

  /// Query that can/should be used to perform a new search.
  final String query;
  @override

  /// Image representation of a topic.
  final Uri? image;

  @override
  String toString() {
    return 'TrendingTopic(name: $name, query: $query, image: $image)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TrendingTopic &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.query, query) &&
            const DeepCollectionEquality().equals(other.image, image));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(query),
      const DeepCollectionEquality().hash(image));

  @JsonKey(ignore: true)
  @override
  _$TrendingTopicCopyWith<_TrendingTopic> get copyWith =>
      __$TrendingTopicCopyWithImpl<_TrendingTopic>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TrendingTopicToJson(this);
  }
}

abstract class _TrendingTopic implements TrendingTopic {
  const factory _TrendingTopic(
      {required String name,
      required String query,
      required Uri? image}) = _$_TrendingTopic;

  factory _TrendingTopic.fromJson(Map<String, dynamic> json) =
      _$_TrendingTopic.fromJson;

  @override

  /// Description of a topic.
  String get name;
  @override

  /// Query that can/should be used to perform a new search.
  String get query;
  @override

  /// Image representation of a topic.
  Uri? get image;
  @override
  @JsonKey(ignore: true)
  _$TrendingTopicCopyWith<_TrendingTopic> get copyWith =>
      throw _privateConstructorUsedError;
}
