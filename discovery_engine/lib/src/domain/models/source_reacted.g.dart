// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'source_reacted.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SourceReactedAdapter extends TypeAdapter<SourceReacted> {
  @override
  final int typeId = 17;

  @override
  SourceReacted read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SourceReacted(
      fields[0] as Source,
      fields[3] as bool,
    )
      ..weight = fields[1] as int
      ..timestamp = fields[2] as DateTime;
  }

  @override
  void write(BinaryWriter writer, SourceReacted obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.source)
      ..writeByte(1)
      ..write(obj.weight)
      ..writeByte(2)
      ..write(obj.timestamp)
      ..writeByte(3)
      ..write(obj.liked);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SourceReactedAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
