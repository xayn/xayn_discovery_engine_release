// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_resource.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class NewsResourceAdapter extends TypeAdapter<_$_NewsResource> {
  @override
  final int typeId = 5;

  @override
  _$_NewsResource read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return _$_NewsResource(
      title: fields[0] as String,
      snippet: fields[1] as String,
      url: fields[2] as Uri,
      sourceDomain: fields[3] as Source,
      image: fields[4] as Uri?,
      datePublished: fields[5] as DateTime,
      rank: fields[6] as int,
      score: fields[7] as double?,
      country: fields[8] as String,
      language: fields[9] as String,
      topic: fields[10] as String,
    );
  }

  @override
  void write(BinaryWriter writer, _$_NewsResource obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.snippet)
      ..writeByte(2)
      ..write(obj.url)
      ..writeByte(3)
      ..write(obj.sourceDomain)
      ..writeByte(4)
      ..write(obj.image)
      ..writeByte(5)
      ..write(obj.datePublished)
      ..writeByte(6)
      ..write(obj.rank)
      ..writeByte(7)
      ..write(obj.score)
      ..writeByte(8)
      ..write(obj.country)
      ..writeByte(9)
      ..write(obj.language)
      ..writeByte(10)
      ..write(obj.topic);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NewsResourceAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_NewsResource _$$_NewsResourceFromJson(Map json) => _$_NewsResource(
      title: json['title'] as String,
      snippet: json['snippet'] as String,
      url: Uri.parse(json['url'] as String),
      sourceDomain: Source.fromJson(json['sourceDomain'] as Object),
      image: json['image'] == null ? null : Uri.parse(json['image'] as String),
      datePublished: DateTime.parse(json['datePublished'] as String),
      rank: json['rank'] as int,
      score: (json['score'] as num?)?.toDouble(),
      country: json['country'] as String,
      language: json['language'] as String,
      topic: json['topic'] as String,
    );

Map<String, dynamic> _$$_NewsResourceToJson(_$_NewsResource instance) =>
    <String, dynamic>{
      'title': instance.title,
      'snippet': instance.snippet,
      'url': instance.url.toString(),
      'sourceDomain': instance.sourceDomain.toJson(),
      'image': instance.image?.toString(),
      'datePublished': instance.datePublished.toIso8601String(),
      'rank': instance.rank,
      'score': instance.score,
      'country': instance.country,
      'language': instance.language,
      'topic': instance.topic,
    };
