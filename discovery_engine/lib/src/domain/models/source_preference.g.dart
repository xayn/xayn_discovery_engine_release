// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'source_preference.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SourcePreferenceAdapter extends TypeAdapter<SourcePreference> {
  @override
  final int typeId = 14;

  @override
  SourcePreference read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SourcePreference(
      fields[0] as Source,
      fields[1] as PreferenceMode,
    );
  }

  @override
  void write(BinaryWriter writer, SourcePreference obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.source)
      ..writeByte(1)
      ..write(obj.mode);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SourcePreferenceAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class PreferenceModeAdapter extends TypeAdapter<PreferenceMode> {
  @override
  final int typeId = 15;

  @override
  PreferenceMode read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return PreferenceMode.trusted;
      case 1:
        return PreferenceMode.excluded;
      default:
        return PreferenceMode.trusted;
    }
  }

  @override
  void write(BinaryWriter writer, PreferenceMode obj) {
    switch (obj) {
      case PreferenceMode.trusted:
        writer.writeByte(0);
        break;
      case PreferenceMode.excluded:
        writer.writeByte(1);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PreferenceModeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
