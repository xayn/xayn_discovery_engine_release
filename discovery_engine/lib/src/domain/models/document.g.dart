// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'document.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DocumentAdapter extends TypeAdapter<Document> {
  @override
  final int typeId = 0;

  @override
  Document read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Document(
      stackId: fields[1] as StackId,
      resource: fields[2] as NewsResource,
      batchIndex: fields[4] as int,
      documentId: fields[0] as DocumentId,
      userReaction: fields[3] as UserReaction,
      isActive: fields[5] as bool,
      isSearched: fields[7] as bool,
    )..timestamp = fields[6] as DateTime;
  }

  @override
  void write(BinaryWriter writer, Document obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.documentId)
      ..writeByte(1)
      ..write(obj.stackId)
      ..writeByte(2)
      ..write(obj.resource)
      ..writeByte(3)
      ..write(obj.userReaction)
      ..writeByte(4)
      ..write(obj.batchIndex)
      ..writeByte(5)
      ..write(obj.isActive)
      ..writeByte(6)
      ..write(obj.timestamp)
      ..writeByte(7)
      ..write(obj.isSearched);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DocumentAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class UserReactionAdapter extends TypeAdapter<UserReaction> {
  @override
  final int typeId = 1;

  @override
  UserReaction read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return UserReaction.neutral;
      case 1:
        return UserReaction.positive;
      case 2:
        return UserReaction.negative;
      default:
        return UserReaction.neutral;
    }
  }

  @override
  void write(BinaryWriter writer, UserReaction obj) {
    switch (obj) {
      case UserReaction.neutral:
        writer.writeByte(0);
        break;
      case UserReaction.positive:
        writer.writeByte(1);
        break;
      case UserReaction.negative:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserReactionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

const _$UserReactionEnumMap = {
  UserReaction.neutral: 0,
  UserReaction.positive: 1,
  UserReaction.negative: 2,
};
