// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'active_data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ActiveDocumentDataAdapter extends TypeAdapter<ActiveDocumentData> {
  @override
  final int typeId = 2;

  @override
  ActiveDocumentData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ActiveDocumentData(
      fields[0] as Embedding,
    );
  }

  @override
  void write(BinaryWriter writer, ActiveDocumentData obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.smbertEmbedding)
      ..writeByte(1)
      ..write(obj.viewTime);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ActiveDocumentDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
