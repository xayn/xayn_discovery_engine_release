// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'active_search.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ActiveSearchAdapter extends TypeAdapter<ActiveSearch> {
  @override
  final int typeId = 11;

  @override
  ActiveSearch read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ActiveSearch(
      searchTerm: fields[0] as String,
      requestedPageNb: fields[1] as int,
      pageSize: fields[2] as int,
      searchBy: fields[3] == null ? SearchBy.query : fields[3] as SearchBy,
    );
  }

  @override
  void write(BinaryWriter writer, ActiveSearch obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.searchTerm)
      ..writeByte(1)
      ..write(obj.requestedPageNb)
      ..writeByte(2)
      ..write(obj.pageSize)
      ..writeByte(3)
      ..write(obj.searchBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ActiveSearchAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class SearchByAdapter extends TypeAdapter<SearchBy> {
  @override
  final int typeId = 16;

  @override
  SearchBy read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return SearchBy.query;
      case 1:
        return SearchBy.topic;
      default:
        return SearchBy.query;
    }
  }

  @override
  void write(BinaryWriter writer, SearchBy obj) {
    switch (obj) {
      case SearchBy.query:
        writer.writeByte(0);
        break;
      case SearchBy.topic:
        writer.writeByte(1);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SearchByAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
