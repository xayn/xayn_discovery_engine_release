// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'view_mode.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DocumentViewModeAdapter extends TypeAdapter<DocumentViewMode> {
  @override
  final int typeId = 4;

  @override
  DocumentViewMode read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return DocumentViewMode.story;
      case 1:
        return DocumentViewMode.reader;
      case 2:
        return DocumentViewMode.web;
      default:
        return DocumentViewMode.story;
    }
  }

  @override
  void write(BinaryWriter writer, DocumentViewMode obj) {
    switch (obj) {
      case DocumentViewMode.story:
        writer.writeByte(0);
        break;
      case DocumentViewMode.reader:
        writer.writeByte(1);
        break;
      case DocumentViewMode.web:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DocumentViewModeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
