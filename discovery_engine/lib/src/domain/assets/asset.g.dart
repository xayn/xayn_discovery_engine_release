// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'asset.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Asset _$AssetFromJson(Map json) {
  $checkKeys(
    json,
    requiredKeys: const ['id', 'url_suffix', 'checksum', 'fragments'],
    disallowNullValues: const ['id', 'url_suffix', 'checksum', 'fragments'],
  );
  return Asset(
    $enumDecode(_$AssetTypeEnumMap, json['id']),
    json['url_suffix'] as String,
    Checksum._checksumFromString(json['checksum'] as String),
    (json['fragments'] as List<dynamic>)
        .map((e) => Fragment.fromJson(Map<String, Object?>.from(e as Map)))
        .toList(),
  );
}

Map<String, dynamic> _$AssetToJson(Asset instance) {
  final val = <String, dynamic>{
    'id': _$AssetTypeEnumMap[instance.id]!,
    'url_suffix': instance.urlSuffix,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('checksum', Checksum._checksumToString(instance.checksum));
  val['fragments'] = instance.fragments.map((e) => e.toJson()).toList();
  return val;
}

const _$AssetTypeEnumMap = {
  AssetType.smbertVocab: 'smbertVocab',
  AssetType.smbertModel: 'smbertModel',
  AssetType.kpeVocab: 'kpeVocab',
  AssetType.kpeModel: 'kpeModel',
  AssetType.kpeCnn: 'kpeCnn',
  AssetType.kpeClassifier: 'kpeClassifier',
  AssetType.availableSources: 'availableSources',
};

Fragment _$FragmentFromJson(Map json) {
  $checkKeys(
    json,
    requiredKeys: const ['url_suffix', 'checksum'],
    disallowNullValues: const ['url_suffix', 'checksum'],
  );
  return Fragment(
    json['url_suffix'] as String,
    Checksum._checksumFromString(json['checksum'] as String),
  );
}

Map<String, dynamic> _$FragmentToJson(Fragment instance) {
  final val = <String, dynamic>{
    'url_suffix': instance.urlSuffix,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('checksum', Checksum._checksumToString(instance.checksum));
  return val;
}

Manifest _$ManifestFromJson(Map json) {
  $checkKeys(
    json,
    requiredKeys: const ['assets'],
    disallowNullValues: const ['assets'],
  );
  return Manifest(
    (json['assets'] as List<dynamic>)
        .map((e) => Asset.fromJson(Map<String, Object?>.from(e as Map)))
        .toList(),
  );
}

Map<String, dynamic> _$ManifestToJson(Manifest instance) => <String, dynamic>{
      'assets': instance.assets.map((e) => e.toJson()).toList(),
    };
