// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'document.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Document _$$_DocumentFromJson(Map json) => _$_Document(
      documentId: DocumentId.fromJson((json['documentId'] as Map).map(
        (k, e) => MapEntry(k as String, e as Object),
      )),
      stackId: StackId.fromJson((json['stackId'] as Map).map(
        (k, e) => MapEntry(k as String, e as Object),
      )),
      resource: NewsResource.fromJson(
          Map<String, Object?>.from(json['resource'] as Map)),
      userReaction: $enumDecode(_$UserReactionEnumMap, json['userReaction']),
      batchIndex: json['batchIndex'] as int,
    );

Map<String, dynamic> _$$_DocumentToJson(_$_Document instance) =>
    <String, dynamic>{
      'documentId': instance.documentId.toJson(),
      'stackId': instance.stackId.toJson(),
      'resource': instance.resource.toJson(),
      'userReaction': _$UserReactionEnumMap[instance.userReaction]!,
      'batchIndex': instance.batchIndex,
    };

const _$UserReactionEnumMap = {
  UserReaction.neutral: 0,
  UserReaction.positive: 1,
  UserReaction.negative: 2,
};
