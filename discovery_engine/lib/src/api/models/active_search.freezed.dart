// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'active_search.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ActiveSearch _$ActiveSearchFromJson(Map<String, dynamic> json) {
  return _ActiveSearch.fromJson(json);
}

/// @nodoc
class _$ActiveSearchTearOff {
  const _$ActiveSearchTearOff();

  _ActiveSearch call(
      {required String searchTerm, required domain.SearchBy searchBy}) {
    return _ActiveSearch(
      searchTerm: searchTerm,
      searchBy: searchBy,
    );
  }

  ActiveSearch fromJson(Map<String, Object?> json) {
    return ActiveSearch.fromJson(json);
  }
}

/// @nodoc
const $ActiveSearch = _$ActiveSearchTearOff();

/// @nodoc
mixin _$ActiveSearch {
  String get searchTerm => throw _privateConstructorUsedError;
  domain.SearchBy get searchBy => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ActiveSearchCopyWith<ActiveSearch> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActiveSearchCopyWith<$Res> {
  factory $ActiveSearchCopyWith(
          ActiveSearch value, $Res Function(ActiveSearch) then) =
      _$ActiveSearchCopyWithImpl<$Res>;
  $Res call({String searchTerm, domain.SearchBy searchBy});
}

/// @nodoc
class _$ActiveSearchCopyWithImpl<$Res> implements $ActiveSearchCopyWith<$Res> {
  _$ActiveSearchCopyWithImpl(this._value, this._then);

  final ActiveSearch _value;
  // ignore: unused_field
  final $Res Function(ActiveSearch) _then;

  @override
  $Res call({
    Object? searchTerm = freezed,
    Object? searchBy = freezed,
  }) {
    return _then(_value.copyWith(
      searchTerm: searchTerm == freezed
          ? _value.searchTerm
          : searchTerm // ignore: cast_nullable_to_non_nullable
              as String,
      searchBy: searchBy == freezed
          ? _value.searchBy
          : searchBy // ignore: cast_nullable_to_non_nullable
              as domain.SearchBy,
    ));
  }
}

/// @nodoc
abstract class _$ActiveSearchCopyWith<$Res>
    implements $ActiveSearchCopyWith<$Res> {
  factory _$ActiveSearchCopyWith(
          _ActiveSearch value, $Res Function(_ActiveSearch) then) =
      __$ActiveSearchCopyWithImpl<$Res>;
  @override
  $Res call({String searchTerm, domain.SearchBy searchBy});
}

/// @nodoc
class __$ActiveSearchCopyWithImpl<$Res> extends _$ActiveSearchCopyWithImpl<$Res>
    implements _$ActiveSearchCopyWith<$Res> {
  __$ActiveSearchCopyWithImpl(
      _ActiveSearch _value, $Res Function(_ActiveSearch) _then)
      : super(_value, (v) => _then(v as _ActiveSearch));

  @override
  _ActiveSearch get _value => super._value as _ActiveSearch;

  @override
  $Res call({
    Object? searchTerm = freezed,
    Object? searchBy = freezed,
  }) {
    return _then(_ActiveSearch(
      searchTerm: searchTerm == freezed
          ? _value.searchTerm
          : searchTerm // ignore: cast_nullable_to_non_nullable
              as String,
      searchBy: searchBy == freezed
          ? _value.searchBy
          : searchBy // ignore: cast_nullable_to_non_nullable
              as domain.SearchBy,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ActiveSearch implements _ActiveSearch {
  const _$_ActiveSearch({required this.searchTerm, required this.searchBy});

  factory _$_ActiveSearch.fromJson(Map<String, dynamic> json) =>
      _$$_ActiveSearchFromJson(json);

  @override
  final String searchTerm;
  @override
  final domain.SearchBy searchBy;

  @override
  String toString() {
    return 'ActiveSearch(searchTerm: $searchTerm, searchBy: $searchBy)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ActiveSearch &&
            const DeepCollectionEquality()
                .equals(other.searchTerm, searchTerm) &&
            const DeepCollectionEquality().equals(other.searchBy, searchBy));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(searchTerm),
      const DeepCollectionEquality().hash(searchBy));

  @JsonKey(ignore: true)
  @override
  _$ActiveSearchCopyWith<_ActiveSearch> get copyWith =>
      __$ActiveSearchCopyWithImpl<_ActiveSearch>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ActiveSearchToJson(this);
  }
}

abstract class _ActiveSearch implements ActiveSearch {
  const factory _ActiveSearch(
      {required String searchTerm,
      required domain.SearchBy searchBy}) = _$_ActiveSearch;

  factory _ActiveSearch.fromJson(Map<String, dynamic> json) =
      _$_ActiveSearch.fromJson;

  @override
  String get searchTerm;
  @override
  domain.SearchBy get searchBy;
  @override
  @JsonKey(ignore: true)
  _$ActiveSearchCopyWith<_ActiveSearch> get copyWith =>
      throw _privateConstructorUsedError;
}
