// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'active_search.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ActiveSearch _$$_ActiveSearchFromJson(Map json) => _$_ActiveSearch(
      searchTerm: json['searchTerm'] as String,
      searchBy: $enumDecode(_$SearchByEnumMap, json['searchBy']),
    );

Map<String, dynamic> _$$_ActiveSearchToJson(_$_ActiveSearch instance) =>
    <String, dynamic>{
      'searchTerm': instance.searchTerm,
      'searchBy': _$SearchByEnumMap[instance.searchBy]!,
    };

const _$SearchByEnumMap = {
  SearchBy.query: 'query',
  SearchBy.topic: 'topic',
};
