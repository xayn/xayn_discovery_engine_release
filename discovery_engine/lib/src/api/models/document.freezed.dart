// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'document.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Document _$DocumentFromJson(Map<String, dynamic> json) {
  return _Document.fromJson(json);
}

/// @nodoc
class _$DocumentTearOff {
  const _$DocumentTearOff();

  _Document call(
      {required DocumentId documentId,
      required StackId stackId,
      required NewsResource resource,
      required domain.UserReaction userReaction,
      required int batchIndex}) {
    return _Document(
      documentId: documentId,
      stackId: stackId,
      resource: resource,
      userReaction: userReaction,
      batchIndex: batchIndex,
    );
  }

  Document fromJson(Map<String, Object?> json) {
    return Document.fromJson(json);
  }
}

/// @nodoc
const $Document = _$DocumentTearOff();

/// @nodoc
mixin _$Document {
  DocumentId get documentId => throw _privateConstructorUsedError;
  StackId get stackId => throw _privateConstructorUsedError;
  NewsResource get resource => throw _privateConstructorUsedError;
  domain.UserReaction get userReaction => throw _privateConstructorUsedError;
  int get batchIndex => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DocumentCopyWith<Document> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DocumentCopyWith<$Res> {
  factory $DocumentCopyWith(Document value, $Res Function(Document) then) =
      _$DocumentCopyWithImpl<$Res>;
  $Res call(
      {DocumentId documentId,
      StackId stackId,
      NewsResource resource,
      domain.UserReaction userReaction,
      int batchIndex});

  $NewsResourceCopyWith<$Res> get resource;
}

/// @nodoc
class _$DocumentCopyWithImpl<$Res> implements $DocumentCopyWith<$Res> {
  _$DocumentCopyWithImpl(this._value, this._then);

  final Document _value;
  // ignore: unused_field
  final $Res Function(Document) _then;

  @override
  $Res call({
    Object? documentId = freezed,
    Object? stackId = freezed,
    Object? resource = freezed,
    Object? userReaction = freezed,
    Object? batchIndex = freezed,
  }) {
    return _then(_value.copyWith(
      documentId: documentId == freezed
          ? _value.documentId
          : documentId // ignore: cast_nullable_to_non_nullable
              as DocumentId,
      stackId: stackId == freezed
          ? _value.stackId
          : stackId // ignore: cast_nullable_to_non_nullable
              as StackId,
      resource: resource == freezed
          ? _value.resource
          : resource // ignore: cast_nullable_to_non_nullable
              as NewsResource,
      userReaction: userReaction == freezed
          ? _value.userReaction
          : userReaction // ignore: cast_nullable_to_non_nullable
              as domain.UserReaction,
      batchIndex: batchIndex == freezed
          ? _value.batchIndex
          : batchIndex // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }

  @override
  $NewsResourceCopyWith<$Res> get resource {
    return $NewsResourceCopyWith<$Res>(_value.resource, (value) {
      return _then(_value.copyWith(resource: value));
    });
  }
}

/// @nodoc
abstract class _$DocumentCopyWith<$Res> implements $DocumentCopyWith<$Res> {
  factory _$DocumentCopyWith(_Document value, $Res Function(_Document) then) =
      __$DocumentCopyWithImpl<$Res>;
  @override
  $Res call(
      {DocumentId documentId,
      StackId stackId,
      NewsResource resource,
      domain.UserReaction userReaction,
      int batchIndex});

  @override
  $NewsResourceCopyWith<$Res> get resource;
}

/// @nodoc
class __$DocumentCopyWithImpl<$Res> extends _$DocumentCopyWithImpl<$Res>
    implements _$DocumentCopyWith<$Res> {
  __$DocumentCopyWithImpl(_Document _value, $Res Function(_Document) _then)
      : super(_value, (v) => _then(v as _Document));

  @override
  _Document get _value => super._value as _Document;

  @override
  $Res call({
    Object? documentId = freezed,
    Object? stackId = freezed,
    Object? resource = freezed,
    Object? userReaction = freezed,
    Object? batchIndex = freezed,
  }) {
    return _then(_Document(
      documentId: documentId == freezed
          ? _value.documentId
          : documentId // ignore: cast_nullable_to_non_nullable
              as DocumentId,
      stackId: stackId == freezed
          ? _value.stackId
          : stackId // ignore: cast_nullable_to_non_nullable
              as StackId,
      resource: resource == freezed
          ? _value.resource
          : resource // ignore: cast_nullable_to_non_nullable
              as NewsResource,
      userReaction: userReaction == freezed
          ? _value.userReaction
          : userReaction // ignore: cast_nullable_to_non_nullable
              as domain.UserReaction,
      batchIndex: batchIndex == freezed
          ? _value.batchIndex
          : batchIndex // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Document extends _Document {
  const _$_Document(
      {required this.documentId,
      required this.stackId,
      required this.resource,
      required this.userReaction,
      required this.batchIndex})
      : super._();

  factory _$_Document.fromJson(Map<String, dynamic> json) =>
      _$$_DocumentFromJson(json);

  @override
  final DocumentId documentId;
  @override
  final StackId stackId;
  @override
  final NewsResource resource;
  @override
  final domain.UserReaction userReaction;
  @override
  final int batchIndex;

  @override
  String toString() {
    return 'Document(documentId: $documentId, stackId: $stackId, resource: $resource, userReaction: $userReaction, batchIndex: $batchIndex)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Document &&
            const DeepCollectionEquality()
                .equals(other.documentId, documentId) &&
            const DeepCollectionEquality().equals(other.stackId, stackId) &&
            const DeepCollectionEquality().equals(other.resource, resource) &&
            const DeepCollectionEquality()
                .equals(other.userReaction, userReaction) &&
            const DeepCollectionEquality()
                .equals(other.batchIndex, batchIndex));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(documentId),
      const DeepCollectionEquality().hash(stackId),
      const DeepCollectionEquality().hash(resource),
      const DeepCollectionEquality().hash(userReaction),
      const DeepCollectionEquality().hash(batchIndex));

  @JsonKey(ignore: true)
  @override
  _$DocumentCopyWith<_Document> get copyWith =>
      __$DocumentCopyWithImpl<_Document>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DocumentToJson(this);
  }
}

abstract class _Document extends Document {
  const factory _Document(
      {required DocumentId documentId,
      required StackId stackId,
      required NewsResource resource,
      required domain.UserReaction userReaction,
      required int batchIndex}) = _$_Document;
  const _Document._() : super._();

  factory _Document.fromJson(Map<String, dynamic> json) = _$_Document.fromJson;

  @override
  DocumentId get documentId;
  @override
  StackId get stackId;
  @override
  NewsResource get resource;
  @override
  domain.UserReaction get userReaction;
  @override
  int get batchIndex;
  @override
  @JsonKey(ignore: true)
  _$DocumentCopyWith<_Document> get copyWith =>
      throw _privateConstructorUsedError;
}
