// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'engine_events.dart';

// **************************************************************************
// EngineEventMapGenerator
// **************************************************************************

extension MapEvent on EngineEvent {
  EngineEvent mapEvent({
    bool? restoreFeedSucceeded,
    bool? restoreFeedFailed,
    bool? nextFeedBatchRequestSucceeded,
    bool? nextFeedBatchRequestFailed,
    bool? nextFeedBatchAvailable,
    bool? addExcludedSourceRequestSucceeded,
    bool? removeExcludedSourceRequestSucceeded,
    bool? addTrustedSourceRequestSucceeded,
    bool? removeTrustedSourceRequestSucceeded,
    bool? excludedSourcesListRequestSucceeded,
    bool? excludedSourcesListRequestFailed,
    bool? trustedSourcesListRequestSucceeded,
    bool? trustedSourcesListRequestFailed,
    bool? availableSourcesListRequestSucceeded,
    bool? availableSourcesListRequestFailed,
    bool? setSourcesRequestSucceeded,
    bool? setSourcesRequestFailed,
    bool? fetchingAssetsStarted,
    bool? fetchingAssetsProgressed,
    bool? fetchingAssetsFinished,
    bool? clientEventSucceeded,
    bool? resetAiSucceeded,
    bool? engineExceptionRaised,
    bool? documentsUpdated,
    bool? activeSearchRequestSucceeded,
    bool? activeSearchRequestFailed,
    bool? nextActiveSearchBatchRequestSucceeded,
    bool? nextActiveSearchBatchRequestFailed,
    bool? restoreActiveSearchSucceeded,
    bool? restoreActiveSearchFailed,
    bool? activeSearchTermRequestSucceeded,
    bool? activeSearchTermRequestFailed,
    bool? activeSearchClosedSucceeded,
    bool? activeSearchClosedFailed,
    bool? deepSearchRequestSucceeded,
    bool? deepSearchRequestFailed,
    bool? trendingTopicsRequestSucceeded,
    bool? trendingTopicsRequestFailed,
  }) =>
      map(
        restoreFeedSucceeded: _maybePassThrough(restoreFeedSucceeded),
        restoreFeedFailed: _maybePassThrough(restoreFeedFailed),
        nextFeedBatchRequestSucceeded:
            _maybePassThrough(nextFeedBatchRequestSucceeded),
        nextFeedBatchRequestFailed:
            _maybePassThrough(nextFeedBatchRequestFailed),
        nextFeedBatchAvailable: _maybePassThrough(nextFeedBatchAvailable),
        addExcludedSourceRequestSucceeded:
            _maybePassThrough(addExcludedSourceRequestSucceeded),
        removeExcludedSourceRequestSucceeded:
            _maybePassThrough(removeExcludedSourceRequestSucceeded),
        addTrustedSourceRequestSucceeded:
            _maybePassThrough(addTrustedSourceRequestSucceeded),
        removeTrustedSourceRequestSucceeded:
            _maybePassThrough(removeTrustedSourceRequestSucceeded),
        excludedSourcesListRequestSucceeded:
            _maybePassThrough(excludedSourcesListRequestSucceeded),
        excludedSourcesListRequestFailed:
            _maybePassThrough(excludedSourcesListRequestFailed),
        trustedSourcesListRequestSucceeded:
            _maybePassThrough(trustedSourcesListRequestSucceeded),
        trustedSourcesListRequestFailed:
            _maybePassThrough(trustedSourcesListRequestFailed),
        availableSourcesListRequestSucceeded:
            _maybePassThrough(availableSourcesListRequestSucceeded),
        availableSourcesListRequestFailed:
            _maybePassThrough(availableSourcesListRequestFailed),
        setSourcesRequestSucceeded:
            _maybePassThrough(setSourcesRequestSucceeded),
        setSourcesRequestFailed: _maybePassThrough(setSourcesRequestFailed),
        fetchingAssetsStarted: _maybePassThrough(fetchingAssetsStarted),
        fetchingAssetsProgressed: _maybePassThrough(fetchingAssetsProgressed),
        fetchingAssetsFinished: _maybePassThrough(fetchingAssetsFinished),
        clientEventSucceeded: _maybePassThrough(clientEventSucceeded),
        resetAiSucceeded: _maybePassThrough(resetAiSucceeded),
        engineExceptionRaised: _maybePassThrough(engineExceptionRaised),
        documentsUpdated: _maybePassThrough(documentsUpdated),
        activeSearchRequestSucceeded:
            _maybePassThrough(activeSearchRequestSucceeded),
        activeSearchRequestFailed: _maybePassThrough(activeSearchRequestFailed),
        nextActiveSearchBatchRequestSucceeded:
            _maybePassThrough(nextActiveSearchBatchRequestSucceeded),
        nextActiveSearchBatchRequestFailed:
            _maybePassThrough(nextActiveSearchBatchRequestFailed),
        restoreActiveSearchSucceeded:
            _maybePassThrough(restoreActiveSearchSucceeded),
        restoreActiveSearchFailed: _maybePassThrough(restoreActiveSearchFailed),
        activeSearchTermRequestSucceeded:
            _maybePassThrough(activeSearchTermRequestSucceeded),
        activeSearchTermRequestFailed:
            _maybePassThrough(activeSearchTermRequestFailed),
        activeSearchClosedSucceeded:
            _maybePassThrough(activeSearchClosedSucceeded),
        activeSearchClosedFailed: _maybePassThrough(activeSearchClosedFailed),
        deepSearchRequestSucceeded:
            _maybePassThrough(deepSearchRequestSucceeded),
        deepSearchRequestFailed: _maybePassThrough(deepSearchRequestFailed),
        trendingTopicsRequestSucceeded:
            _maybePassThrough(trendingTopicsRequestSucceeded),
        trendingTopicsRequestFailed:
            _maybePassThrough(trendingTopicsRequestFailed),
      );
  EngineEvent Function(EngineEvent) _maybePassThrough(bool? condition) {
    return condition ?? false ? _passThrough : _orElse;
  }

  // just pass through the original event
  EngineEvent _passThrough(EngineEvent event) => event;

  // in case of a wrong event in response create an EngineExceptionRaised
  EngineEvent _orElse(EngineEvent event) =>
      const EngineEvent.engineExceptionRaised(
        EngineExceptionReason.wrongEventInResponse,
      );
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RestoreFeedSucceeded _$$RestoreFeedSucceededFromJson(Map json) =>
    _$RestoreFeedSucceeded(
      (json['items'] as List<dynamic>)
          .map((e) => Document.fromJson(Map<String, Object?>.from(e as Map)))
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$RestoreFeedSucceededToJson(
        _$RestoreFeedSucceeded instance) =>
    <String, dynamic>{
      'items': instance.items.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$RestoreFeedFailed _$$RestoreFeedFailedFromJson(Map json) =>
    _$RestoreFeedFailed(
      $enumDecode(_$FeedFailureReasonEnumMap, json['reason']),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$RestoreFeedFailedToJson(_$RestoreFeedFailed instance) =>
    <String, dynamic>{
      'reason': _$FeedFailureReasonEnumMap[instance.reason]!,
      'runtimeType': instance.$type,
    };

const _$FeedFailureReasonEnumMap = {
  FeedFailureReason.notAuthorised: 0,
  FeedFailureReason.noNewsForMarket: 1,
  FeedFailureReason.stacksOpsError: 2,
};

_$NextFeedBatchRequestSucceeded _$$NextFeedBatchRequestSucceededFromJson(
        Map json) =>
    _$NextFeedBatchRequestSucceeded(
      (json['items'] as List<dynamic>)
          .map((e) => Document.fromJson(Map<String, Object?>.from(e as Map)))
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$NextFeedBatchRequestSucceededToJson(
        _$NextFeedBatchRequestSucceeded instance) =>
    <String, dynamic>{
      'items': instance.items.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$NextFeedBatchRequestFailed _$$NextFeedBatchRequestFailedFromJson(Map json) =>
    _$NextFeedBatchRequestFailed(
      $enumDecode(_$FeedFailureReasonEnumMap, json['reason']),
      errors: json['errors'] as String?,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$NextFeedBatchRequestFailedToJson(
        _$NextFeedBatchRequestFailed instance) =>
    <String, dynamic>{
      'reason': _$FeedFailureReasonEnumMap[instance.reason]!,
      'errors': instance.errors,
      'runtimeType': instance.$type,
    };

_$NextFeedBatchAvailable _$$NextFeedBatchAvailableFromJson(Map json) =>
    _$NextFeedBatchAvailable(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$NextFeedBatchAvailableToJson(
        _$NextFeedBatchAvailable instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$AddExcludedSourceRequestSucceeded
    _$$AddExcludedSourceRequestSucceededFromJson(Map json) =>
        _$AddExcludedSourceRequestSucceeded(
          Source.fromJson(json['source'] as Object),
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$AddExcludedSourceRequestSucceededToJson(
        _$AddExcludedSourceRequestSucceeded instance) =>
    <String, dynamic>{
      'source': instance.source.toJson(),
      'runtimeType': instance.$type,
    };

_$RemoveExcludedSourceRequestSucceeded
    _$$RemoveExcludedSourceRequestSucceededFromJson(Map json) =>
        _$RemoveExcludedSourceRequestSucceeded(
          Source.fromJson(json['source'] as Object),
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$RemoveExcludedSourceRequestSucceededToJson(
        _$RemoveExcludedSourceRequestSucceeded instance) =>
    <String, dynamic>{
      'source': instance.source.toJson(),
      'runtimeType': instance.$type,
    };

_$AddTrustedSourceRequestSucceeded _$$AddTrustedSourceRequestSucceededFromJson(
        Map json) =>
    _$AddTrustedSourceRequestSucceeded(
      Source.fromJson(json['source'] as Object),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$AddTrustedSourceRequestSucceededToJson(
        _$AddTrustedSourceRequestSucceeded instance) =>
    <String, dynamic>{
      'source': instance.source.toJson(),
      'runtimeType': instance.$type,
    };

_$RemoveTrustedSourceRequestSucceeded
    _$$RemoveTrustedSourceRequestSucceededFromJson(Map json) =>
        _$RemoveTrustedSourceRequestSucceeded(
          Source.fromJson(json['source'] as Object),
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$RemoveTrustedSourceRequestSucceededToJson(
        _$RemoveTrustedSourceRequestSucceeded instance) =>
    <String, dynamic>{
      'source': instance.source.toJson(),
      'runtimeType': instance.$type,
    };

_$ExcludedSourcesListRequestSucceeded
    _$$ExcludedSourcesListRequestSucceededFromJson(Map json) =>
        _$ExcludedSourcesListRequestSucceeded(
          (json['excludedSources'] as List<dynamic>)
              .map((e) => Source.fromJson(e as Object))
              .toSet(),
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$ExcludedSourcesListRequestSucceededToJson(
        _$ExcludedSourcesListRequestSucceeded instance) =>
    <String, dynamic>{
      'excludedSources':
          instance.excludedSources.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$ExcludedSourcesListRequestFailed _$$ExcludedSourcesListRequestFailedFromJson(
        Map json) =>
    _$ExcludedSourcesListRequestFailed(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ExcludedSourcesListRequestFailedToJson(
        _$ExcludedSourcesListRequestFailed instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$TrustedSourcesListRequestSucceeded
    _$$TrustedSourcesListRequestSucceededFromJson(Map json) =>
        _$TrustedSourcesListRequestSucceeded(
          (json['sources'] as List<dynamic>)
              .map((e) => Source.fromJson(e as Object))
              .toSet(),
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$TrustedSourcesListRequestSucceededToJson(
        _$TrustedSourcesListRequestSucceeded instance) =>
    <String, dynamic>{
      'sources': instance.sources.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$TrustedSourcesListRequestFailed _$$TrustedSourcesListRequestFailedFromJson(
        Map json) =>
    _$TrustedSourcesListRequestFailed(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$TrustedSourcesListRequestFailedToJson(
        _$TrustedSourcesListRequestFailed instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$AvailableSourcesListRequestSucceeded
    _$$AvailableSourcesListRequestSucceededFromJson(Map json) =>
        _$AvailableSourcesListRequestSucceeded(
          (json['availableSources'] as List<dynamic>)
              .map((e) =>
                  AvailableSource.fromJson(Map<String, Object?>.from(e as Map)))
              .toList(),
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$AvailableSourcesListRequestSucceededToJson(
        _$AvailableSourcesListRequestSucceeded instance) =>
    <String, dynamic>{
      'availableSources':
          instance.availableSources.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$AvailableSourcesListRequestFailed
    _$$AvailableSourcesListRequestFailedFromJson(Map json) =>
        _$AvailableSourcesListRequestFailed(
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$AvailableSourcesListRequestFailedToJson(
        _$AvailableSourcesListRequestFailed instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$SetSourcesRequestSucceeded _$$SetSourcesRequestSucceededFromJson(Map json) =>
    _$SetSourcesRequestSucceeded(
      trustedSources: (json['trustedSources'] as List<dynamic>)
          .map((e) => Source.fromJson(e as Object))
          .toSet(),
      excludedSources: (json['excludedSources'] as List<dynamic>)
          .map((e) => Source.fromJson(e as Object))
          .toSet(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$SetSourcesRequestSucceededToJson(
        _$SetSourcesRequestSucceeded instance) =>
    <String, dynamic>{
      'trustedSources': instance.trustedSources.map((e) => e.toJson()).toList(),
      'excludedSources':
          instance.excludedSources.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$SetSourcesRequestFailed _$$SetSourcesRequestFailedFromJson(Map json) =>
    _$SetSourcesRequestFailed(
      (json['duplicateSources'] as List<dynamic>)
          .map((e) => Source.fromJson(e as Object))
          .toSet(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$SetSourcesRequestFailedToJson(
        _$SetSourcesRequestFailed instance) =>
    <String, dynamic>{
      'duplicateSources':
          instance.duplicateSources.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$FetchingAssetsStarted _$$FetchingAssetsStartedFromJson(Map json) =>
    _$FetchingAssetsStarted(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$FetchingAssetsStartedToJson(
        _$FetchingAssetsStarted instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$FetchingAssetsProgressed _$$FetchingAssetsProgressedFromJson(Map json) =>
    _$FetchingAssetsProgressed(
      (json['percentage'] as num).toDouble(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$FetchingAssetsProgressedToJson(
        _$FetchingAssetsProgressed instance) =>
    <String, dynamic>{
      'percentage': instance.percentage,
      'runtimeType': instance.$type,
    };

_$FetchingAssetsFinished _$$FetchingAssetsFinishedFromJson(Map json) =>
    _$FetchingAssetsFinished(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$FetchingAssetsFinishedToJson(
        _$FetchingAssetsFinished instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$ClientEventSucceeded _$$ClientEventSucceededFromJson(Map json) =>
    _$ClientEventSucceeded(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ClientEventSucceededToJson(
        _$ClientEventSucceeded instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$ResetAiSucceeded _$$ResetAiSucceededFromJson(Map json) => _$ResetAiSucceeded(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ResetAiSucceededToJson(_$ResetAiSucceeded instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$EngineExceptionRaised _$$EngineExceptionRaisedFromJson(Map json) =>
    _$EngineExceptionRaised(
      $enumDecode(_$EngineExceptionReasonEnumMap, json['reason']),
      message: json['message'] as String?,
      stackTrace: json['stackTrace'] as String?,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$EngineExceptionRaisedToJson(
        _$EngineExceptionRaised instance) =>
    <String, dynamic>{
      'reason': _$EngineExceptionReasonEnumMap[instance.reason]!,
      'message': instance.message,
      'stackTrace': instance.stackTrace,
      'runtimeType': instance.$type,
    };

const _$EngineExceptionReasonEnumMap = {
  EngineExceptionReason.genericError: 0,
  EngineExceptionReason.engineNotReady: 1,
  EngineExceptionReason.wrongEventRequested: 2,
  EngineExceptionReason.wrongEventInResponse: 3,
  EngineExceptionReason.converterException: 4,
  EngineExceptionReason.responseTimeout: 5,
  EngineExceptionReason.engineDisposed: 6,
  EngineExceptionReason.failedToGetAssets: 7,
  EngineExceptionReason.invalidEngineState: 8,
};

_$DocumentsUpdated _$$DocumentsUpdatedFromJson(Map json) => _$DocumentsUpdated(
      (json['items'] as List<dynamic>)
          .map((e) => Document.fromJson(Map<String, Object?>.from(e as Map)))
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$DocumentsUpdatedToJson(_$DocumentsUpdated instance) =>
    <String, dynamic>{
      'items': instance.items.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$ActiveSearchRequestSucceeded _$$ActiveSearchRequestSucceededFromJson(
        Map json) =>
    _$ActiveSearchRequestSucceeded(
      ActiveSearch.fromJson(Map<String, Object?>.from(json['search'] as Map)),
      (json['items'] as List<dynamic>)
          .map((e) => Document.fromJson(Map<String, Object?>.from(e as Map)))
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ActiveSearchRequestSucceededToJson(
        _$ActiveSearchRequestSucceeded instance) =>
    <String, dynamic>{
      'search': instance.search.toJson(),
      'items': instance.items.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$ActiveSearchRequestFailed _$$ActiveSearchRequestFailedFromJson(Map json) =>
    _$ActiveSearchRequestFailed(
      $enumDecode(_$SearchFailureReasonEnumMap, json['reason']),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ActiveSearchRequestFailedToJson(
        _$ActiveSearchRequestFailed instance) =>
    <String, dynamic>{
      'reason': _$SearchFailureReasonEnumMap[instance.reason]!,
      'runtimeType': instance.$type,
    };

const _$SearchFailureReasonEnumMap = {
  SearchFailureReason.noActiveSearch: 0,
  SearchFailureReason.noResultsAvailable: 1,
  SearchFailureReason.openActiveSearch: 2,
};

_$NextActiveSearchBatchRequestSucceeded
    _$$NextActiveSearchBatchRequestSucceededFromJson(Map json) =>
        _$NextActiveSearchBatchRequestSucceeded(
          ActiveSearch.fromJson(
              Map<String, Object?>.from(json['search'] as Map)),
          (json['items'] as List<dynamic>)
              .map(
                  (e) => Document.fromJson(Map<String, Object?>.from(e as Map)))
              .toList(),
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$NextActiveSearchBatchRequestSucceededToJson(
        _$NextActiveSearchBatchRequestSucceeded instance) =>
    <String, dynamic>{
      'search': instance.search.toJson(),
      'items': instance.items.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$NextActiveSearchBatchRequestFailed
    _$$NextActiveSearchBatchRequestFailedFromJson(Map json) =>
        _$NextActiveSearchBatchRequestFailed(
          $enumDecode(_$SearchFailureReasonEnumMap, json['reason']),
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$NextActiveSearchBatchRequestFailedToJson(
        _$NextActiveSearchBatchRequestFailed instance) =>
    <String, dynamic>{
      'reason': _$SearchFailureReasonEnumMap[instance.reason]!,
      'runtimeType': instance.$type,
    };

_$RestoreActiveSearchSucceeded _$$RestoreActiveSearchSucceededFromJson(
        Map json) =>
    _$RestoreActiveSearchSucceeded(
      ActiveSearch.fromJson(Map<String, Object?>.from(json['search'] as Map)),
      (json['items'] as List<dynamic>)
          .map((e) => Document.fromJson(Map<String, Object?>.from(e as Map)))
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$RestoreActiveSearchSucceededToJson(
        _$RestoreActiveSearchSucceeded instance) =>
    <String, dynamic>{
      'search': instance.search.toJson(),
      'items': instance.items.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$RestoreActiveSearchFailed _$$RestoreActiveSearchFailedFromJson(Map json) =>
    _$RestoreActiveSearchFailed(
      $enumDecode(_$SearchFailureReasonEnumMap, json['reason']),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$RestoreActiveSearchFailedToJson(
        _$RestoreActiveSearchFailed instance) =>
    <String, dynamic>{
      'reason': _$SearchFailureReasonEnumMap[instance.reason]!,
      'runtimeType': instance.$type,
    };

_$ActiveSearchTermRequestSucceeded _$$ActiveSearchTermRequestSucceededFromJson(
        Map json) =>
    _$ActiveSearchTermRequestSucceeded(
      json['searchTerm'] as String,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ActiveSearchTermRequestSucceededToJson(
        _$ActiveSearchTermRequestSucceeded instance) =>
    <String, dynamic>{
      'searchTerm': instance.searchTerm,
      'runtimeType': instance.$type,
    };

_$ActiveSearchTermRequestFailed _$$ActiveSearchTermRequestFailedFromJson(
        Map json) =>
    _$ActiveSearchTermRequestFailed(
      $enumDecode(_$SearchFailureReasonEnumMap, json['reason']),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ActiveSearchTermRequestFailedToJson(
        _$ActiveSearchTermRequestFailed instance) =>
    <String, dynamic>{
      'reason': _$SearchFailureReasonEnumMap[instance.reason]!,
      'runtimeType': instance.$type,
    };

_$ActiveSearchClosedSucceeded _$$ActiveSearchClosedSucceededFromJson(
        Map json) =>
    _$ActiveSearchClosedSucceeded(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ActiveSearchClosedSucceededToJson(
        _$ActiveSearchClosedSucceeded instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$ActiveSearchClosedFailed _$$ActiveSearchClosedFailedFromJson(Map json) =>
    _$ActiveSearchClosedFailed(
      $enumDecode(_$SearchFailureReasonEnumMap, json['reason']),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ActiveSearchClosedFailedToJson(
        _$ActiveSearchClosedFailed instance) =>
    <String, dynamic>{
      'reason': _$SearchFailureReasonEnumMap[instance.reason]!,
      'runtimeType': instance.$type,
    };

_$DeepSearchRequestSucceeded _$$DeepSearchRequestSucceededFromJson(Map json) =>
    _$DeepSearchRequestSucceeded(
      (json['items'] as List<dynamic>)
          .map((e) => Document.fromJson(Map<String, Object?>.from(e as Map)))
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$DeepSearchRequestSucceededToJson(
        _$DeepSearchRequestSucceeded instance) =>
    <String, dynamic>{
      'items': instance.items.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$DeepSearchRequestFailed _$$DeepSearchRequestFailedFromJson(Map json) =>
    _$DeepSearchRequestFailed(
      $enumDecode(_$SearchFailureReasonEnumMap, json['reason']),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$DeepSearchRequestFailedToJson(
        _$DeepSearchRequestFailed instance) =>
    <String, dynamic>{
      'reason': _$SearchFailureReasonEnumMap[instance.reason]!,
      'runtimeType': instance.$type,
    };

_$TrendingTopicsRequestSucceeded _$$TrendingTopicsRequestSucceededFromJson(
        Map json) =>
    _$TrendingTopicsRequestSucceeded(
      (json['topics'] as List<dynamic>)
          .map((e) =>
              TrendingTopic.fromJson(Map<String, Object?>.from(e as Map)))
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$TrendingTopicsRequestSucceededToJson(
        _$TrendingTopicsRequestSucceeded instance) =>
    <String, dynamic>{
      'topics': instance.topics.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$TrendingTopicsRequestFailed _$$TrendingTopicsRequestFailedFromJson(
        Map json) =>
    _$TrendingTopicsRequestFailed(
      $enumDecode(_$SearchFailureReasonEnumMap, json['reason']),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$TrendingTopicsRequestFailedToJson(
        _$TrendingTopicsRequestFailed instance) =>
    <String, dynamic>{
      'reason': _$SearchFailureReasonEnumMap[instance.reason]!,
      'runtimeType': instance.$type,
    };
