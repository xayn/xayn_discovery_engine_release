// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'engine_events.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

EngineEvent _$EngineEventFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType']) {
    case 'restoreFeedSucceeded':
      return RestoreFeedSucceeded.fromJson(json);
    case 'restoreFeedFailed':
      return RestoreFeedFailed.fromJson(json);
    case 'nextFeedBatchRequestSucceeded':
      return NextFeedBatchRequestSucceeded.fromJson(json);
    case 'nextFeedBatchRequestFailed':
      return NextFeedBatchRequestFailed.fromJson(json);
    case 'nextFeedBatchAvailable':
      return NextFeedBatchAvailable.fromJson(json);
    case 'addExcludedSourceRequestSucceeded':
      return AddExcludedSourceRequestSucceeded.fromJson(json);
    case 'removeExcludedSourceRequestSucceeded':
      return RemoveExcludedSourceRequestSucceeded.fromJson(json);
    case 'addTrustedSourceRequestSucceeded':
      return AddTrustedSourceRequestSucceeded.fromJson(json);
    case 'removeTrustedSourceRequestSucceeded':
      return RemoveTrustedSourceRequestSucceeded.fromJson(json);
    case 'excludedSourcesListRequestSucceeded':
      return ExcludedSourcesListRequestSucceeded.fromJson(json);
    case 'excludedSourcesListRequestFailed':
      return ExcludedSourcesListRequestFailed.fromJson(json);
    case 'trustedSourcesListRequestSucceeded':
      return TrustedSourcesListRequestSucceeded.fromJson(json);
    case 'trustedSourcesListRequestFailed':
      return TrustedSourcesListRequestFailed.fromJson(json);
    case 'availableSourcesListRequestSucceeded':
      return AvailableSourcesListRequestSucceeded.fromJson(json);
    case 'availableSourcesListRequestFailed':
      return AvailableSourcesListRequestFailed.fromJson(json);
    case 'setSourcesRequestSucceeded':
      return SetSourcesRequestSucceeded.fromJson(json);
    case 'setSourcesRequestFailed':
      return SetSourcesRequestFailed.fromJson(json);
    case 'fetchingAssetsStarted':
      return FetchingAssetsStarted.fromJson(json);
    case 'fetchingAssetsProgressed':
      return FetchingAssetsProgressed.fromJson(json);
    case 'fetchingAssetsFinished':
      return FetchingAssetsFinished.fromJson(json);
    case 'clientEventSucceeded':
      return ClientEventSucceeded.fromJson(json);
    case 'resetAiSucceeded':
      return ResetAiSucceeded.fromJson(json);
    case 'engineExceptionRaised':
      return EngineExceptionRaised.fromJson(json);
    case 'documentsUpdated':
      return DocumentsUpdated.fromJson(json);
    case 'activeSearchRequestSucceeded':
      return ActiveSearchRequestSucceeded.fromJson(json);
    case 'activeSearchRequestFailed':
      return ActiveSearchRequestFailed.fromJson(json);
    case 'nextActiveSearchBatchRequestSucceeded':
      return NextActiveSearchBatchRequestSucceeded.fromJson(json);
    case 'nextActiveSearchBatchRequestFailed':
      return NextActiveSearchBatchRequestFailed.fromJson(json);
    case 'restoreActiveSearchSucceeded':
      return RestoreActiveSearchSucceeded.fromJson(json);
    case 'restoreActiveSearchFailed':
      return RestoreActiveSearchFailed.fromJson(json);
    case 'activeSearchTermRequestSucceeded':
      return ActiveSearchTermRequestSucceeded.fromJson(json);
    case 'activeSearchTermRequestFailed':
      return ActiveSearchTermRequestFailed.fromJson(json);
    case 'activeSearchClosedSucceeded':
      return ActiveSearchClosedSucceeded.fromJson(json);
    case 'activeSearchClosedFailed':
      return ActiveSearchClosedFailed.fromJson(json);
    case 'deepSearchRequestSucceeded':
      return DeepSearchRequestSucceeded.fromJson(json);
    case 'deepSearchRequestFailed':
      return DeepSearchRequestFailed.fromJson(json);
    case 'trendingTopicsRequestSucceeded':
      return TrendingTopicsRequestSucceeded.fromJson(json);
    case 'trendingTopicsRequestFailed':
      return TrendingTopicsRequestFailed.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'runtimeType', 'EngineEvent',
          'Invalid union type "${json['runtimeType']}"!');
  }
}

/// @nodoc
class _$EngineEventTearOff {
  const _$EngineEventTearOff();

  RestoreFeedSucceeded restoreFeedSucceeded(List<Document> items) {
    return RestoreFeedSucceeded(
      items,
    );
  }

  RestoreFeedFailed restoreFeedFailed(FeedFailureReason reason) {
    return RestoreFeedFailed(
      reason,
    );
  }

  NextFeedBatchRequestSucceeded nextFeedBatchRequestSucceeded(
      List<Document> items) {
    return NextFeedBatchRequestSucceeded(
      items,
    );
  }

  NextFeedBatchRequestFailed nextFeedBatchRequestFailed(
      FeedFailureReason reason,
      {String? errors}) {
    return NextFeedBatchRequestFailed(
      reason,
      errors: errors,
    );
  }

  NextFeedBatchAvailable nextFeedBatchAvailable() {
    return const NextFeedBatchAvailable();
  }

  AddExcludedSourceRequestSucceeded addExcludedSourceRequestSucceeded(
      Source source) {
    return AddExcludedSourceRequestSucceeded(
      source,
    );
  }

  RemoveExcludedSourceRequestSucceeded removeExcludedSourceRequestSucceeded(
      Source source) {
    return RemoveExcludedSourceRequestSucceeded(
      source,
    );
  }

  AddTrustedSourceRequestSucceeded addTrustedSourceRequestSucceeded(
      Source source) {
    return AddTrustedSourceRequestSucceeded(
      source,
    );
  }

  RemoveTrustedSourceRequestSucceeded removeTrustedSourceRequestSucceeded(
      Source source) {
    return RemoveTrustedSourceRequestSucceeded(
      source,
    );
  }

  ExcludedSourcesListRequestSucceeded excludedSourcesListRequestSucceeded(
      Set<Source> excludedSources) {
    return ExcludedSourcesListRequestSucceeded(
      excludedSources,
    );
  }

  ExcludedSourcesListRequestFailed excludedSourcesListRequestFailed() {
    return const ExcludedSourcesListRequestFailed();
  }

  TrustedSourcesListRequestSucceeded trustedSourcesListRequestSucceeded(
      Set<Source> sources) {
    return TrustedSourcesListRequestSucceeded(
      sources,
    );
  }

  TrustedSourcesListRequestFailed trustedSourcesListRequestFailed() {
    return const TrustedSourcesListRequestFailed();
  }

  AvailableSourcesListRequestSucceeded availableSourcesListRequestSucceeded(
      List<AvailableSource> availableSources) {
    return AvailableSourcesListRequestSucceeded(
      availableSources,
    );
  }

  AvailableSourcesListRequestFailed availableSourcesListRequestFailed() {
    return const AvailableSourcesListRequestFailed();
  }

  SetSourcesRequestSucceeded setSourcesRequestSucceeded(
      {required Set<Source> trustedSources,
      required Set<Source> excludedSources}) {
    return SetSourcesRequestSucceeded(
      trustedSources: trustedSources,
      excludedSources: excludedSources,
    );
  }

  SetSourcesRequestFailed setSourcesRequestFailed(
      Set<Source> duplicateSources) {
    return SetSourcesRequestFailed(
      duplicateSources,
    );
  }

  FetchingAssetsStarted fetchingAssetsStarted() {
    return const FetchingAssetsStarted();
  }

  FetchingAssetsProgressed fetchingAssetsProgressed(double percentage) {
    return FetchingAssetsProgressed(
      percentage,
    );
  }

  FetchingAssetsFinished fetchingAssetsFinished() {
    return const FetchingAssetsFinished();
  }

  ClientEventSucceeded clientEventSucceeded() {
    return const ClientEventSucceeded();
  }

  ResetAiSucceeded resetAiSucceeded() {
    return const ResetAiSucceeded();
  }

  EngineExceptionRaised engineExceptionRaised(EngineExceptionReason reason,
      {String? message, String? stackTrace}) {
    return EngineExceptionRaised(
      reason,
      message: message,
      stackTrace: stackTrace,
    );
  }

  DocumentsUpdated documentsUpdated(List<Document> items) {
    return DocumentsUpdated(
      items,
    );
  }

  ActiveSearchRequestSucceeded activeSearchRequestSucceeded(
      ActiveSearch search, List<Document> items) {
    return ActiveSearchRequestSucceeded(
      search,
      items,
    );
  }

  ActiveSearchRequestFailed activeSearchRequestFailed(
      SearchFailureReason reason) {
    return ActiveSearchRequestFailed(
      reason,
    );
  }

  NextActiveSearchBatchRequestSucceeded nextActiveSearchBatchRequestSucceeded(
      ActiveSearch search, List<Document> items) {
    return NextActiveSearchBatchRequestSucceeded(
      search,
      items,
    );
  }

  NextActiveSearchBatchRequestFailed nextActiveSearchBatchRequestFailed(
      SearchFailureReason reason) {
    return NextActiveSearchBatchRequestFailed(
      reason,
    );
  }

  RestoreActiveSearchSucceeded restoreActiveSearchSucceeded(
      ActiveSearch search, List<Document> items) {
    return RestoreActiveSearchSucceeded(
      search,
      items,
    );
  }

  RestoreActiveSearchFailed restoreActiveSearchFailed(
      SearchFailureReason reason) {
    return RestoreActiveSearchFailed(
      reason,
    );
  }

  ActiveSearchTermRequestSucceeded activeSearchTermRequestSucceeded(
      String searchTerm) {
    return ActiveSearchTermRequestSucceeded(
      searchTerm,
    );
  }

  ActiveSearchTermRequestFailed activeSearchTermRequestFailed(
      SearchFailureReason reason) {
    return ActiveSearchTermRequestFailed(
      reason,
    );
  }

  ActiveSearchClosedSucceeded activeSearchClosedSucceeded() {
    return const ActiveSearchClosedSucceeded();
  }

  ActiveSearchClosedFailed activeSearchClosedFailed(
      SearchFailureReason reason) {
    return ActiveSearchClosedFailed(
      reason,
    );
  }

  DeepSearchRequestSucceeded deepSearchRequestSucceeded(List<Document> items) {
    return DeepSearchRequestSucceeded(
      items,
    );
  }

  DeepSearchRequestFailed deepSearchRequestFailed(SearchFailureReason reason) {
    return DeepSearchRequestFailed(
      reason,
    );
  }

  TrendingTopicsRequestSucceeded trendingTopicsRequestSucceeded(
      List<TrendingTopic> topics) {
    return TrendingTopicsRequestSucceeded(
      topics,
    );
  }

  TrendingTopicsRequestFailed trendingTopicsRequestFailed(
      SearchFailureReason reason) {
    return TrendingTopicsRequestFailed(
      reason,
    );
  }

  EngineEvent fromJson(Map<String, Object?> json) {
    return EngineEvent.fromJson(json);
  }
}

/// @nodoc
const $EngineEvent = _$EngineEventTearOff();

/// @nodoc
mixin _$EngineEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EngineEventCopyWith<$Res> {
  factory $EngineEventCopyWith(
          EngineEvent value, $Res Function(EngineEvent) then) =
      _$EngineEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$EngineEventCopyWithImpl<$Res> implements $EngineEventCopyWith<$Res> {
  _$EngineEventCopyWithImpl(this._value, this._then);

  final EngineEvent _value;
  // ignore: unused_field
  final $Res Function(EngineEvent) _then;
}

/// @nodoc
abstract class $RestoreFeedSucceededCopyWith<$Res> {
  factory $RestoreFeedSucceededCopyWith(RestoreFeedSucceeded value,
          $Res Function(RestoreFeedSucceeded) then) =
      _$RestoreFeedSucceededCopyWithImpl<$Res>;
  $Res call({List<Document> items});
}

/// @nodoc
class _$RestoreFeedSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $RestoreFeedSucceededCopyWith<$Res> {
  _$RestoreFeedSucceededCopyWithImpl(
      RestoreFeedSucceeded _value, $Res Function(RestoreFeedSucceeded) _then)
      : super(_value, (v) => _then(v as RestoreFeedSucceeded));

  @override
  RestoreFeedSucceeded get _value => super._value as RestoreFeedSucceeded;

  @override
  $Res call({
    Object? items = freezed,
  }) {
    return _then(RestoreFeedSucceeded(
      items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Document>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$RestoreFeedSucceeded implements RestoreFeedSucceeded {
  const _$RestoreFeedSucceeded(this.items, {String? $type})
      : $type = $type ?? 'restoreFeedSucceeded';

  factory _$RestoreFeedSucceeded.fromJson(Map<String, dynamic> json) =>
      _$$RestoreFeedSucceededFromJson(json);

  @override
  final List<Document> items;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.restoreFeedSucceeded(items: $items)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is RestoreFeedSucceeded &&
            const DeepCollectionEquality().equals(other.items, items));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(items));

  @JsonKey(ignore: true)
  @override
  $RestoreFeedSucceededCopyWith<RestoreFeedSucceeded> get copyWith =>
      _$RestoreFeedSucceededCopyWithImpl<RestoreFeedSucceeded>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return restoreFeedSucceeded(items);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return restoreFeedSucceeded?.call(items);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (restoreFeedSucceeded != null) {
      return restoreFeedSucceeded(items);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return restoreFeedSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return restoreFeedSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (restoreFeedSucceeded != null) {
      return restoreFeedSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$RestoreFeedSucceededToJson(this);
  }
}

abstract class RestoreFeedSucceeded implements EngineEvent, FeedEngineEvent {
  const factory RestoreFeedSucceeded(List<Document> items) =
      _$RestoreFeedSucceeded;

  factory RestoreFeedSucceeded.fromJson(Map<String, dynamic> json) =
      _$RestoreFeedSucceeded.fromJson;

  List<Document> get items;
  @JsonKey(ignore: true)
  $RestoreFeedSucceededCopyWith<RestoreFeedSucceeded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RestoreFeedFailedCopyWith<$Res> {
  factory $RestoreFeedFailedCopyWith(
          RestoreFeedFailed value, $Res Function(RestoreFeedFailed) then) =
      _$RestoreFeedFailedCopyWithImpl<$Res>;
  $Res call({FeedFailureReason reason});
}

/// @nodoc
class _$RestoreFeedFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $RestoreFeedFailedCopyWith<$Res> {
  _$RestoreFeedFailedCopyWithImpl(
      RestoreFeedFailed _value, $Res Function(RestoreFeedFailed) _then)
      : super(_value, (v) => _then(v as RestoreFeedFailed));

  @override
  RestoreFeedFailed get _value => super._value as RestoreFeedFailed;

  @override
  $Res call({
    Object? reason = freezed,
  }) {
    return _then(RestoreFeedFailed(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as FeedFailureReason,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$RestoreFeedFailed implements RestoreFeedFailed {
  const _$RestoreFeedFailed(this.reason, {String? $type})
      : $type = $type ?? 'restoreFeedFailed';

  factory _$RestoreFeedFailed.fromJson(Map<String, dynamic> json) =>
      _$$RestoreFeedFailedFromJson(json);

  @override
  final FeedFailureReason reason;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.restoreFeedFailed(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is RestoreFeedFailed &&
            const DeepCollectionEquality().equals(other.reason, reason));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(reason));

  @JsonKey(ignore: true)
  @override
  $RestoreFeedFailedCopyWith<RestoreFeedFailed> get copyWith =>
      _$RestoreFeedFailedCopyWithImpl<RestoreFeedFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return restoreFeedFailed(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return restoreFeedFailed?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (restoreFeedFailed != null) {
      return restoreFeedFailed(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return restoreFeedFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return restoreFeedFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (restoreFeedFailed != null) {
      return restoreFeedFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$RestoreFeedFailedToJson(this);
  }
}

abstract class RestoreFeedFailed implements EngineEvent, FeedEngineEvent {
  const factory RestoreFeedFailed(FeedFailureReason reason) =
      _$RestoreFeedFailed;

  factory RestoreFeedFailed.fromJson(Map<String, dynamic> json) =
      _$RestoreFeedFailed.fromJson;

  FeedFailureReason get reason;
  @JsonKey(ignore: true)
  $RestoreFeedFailedCopyWith<RestoreFeedFailed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NextFeedBatchRequestSucceededCopyWith<$Res> {
  factory $NextFeedBatchRequestSucceededCopyWith(
          NextFeedBatchRequestSucceeded value,
          $Res Function(NextFeedBatchRequestSucceeded) then) =
      _$NextFeedBatchRequestSucceededCopyWithImpl<$Res>;
  $Res call({List<Document> items});
}

/// @nodoc
class _$NextFeedBatchRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $NextFeedBatchRequestSucceededCopyWith<$Res> {
  _$NextFeedBatchRequestSucceededCopyWithImpl(
      NextFeedBatchRequestSucceeded _value,
      $Res Function(NextFeedBatchRequestSucceeded) _then)
      : super(_value, (v) => _then(v as NextFeedBatchRequestSucceeded));

  @override
  NextFeedBatchRequestSucceeded get _value =>
      super._value as NextFeedBatchRequestSucceeded;

  @override
  $Res call({
    Object? items = freezed,
  }) {
    return _then(NextFeedBatchRequestSucceeded(
      items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Document>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$NextFeedBatchRequestSucceeded implements NextFeedBatchRequestSucceeded {
  const _$NextFeedBatchRequestSucceeded(this.items, {String? $type})
      : $type = $type ?? 'nextFeedBatchRequestSucceeded';

  factory _$NextFeedBatchRequestSucceeded.fromJson(Map<String, dynamic> json) =>
      _$$NextFeedBatchRequestSucceededFromJson(json);

  @override
  final List<Document> items;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.nextFeedBatchRequestSucceeded(items: $items)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is NextFeedBatchRequestSucceeded &&
            const DeepCollectionEquality().equals(other.items, items));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(items));

  @JsonKey(ignore: true)
  @override
  $NextFeedBatchRequestSucceededCopyWith<NextFeedBatchRequestSucceeded>
      get copyWith => _$NextFeedBatchRequestSucceededCopyWithImpl<
          NextFeedBatchRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchRequestSucceeded(items);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchRequestSucceeded?.call(items);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextFeedBatchRequestSucceeded != null) {
      return nextFeedBatchRequestSucceeded(items);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextFeedBatchRequestSucceeded != null) {
      return nextFeedBatchRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NextFeedBatchRequestSucceededToJson(this);
  }
}

abstract class NextFeedBatchRequestSucceeded
    implements EngineEvent, FeedEngineEvent {
  const factory NextFeedBatchRequestSucceeded(List<Document> items) =
      _$NextFeedBatchRequestSucceeded;

  factory NextFeedBatchRequestSucceeded.fromJson(Map<String, dynamic> json) =
      _$NextFeedBatchRequestSucceeded.fromJson;

  List<Document> get items;
  @JsonKey(ignore: true)
  $NextFeedBatchRequestSucceededCopyWith<NextFeedBatchRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NextFeedBatchRequestFailedCopyWith<$Res> {
  factory $NextFeedBatchRequestFailedCopyWith(NextFeedBatchRequestFailed value,
          $Res Function(NextFeedBatchRequestFailed) then) =
      _$NextFeedBatchRequestFailedCopyWithImpl<$Res>;
  $Res call({FeedFailureReason reason, String? errors});
}

/// @nodoc
class _$NextFeedBatchRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $NextFeedBatchRequestFailedCopyWith<$Res> {
  _$NextFeedBatchRequestFailedCopyWithImpl(NextFeedBatchRequestFailed _value,
      $Res Function(NextFeedBatchRequestFailed) _then)
      : super(_value, (v) => _then(v as NextFeedBatchRequestFailed));

  @override
  NextFeedBatchRequestFailed get _value =>
      super._value as NextFeedBatchRequestFailed;

  @override
  $Res call({
    Object? reason = freezed,
    Object? errors = freezed,
  }) {
    return _then(NextFeedBatchRequestFailed(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as FeedFailureReason,
      errors: errors == freezed
          ? _value.errors
          : errors // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$NextFeedBatchRequestFailed implements NextFeedBatchRequestFailed {
  const _$NextFeedBatchRequestFailed(this.reason, {this.errors, String? $type})
      : $type = $type ?? 'nextFeedBatchRequestFailed';

  factory _$NextFeedBatchRequestFailed.fromJson(Map<String, dynamic> json) =>
      _$$NextFeedBatchRequestFailedFromJson(json);

  @override
  final FeedFailureReason reason;
  @override
  final String? errors;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.nextFeedBatchRequestFailed(reason: $reason, errors: $errors)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is NextFeedBatchRequestFailed &&
            const DeepCollectionEquality().equals(other.reason, reason) &&
            const DeepCollectionEquality().equals(other.errors, errors));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(reason),
      const DeepCollectionEquality().hash(errors));

  @JsonKey(ignore: true)
  @override
  $NextFeedBatchRequestFailedCopyWith<NextFeedBatchRequestFailed>
      get copyWith =>
          _$NextFeedBatchRequestFailedCopyWithImpl<NextFeedBatchRequestFailed>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchRequestFailed(reason, errors);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchRequestFailed?.call(reason, errors);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextFeedBatchRequestFailed != null) {
      return nextFeedBatchRequestFailed(reason, errors);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextFeedBatchRequestFailed != null) {
      return nextFeedBatchRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NextFeedBatchRequestFailedToJson(this);
  }
}

abstract class NextFeedBatchRequestFailed
    implements EngineEvent, FeedEngineEvent {
  const factory NextFeedBatchRequestFailed(FeedFailureReason reason,
      {String? errors}) = _$NextFeedBatchRequestFailed;

  factory NextFeedBatchRequestFailed.fromJson(Map<String, dynamic> json) =
      _$NextFeedBatchRequestFailed.fromJson;

  FeedFailureReason get reason;
  String? get errors;
  @JsonKey(ignore: true)
  $NextFeedBatchRequestFailedCopyWith<NextFeedBatchRequestFailed>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NextFeedBatchAvailableCopyWith<$Res> {
  factory $NextFeedBatchAvailableCopyWith(NextFeedBatchAvailable value,
          $Res Function(NextFeedBatchAvailable) then) =
      _$NextFeedBatchAvailableCopyWithImpl<$Res>;
}

/// @nodoc
class _$NextFeedBatchAvailableCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $NextFeedBatchAvailableCopyWith<$Res> {
  _$NextFeedBatchAvailableCopyWithImpl(NextFeedBatchAvailable _value,
      $Res Function(NextFeedBatchAvailable) _then)
      : super(_value, (v) => _then(v as NextFeedBatchAvailable));

  @override
  NextFeedBatchAvailable get _value => super._value as NextFeedBatchAvailable;
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$NextFeedBatchAvailable implements NextFeedBatchAvailable {
  const _$NextFeedBatchAvailable({String? $type})
      : $type = $type ?? 'nextFeedBatchAvailable';

  factory _$NextFeedBatchAvailable.fromJson(Map<String, dynamic> json) =>
      _$$NextFeedBatchAvailableFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.nextFeedBatchAvailable()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is NextFeedBatchAvailable);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchAvailable();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchAvailable?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextFeedBatchAvailable != null) {
      return nextFeedBatchAvailable();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchAvailable(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return nextFeedBatchAvailable?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextFeedBatchAvailable != null) {
      return nextFeedBatchAvailable(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NextFeedBatchAvailableToJson(this);
  }
}

abstract class NextFeedBatchAvailable implements EngineEvent, FeedEngineEvent {
  const factory NextFeedBatchAvailable() = _$NextFeedBatchAvailable;

  factory NextFeedBatchAvailable.fromJson(Map<String, dynamic> json) =
      _$NextFeedBatchAvailable.fromJson;
}

/// @nodoc
abstract class $AddExcludedSourceRequestSucceededCopyWith<$Res> {
  factory $AddExcludedSourceRequestSucceededCopyWith(
          AddExcludedSourceRequestSucceeded value,
          $Res Function(AddExcludedSourceRequestSucceeded) then) =
      _$AddExcludedSourceRequestSucceededCopyWithImpl<$Res>;
  $Res call({Source source});
}

/// @nodoc
class _$AddExcludedSourceRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $AddExcludedSourceRequestSucceededCopyWith<$Res> {
  _$AddExcludedSourceRequestSucceededCopyWithImpl(
      AddExcludedSourceRequestSucceeded _value,
      $Res Function(AddExcludedSourceRequestSucceeded) _then)
      : super(_value, (v) => _then(v as AddExcludedSourceRequestSucceeded));

  @override
  AddExcludedSourceRequestSucceeded get _value =>
      super._value as AddExcludedSourceRequestSucceeded;

  @override
  $Res call({
    Object? source = freezed,
  }) {
    return _then(AddExcludedSourceRequestSucceeded(
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as Source,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$AddExcludedSourceRequestSucceeded
    implements AddExcludedSourceRequestSucceeded {
  const _$AddExcludedSourceRequestSucceeded(this.source, {String? $type})
      : $type = $type ?? 'addExcludedSourceRequestSucceeded';

  factory _$AddExcludedSourceRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$AddExcludedSourceRequestSucceededFromJson(json);

  @override
  final Source source;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.addExcludedSourceRequestSucceeded(source: $source)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is AddExcludedSourceRequestSucceeded &&
            const DeepCollectionEquality().equals(other.source, source));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(source));

  @JsonKey(ignore: true)
  @override
  $AddExcludedSourceRequestSucceededCopyWith<AddExcludedSourceRequestSucceeded>
      get copyWith => _$AddExcludedSourceRequestSucceededCopyWithImpl<
          AddExcludedSourceRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return addExcludedSourceRequestSucceeded(source);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return addExcludedSourceRequestSucceeded?.call(source);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (addExcludedSourceRequestSucceeded != null) {
      return addExcludedSourceRequestSucceeded(source);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return addExcludedSourceRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return addExcludedSourceRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (addExcludedSourceRequestSucceeded != null) {
      return addExcludedSourceRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$AddExcludedSourceRequestSucceededToJson(this);
  }
}

abstract class AddExcludedSourceRequestSucceeded
    implements EngineEvent, FeedEngineEvent {
  const factory AddExcludedSourceRequestSucceeded(Source source) =
      _$AddExcludedSourceRequestSucceeded;

  factory AddExcludedSourceRequestSucceeded.fromJson(
      Map<String, dynamic> json) = _$AddExcludedSourceRequestSucceeded.fromJson;

  Source get source;
  @JsonKey(ignore: true)
  $AddExcludedSourceRequestSucceededCopyWith<AddExcludedSourceRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RemoveExcludedSourceRequestSucceededCopyWith<$Res> {
  factory $RemoveExcludedSourceRequestSucceededCopyWith(
          RemoveExcludedSourceRequestSucceeded value,
          $Res Function(RemoveExcludedSourceRequestSucceeded) then) =
      _$RemoveExcludedSourceRequestSucceededCopyWithImpl<$Res>;
  $Res call({Source source});
}

/// @nodoc
class _$RemoveExcludedSourceRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $RemoveExcludedSourceRequestSucceededCopyWith<$Res> {
  _$RemoveExcludedSourceRequestSucceededCopyWithImpl(
      RemoveExcludedSourceRequestSucceeded _value,
      $Res Function(RemoveExcludedSourceRequestSucceeded) _then)
      : super(_value, (v) => _then(v as RemoveExcludedSourceRequestSucceeded));

  @override
  RemoveExcludedSourceRequestSucceeded get _value =>
      super._value as RemoveExcludedSourceRequestSucceeded;

  @override
  $Res call({
    Object? source = freezed,
  }) {
    return _then(RemoveExcludedSourceRequestSucceeded(
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as Source,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$RemoveExcludedSourceRequestSucceeded
    implements RemoveExcludedSourceRequestSucceeded {
  const _$RemoveExcludedSourceRequestSucceeded(this.source, {String? $type})
      : $type = $type ?? 'removeExcludedSourceRequestSucceeded';

  factory _$RemoveExcludedSourceRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$RemoveExcludedSourceRequestSucceededFromJson(json);

  @override
  final Source source;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.removeExcludedSourceRequestSucceeded(source: $source)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is RemoveExcludedSourceRequestSucceeded &&
            const DeepCollectionEquality().equals(other.source, source));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(source));

  @JsonKey(ignore: true)
  @override
  $RemoveExcludedSourceRequestSucceededCopyWith<
          RemoveExcludedSourceRequestSucceeded>
      get copyWith => _$RemoveExcludedSourceRequestSucceededCopyWithImpl<
          RemoveExcludedSourceRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return removeExcludedSourceRequestSucceeded(source);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return removeExcludedSourceRequestSucceeded?.call(source);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (removeExcludedSourceRequestSucceeded != null) {
      return removeExcludedSourceRequestSucceeded(source);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return removeExcludedSourceRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return removeExcludedSourceRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (removeExcludedSourceRequestSucceeded != null) {
      return removeExcludedSourceRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$RemoveExcludedSourceRequestSucceededToJson(this);
  }
}

abstract class RemoveExcludedSourceRequestSucceeded
    implements EngineEvent, FeedEngineEvent {
  const factory RemoveExcludedSourceRequestSucceeded(Source source) =
      _$RemoveExcludedSourceRequestSucceeded;

  factory RemoveExcludedSourceRequestSucceeded.fromJson(
          Map<String, dynamic> json) =
      _$RemoveExcludedSourceRequestSucceeded.fromJson;

  Source get source;
  @JsonKey(ignore: true)
  $RemoveExcludedSourceRequestSucceededCopyWith<
          RemoveExcludedSourceRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddTrustedSourceRequestSucceededCopyWith<$Res> {
  factory $AddTrustedSourceRequestSucceededCopyWith(
          AddTrustedSourceRequestSucceeded value,
          $Res Function(AddTrustedSourceRequestSucceeded) then) =
      _$AddTrustedSourceRequestSucceededCopyWithImpl<$Res>;
  $Res call({Source source});
}

/// @nodoc
class _$AddTrustedSourceRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $AddTrustedSourceRequestSucceededCopyWith<$Res> {
  _$AddTrustedSourceRequestSucceededCopyWithImpl(
      AddTrustedSourceRequestSucceeded _value,
      $Res Function(AddTrustedSourceRequestSucceeded) _then)
      : super(_value, (v) => _then(v as AddTrustedSourceRequestSucceeded));

  @override
  AddTrustedSourceRequestSucceeded get _value =>
      super._value as AddTrustedSourceRequestSucceeded;

  @override
  $Res call({
    Object? source = freezed,
  }) {
    return _then(AddTrustedSourceRequestSucceeded(
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as Source,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$AddTrustedSourceRequestSucceeded
    implements AddTrustedSourceRequestSucceeded {
  const _$AddTrustedSourceRequestSucceeded(this.source, {String? $type})
      : $type = $type ?? 'addTrustedSourceRequestSucceeded';

  factory _$AddTrustedSourceRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$AddTrustedSourceRequestSucceededFromJson(json);

  @override
  final Source source;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.addTrustedSourceRequestSucceeded(source: $source)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is AddTrustedSourceRequestSucceeded &&
            const DeepCollectionEquality().equals(other.source, source));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(source));

  @JsonKey(ignore: true)
  @override
  $AddTrustedSourceRequestSucceededCopyWith<AddTrustedSourceRequestSucceeded>
      get copyWith => _$AddTrustedSourceRequestSucceededCopyWithImpl<
          AddTrustedSourceRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return addTrustedSourceRequestSucceeded(source);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return addTrustedSourceRequestSucceeded?.call(source);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (addTrustedSourceRequestSucceeded != null) {
      return addTrustedSourceRequestSucceeded(source);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return addTrustedSourceRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return addTrustedSourceRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (addTrustedSourceRequestSucceeded != null) {
      return addTrustedSourceRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$AddTrustedSourceRequestSucceededToJson(this);
  }
}

abstract class AddTrustedSourceRequestSucceeded
    implements EngineEvent, FeedEngineEvent {
  const factory AddTrustedSourceRequestSucceeded(Source source) =
      _$AddTrustedSourceRequestSucceeded;

  factory AddTrustedSourceRequestSucceeded.fromJson(Map<String, dynamic> json) =
      _$AddTrustedSourceRequestSucceeded.fromJson;

  Source get source;
  @JsonKey(ignore: true)
  $AddTrustedSourceRequestSucceededCopyWith<AddTrustedSourceRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RemoveTrustedSourceRequestSucceededCopyWith<$Res> {
  factory $RemoveTrustedSourceRequestSucceededCopyWith(
          RemoveTrustedSourceRequestSucceeded value,
          $Res Function(RemoveTrustedSourceRequestSucceeded) then) =
      _$RemoveTrustedSourceRequestSucceededCopyWithImpl<$Res>;
  $Res call({Source source});
}

/// @nodoc
class _$RemoveTrustedSourceRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $RemoveTrustedSourceRequestSucceededCopyWith<$Res> {
  _$RemoveTrustedSourceRequestSucceededCopyWithImpl(
      RemoveTrustedSourceRequestSucceeded _value,
      $Res Function(RemoveTrustedSourceRequestSucceeded) _then)
      : super(_value, (v) => _then(v as RemoveTrustedSourceRequestSucceeded));

  @override
  RemoveTrustedSourceRequestSucceeded get _value =>
      super._value as RemoveTrustedSourceRequestSucceeded;

  @override
  $Res call({
    Object? source = freezed,
  }) {
    return _then(RemoveTrustedSourceRequestSucceeded(
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as Source,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$RemoveTrustedSourceRequestSucceeded
    implements RemoveTrustedSourceRequestSucceeded {
  const _$RemoveTrustedSourceRequestSucceeded(this.source, {String? $type})
      : $type = $type ?? 'removeTrustedSourceRequestSucceeded';

  factory _$RemoveTrustedSourceRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$RemoveTrustedSourceRequestSucceededFromJson(json);

  @override
  final Source source;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.removeTrustedSourceRequestSucceeded(source: $source)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is RemoveTrustedSourceRequestSucceeded &&
            const DeepCollectionEquality().equals(other.source, source));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(source));

  @JsonKey(ignore: true)
  @override
  $RemoveTrustedSourceRequestSucceededCopyWith<
          RemoveTrustedSourceRequestSucceeded>
      get copyWith => _$RemoveTrustedSourceRequestSucceededCopyWithImpl<
          RemoveTrustedSourceRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return removeTrustedSourceRequestSucceeded(source);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return removeTrustedSourceRequestSucceeded?.call(source);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (removeTrustedSourceRequestSucceeded != null) {
      return removeTrustedSourceRequestSucceeded(source);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return removeTrustedSourceRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return removeTrustedSourceRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (removeTrustedSourceRequestSucceeded != null) {
      return removeTrustedSourceRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$RemoveTrustedSourceRequestSucceededToJson(this);
  }
}

abstract class RemoveTrustedSourceRequestSucceeded
    implements EngineEvent, FeedEngineEvent {
  const factory RemoveTrustedSourceRequestSucceeded(Source source) =
      _$RemoveTrustedSourceRequestSucceeded;

  factory RemoveTrustedSourceRequestSucceeded.fromJson(
          Map<String, dynamic> json) =
      _$RemoveTrustedSourceRequestSucceeded.fromJson;

  Source get source;
  @JsonKey(ignore: true)
  $RemoveTrustedSourceRequestSucceededCopyWith<
          RemoveTrustedSourceRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExcludedSourcesListRequestSucceededCopyWith<$Res> {
  factory $ExcludedSourcesListRequestSucceededCopyWith(
          ExcludedSourcesListRequestSucceeded value,
          $Res Function(ExcludedSourcesListRequestSucceeded) then) =
      _$ExcludedSourcesListRequestSucceededCopyWithImpl<$Res>;
  $Res call({Set<Source> excludedSources});
}

/// @nodoc
class _$ExcludedSourcesListRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ExcludedSourcesListRequestSucceededCopyWith<$Res> {
  _$ExcludedSourcesListRequestSucceededCopyWithImpl(
      ExcludedSourcesListRequestSucceeded _value,
      $Res Function(ExcludedSourcesListRequestSucceeded) _then)
      : super(_value, (v) => _then(v as ExcludedSourcesListRequestSucceeded));

  @override
  ExcludedSourcesListRequestSucceeded get _value =>
      super._value as ExcludedSourcesListRequestSucceeded;

  @override
  $Res call({
    Object? excludedSources = freezed,
  }) {
    return _then(ExcludedSourcesListRequestSucceeded(
      excludedSources == freezed
          ? _value.excludedSources
          : excludedSources // ignore: cast_nullable_to_non_nullable
              as Set<Source>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$ExcludedSourcesListRequestSucceeded
    implements ExcludedSourcesListRequestSucceeded {
  const _$ExcludedSourcesListRequestSucceeded(this.excludedSources,
      {String? $type})
      : $type = $type ?? 'excludedSourcesListRequestSucceeded';

  factory _$ExcludedSourcesListRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$ExcludedSourcesListRequestSucceededFromJson(json);

  @override
  final Set<Source> excludedSources;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.excludedSourcesListRequestSucceeded(excludedSources: $excludedSources)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ExcludedSourcesListRequestSucceeded &&
            const DeepCollectionEquality()
                .equals(other.excludedSources, excludedSources));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(excludedSources));

  @JsonKey(ignore: true)
  @override
  $ExcludedSourcesListRequestSucceededCopyWith<
          ExcludedSourcesListRequestSucceeded>
      get copyWith => _$ExcludedSourcesListRequestSucceededCopyWithImpl<
          ExcludedSourcesListRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return excludedSourcesListRequestSucceeded(excludedSources);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return excludedSourcesListRequestSucceeded?.call(excludedSources);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (excludedSourcesListRequestSucceeded != null) {
      return excludedSourcesListRequestSucceeded(excludedSources);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return excludedSourcesListRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return excludedSourcesListRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (excludedSourcesListRequestSucceeded != null) {
      return excludedSourcesListRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ExcludedSourcesListRequestSucceededToJson(this);
  }
}

abstract class ExcludedSourcesListRequestSucceeded
    implements EngineEvent, FeedEngineEvent {
  const factory ExcludedSourcesListRequestSucceeded(
      Set<Source> excludedSources) = _$ExcludedSourcesListRequestSucceeded;

  factory ExcludedSourcesListRequestSucceeded.fromJson(
          Map<String, dynamic> json) =
      _$ExcludedSourcesListRequestSucceeded.fromJson;

  Set<Source> get excludedSources;
  @JsonKey(ignore: true)
  $ExcludedSourcesListRequestSucceededCopyWith<
          ExcludedSourcesListRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExcludedSourcesListRequestFailedCopyWith<$Res> {
  factory $ExcludedSourcesListRequestFailedCopyWith(
          ExcludedSourcesListRequestFailed value,
          $Res Function(ExcludedSourcesListRequestFailed) then) =
      _$ExcludedSourcesListRequestFailedCopyWithImpl<$Res>;
}

/// @nodoc
class _$ExcludedSourcesListRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ExcludedSourcesListRequestFailedCopyWith<$Res> {
  _$ExcludedSourcesListRequestFailedCopyWithImpl(
      ExcludedSourcesListRequestFailed _value,
      $Res Function(ExcludedSourcesListRequestFailed) _then)
      : super(_value, (v) => _then(v as ExcludedSourcesListRequestFailed));

  @override
  ExcludedSourcesListRequestFailed get _value =>
      super._value as ExcludedSourcesListRequestFailed;
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$ExcludedSourcesListRequestFailed
    implements ExcludedSourcesListRequestFailed {
  const _$ExcludedSourcesListRequestFailed({String? $type})
      : $type = $type ?? 'excludedSourcesListRequestFailed';

  factory _$ExcludedSourcesListRequestFailed.fromJson(
          Map<String, dynamic> json) =>
      _$$ExcludedSourcesListRequestFailedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.excludedSourcesListRequestFailed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ExcludedSourcesListRequestFailed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return excludedSourcesListRequestFailed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return excludedSourcesListRequestFailed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (excludedSourcesListRequestFailed != null) {
      return excludedSourcesListRequestFailed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return excludedSourcesListRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return excludedSourcesListRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (excludedSourcesListRequestFailed != null) {
      return excludedSourcesListRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ExcludedSourcesListRequestFailedToJson(this);
  }
}

abstract class ExcludedSourcesListRequestFailed
    implements EngineEvent, FeedEngineEvent {
  const factory ExcludedSourcesListRequestFailed() =
      _$ExcludedSourcesListRequestFailed;

  factory ExcludedSourcesListRequestFailed.fromJson(Map<String, dynamic> json) =
      _$ExcludedSourcesListRequestFailed.fromJson;
}

/// @nodoc
abstract class $TrustedSourcesListRequestSucceededCopyWith<$Res> {
  factory $TrustedSourcesListRequestSucceededCopyWith(
          TrustedSourcesListRequestSucceeded value,
          $Res Function(TrustedSourcesListRequestSucceeded) then) =
      _$TrustedSourcesListRequestSucceededCopyWithImpl<$Res>;
  $Res call({Set<Source> sources});
}

/// @nodoc
class _$TrustedSourcesListRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $TrustedSourcesListRequestSucceededCopyWith<$Res> {
  _$TrustedSourcesListRequestSucceededCopyWithImpl(
      TrustedSourcesListRequestSucceeded _value,
      $Res Function(TrustedSourcesListRequestSucceeded) _then)
      : super(_value, (v) => _then(v as TrustedSourcesListRequestSucceeded));

  @override
  TrustedSourcesListRequestSucceeded get _value =>
      super._value as TrustedSourcesListRequestSucceeded;

  @override
  $Res call({
    Object? sources = freezed,
  }) {
    return _then(TrustedSourcesListRequestSucceeded(
      sources == freezed
          ? _value.sources
          : sources // ignore: cast_nullable_to_non_nullable
              as Set<Source>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$TrustedSourcesListRequestSucceeded
    implements TrustedSourcesListRequestSucceeded {
  const _$TrustedSourcesListRequestSucceeded(this.sources, {String? $type})
      : $type = $type ?? 'trustedSourcesListRequestSucceeded';

  factory _$TrustedSourcesListRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$TrustedSourcesListRequestSucceededFromJson(json);

  @override
  final Set<Source> sources;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.trustedSourcesListRequestSucceeded(sources: $sources)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is TrustedSourcesListRequestSucceeded &&
            const DeepCollectionEquality().equals(other.sources, sources));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(sources));

  @JsonKey(ignore: true)
  @override
  $TrustedSourcesListRequestSucceededCopyWith<
          TrustedSourcesListRequestSucceeded>
      get copyWith => _$TrustedSourcesListRequestSucceededCopyWithImpl<
          TrustedSourcesListRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return trustedSourcesListRequestSucceeded(sources);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return trustedSourcesListRequestSucceeded?.call(sources);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (trustedSourcesListRequestSucceeded != null) {
      return trustedSourcesListRequestSucceeded(sources);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return trustedSourcesListRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return trustedSourcesListRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (trustedSourcesListRequestSucceeded != null) {
      return trustedSourcesListRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$TrustedSourcesListRequestSucceededToJson(this);
  }
}

abstract class TrustedSourcesListRequestSucceeded
    implements EngineEvent, FeedEngineEvent {
  const factory TrustedSourcesListRequestSucceeded(Set<Source> sources) =
      _$TrustedSourcesListRequestSucceeded;

  factory TrustedSourcesListRequestSucceeded.fromJson(
          Map<String, dynamic> json) =
      _$TrustedSourcesListRequestSucceeded.fromJson;

  Set<Source> get sources;
  @JsonKey(ignore: true)
  $TrustedSourcesListRequestSucceededCopyWith<
          TrustedSourcesListRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrustedSourcesListRequestFailedCopyWith<$Res> {
  factory $TrustedSourcesListRequestFailedCopyWith(
          TrustedSourcesListRequestFailed value,
          $Res Function(TrustedSourcesListRequestFailed) then) =
      _$TrustedSourcesListRequestFailedCopyWithImpl<$Res>;
}

/// @nodoc
class _$TrustedSourcesListRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $TrustedSourcesListRequestFailedCopyWith<$Res> {
  _$TrustedSourcesListRequestFailedCopyWithImpl(
      TrustedSourcesListRequestFailed _value,
      $Res Function(TrustedSourcesListRequestFailed) _then)
      : super(_value, (v) => _then(v as TrustedSourcesListRequestFailed));

  @override
  TrustedSourcesListRequestFailed get _value =>
      super._value as TrustedSourcesListRequestFailed;
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$TrustedSourcesListRequestFailed
    implements TrustedSourcesListRequestFailed {
  const _$TrustedSourcesListRequestFailed({String? $type})
      : $type = $type ?? 'trustedSourcesListRequestFailed';

  factory _$TrustedSourcesListRequestFailed.fromJson(
          Map<String, dynamic> json) =>
      _$$TrustedSourcesListRequestFailedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.trustedSourcesListRequestFailed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is TrustedSourcesListRequestFailed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return trustedSourcesListRequestFailed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return trustedSourcesListRequestFailed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (trustedSourcesListRequestFailed != null) {
      return trustedSourcesListRequestFailed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return trustedSourcesListRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return trustedSourcesListRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (trustedSourcesListRequestFailed != null) {
      return trustedSourcesListRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$TrustedSourcesListRequestFailedToJson(this);
  }
}

abstract class TrustedSourcesListRequestFailed
    implements EngineEvent, FeedEngineEvent {
  const factory TrustedSourcesListRequestFailed() =
      _$TrustedSourcesListRequestFailed;

  factory TrustedSourcesListRequestFailed.fromJson(Map<String, dynamic> json) =
      _$TrustedSourcesListRequestFailed.fromJson;
}

/// @nodoc
abstract class $AvailableSourcesListRequestSucceededCopyWith<$Res> {
  factory $AvailableSourcesListRequestSucceededCopyWith(
          AvailableSourcesListRequestSucceeded value,
          $Res Function(AvailableSourcesListRequestSucceeded) then) =
      _$AvailableSourcesListRequestSucceededCopyWithImpl<$Res>;
  $Res call({List<AvailableSource> availableSources});
}

/// @nodoc
class _$AvailableSourcesListRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $AvailableSourcesListRequestSucceededCopyWith<$Res> {
  _$AvailableSourcesListRequestSucceededCopyWithImpl(
      AvailableSourcesListRequestSucceeded _value,
      $Res Function(AvailableSourcesListRequestSucceeded) _then)
      : super(_value, (v) => _then(v as AvailableSourcesListRequestSucceeded));

  @override
  AvailableSourcesListRequestSucceeded get _value =>
      super._value as AvailableSourcesListRequestSucceeded;

  @override
  $Res call({
    Object? availableSources = freezed,
  }) {
    return _then(AvailableSourcesListRequestSucceeded(
      availableSources == freezed
          ? _value.availableSources
          : availableSources // ignore: cast_nullable_to_non_nullable
              as List<AvailableSource>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$AvailableSourcesListRequestSucceeded
    implements AvailableSourcesListRequestSucceeded {
  const _$AvailableSourcesListRequestSucceeded(this.availableSources,
      {String? $type})
      : $type = $type ?? 'availableSourcesListRequestSucceeded';

  factory _$AvailableSourcesListRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$AvailableSourcesListRequestSucceededFromJson(json);

  @override
  final List<AvailableSource> availableSources;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.availableSourcesListRequestSucceeded(availableSources: $availableSources)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is AvailableSourcesListRequestSucceeded &&
            const DeepCollectionEquality()
                .equals(other.availableSources, availableSources));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(availableSources));

  @JsonKey(ignore: true)
  @override
  $AvailableSourcesListRequestSucceededCopyWith<
          AvailableSourcesListRequestSucceeded>
      get copyWith => _$AvailableSourcesListRequestSucceededCopyWithImpl<
          AvailableSourcesListRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return availableSourcesListRequestSucceeded(availableSources);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return availableSourcesListRequestSucceeded?.call(availableSources);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (availableSourcesListRequestSucceeded != null) {
      return availableSourcesListRequestSucceeded(availableSources);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return availableSourcesListRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return availableSourcesListRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (availableSourcesListRequestSucceeded != null) {
      return availableSourcesListRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$AvailableSourcesListRequestSucceededToJson(this);
  }
}

abstract class AvailableSourcesListRequestSucceeded
    implements EngineEvent, FeedEngineEvent {
  const factory AvailableSourcesListRequestSucceeded(
          List<AvailableSource> availableSources) =
      _$AvailableSourcesListRequestSucceeded;

  factory AvailableSourcesListRequestSucceeded.fromJson(
          Map<String, dynamic> json) =
      _$AvailableSourcesListRequestSucceeded.fromJson;

  List<AvailableSource> get availableSources;
  @JsonKey(ignore: true)
  $AvailableSourcesListRequestSucceededCopyWith<
          AvailableSourcesListRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AvailableSourcesListRequestFailedCopyWith<$Res> {
  factory $AvailableSourcesListRequestFailedCopyWith(
          AvailableSourcesListRequestFailed value,
          $Res Function(AvailableSourcesListRequestFailed) then) =
      _$AvailableSourcesListRequestFailedCopyWithImpl<$Res>;
}

/// @nodoc
class _$AvailableSourcesListRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $AvailableSourcesListRequestFailedCopyWith<$Res> {
  _$AvailableSourcesListRequestFailedCopyWithImpl(
      AvailableSourcesListRequestFailed _value,
      $Res Function(AvailableSourcesListRequestFailed) _then)
      : super(_value, (v) => _then(v as AvailableSourcesListRequestFailed));

  @override
  AvailableSourcesListRequestFailed get _value =>
      super._value as AvailableSourcesListRequestFailed;
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$AvailableSourcesListRequestFailed
    implements AvailableSourcesListRequestFailed {
  const _$AvailableSourcesListRequestFailed({String? $type})
      : $type = $type ?? 'availableSourcesListRequestFailed';

  factory _$AvailableSourcesListRequestFailed.fromJson(
          Map<String, dynamic> json) =>
      _$$AvailableSourcesListRequestFailedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.availableSourcesListRequestFailed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is AvailableSourcesListRequestFailed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return availableSourcesListRequestFailed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return availableSourcesListRequestFailed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (availableSourcesListRequestFailed != null) {
      return availableSourcesListRequestFailed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return availableSourcesListRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return availableSourcesListRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (availableSourcesListRequestFailed != null) {
      return availableSourcesListRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$AvailableSourcesListRequestFailedToJson(this);
  }
}

abstract class AvailableSourcesListRequestFailed
    implements EngineEvent, FeedEngineEvent {
  const factory AvailableSourcesListRequestFailed() =
      _$AvailableSourcesListRequestFailed;

  factory AvailableSourcesListRequestFailed.fromJson(
      Map<String, dynamic> json) = _$AvailableSourcesListRequestFailed.fromJson;
}

/// @nodoc
abstract class $SetSourcesRequestSucceededCopyWith<$Res> {
  factory $SetSourcesRequestSucceededCopyWith(SetSourcesRequestSucceeded value,
          $Res Function(SetSourcesRequestSucceeded) then) =
      _$SetSourcesRequestSucceededCopyWithImpl<$Res>;
  $Res call({Set<Source> trustedSources, Set<Source> excludedSources});
}

/// @nodoc
class _$SetSourcesRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $SetSourcesRequestSucceededCopyWith<$Res> {
  _$SetSourcesRequestSucceededCopyWithImpl(SetSourcesRequestSucceeded _value,
      $Res Function(SetSourcesRequestSucceeded) _then)
      : super(_value, (v) => _then(v as SetSourcesRequestSucceeded));

  @override
  SetSourcesRequestSucceeded get _value =>
      super._value as SetSourcesRequestSucceeded;

  @override
  $Res call({
    Object? trustedSources = freezed,
    Object? excludedSources = freezed,
  }) {
    return _then(SetSourcesRequestSucceeded(
      trustedSources: trustedSources == freezed
          ? _value.trustedSources
          : trustedSources // ignore: cast_nullable_to_non_nullable
              as Set<Source>,
      excludedSources: excludedSources == freezed
          ? _value.excludedSources
          : excludedSources // ignore: cast_nullable_to_non_nullable
              as Set<Source>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$SetSourcesRequestSucceeded implements SetSourcesRequestSucceeded {
  const _$SetSourcesRequestSucceeded(
      {required this.trustedSources,
      required this.excludedSources,
      String? $type})
      : $type = $type ?? 'setSourcesRequestSucceeded';

  factory _$SetSourcesRequestSucceeded.fromJson(Map<String, dynamic> json) =>
      _$$SetSourcesRequestSucceededFromJson(json);

  @override
  final Set<Source> trustedSources;
  @override
  final Set<Source> excludedSources;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.setSourcesRequestSucceeded(trustedSources: $trustedSources, excludedSources: $excludedSources)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is SetSourcesRequestSucceeded &&
            const DeepCollectionEquality()
                .equals(other.trustedSources, trustedSources) &&
            const DeepCollectionEquality()
                .equals(other.excludedSources, excludedSources));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(trustedSources),
      const DeepCollectionEquality().hash(excludedSources));

  @JsonKey(ignore: true)
  @override
  $SetSourcesRequestSucceededCopyWith<SetSourcesRequestSucceeded>
      get copyWith =>
          _$SetSourcesRequestSucceededCopyWithImpl<SetSourcesRequestSucceeded>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return setSourcesRequestSucceeded(trustedSources, excludedSources);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return setSourcesRequestSucceeded?.call(trustedSources, excludedSources);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (setSourcesRequestSucceeded != null) {
      return setSourcesRequestSucceeded(trustedSources, excludedSources);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return setSourcesRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return setSourcesRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (setSourcesRequestSucceeded != null) {
      return setSourcesRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$SetSourcesRequestSucceededToJson(this);
  }
}

abstract class SetSourcesRequestSucceeded
    implements EngineEvent, FeedEngineEvent {
  const factory SetSourcesRequestSucceeded(
      {required Set<Source> trustedSources,
      required Set<Source> excludedSources}) = _$SetSourcesRequestSucceeded;

  factory SetSourcesRequestSucceeded.fromJson(Map<String, dynamic> json) =
      _$SetSourcesRequestSucceeded.fromJson;

  Set<Source> get trustedSources;
  Set<Source> get excludedSources;
  @JsonKey(ignore: true)
  $SetSourcesRequestSucceededCopyWith<SetSourcesRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SetSourcesRequestFailedCopyWith<$Res> {
  factory $SetSourcesRequestFailedCopyWith(SetSourcesRequestFailed value,
          $Res Function(SetSourcesRequestFailed) then) =
      _$SetSourcesRequestFailedCopyWithImpl<$Res>;
  $Res call({Set<Source> duplicateSources});
}

/// @nodoc
class _$SetSourcesRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $SetSourcesRequestFailedCopyWith<$Res> {
  _$SetSourcesRequestFailedCopyWithImpl(SetSourcesRequestFailed _value,
      $Res Function(SetSourcesRequestFailed) _then)
      : super(_value, (v) => _then(v as SetSourcesRequestFailed));

  @override
  SetSourcesRequestFailed get _value => super._value as SetSourcesRequestFailed;

  @override
  $Res call({
    Object? duplicateSources = freezed,
  }) {
    return _then(SetSourcesRequestFailed(
      duplicateSources == freezed
          ? _value.duplicateSources
          : duplicateSources // ignore: cast_nullable_to_non_nullable
              as Set<Source>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedEngineEvent>()
class _$SetSourcesRequestFailed implements SetSourcesRequestFailed {
  const _$SetSourcesRequestFailed(this.duplicateSources, {String? $type})
      : $type = $type ?? 'setSourcesRequestFailed';

  factory _$SetSourcesRequestFailed.fromJson(Map<String, dynamic> json) =>
      _$$SetSourcesRequestFailedFromJson(json);

  @override
  final Set<Source> duplicateSources;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.setSourcesRequestFailed(duplicateSources: $duplicateSources)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is SetSourcesRequestFailed &&
            const DeepCollectionEquality()
                .equals(other.duplicateSources, duplicateSources));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(duplicateSources));

  @JsonKey(ignore: true)
  @override
  $SetSourcesRequestFailedCopyWith<SetSourcesRequestFailed> get copyWith =>
      _$SetSourcesRequestFailedCopyWithImpl<SetSourcesRequestFailed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return setSourcesRequestFailed(duplicateSources);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return setSourcesRequestFailed?.call(duplicateSources);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (setSourcesRequestFailed != null) {
      return setSourcesRequestFailed(duplicateSources);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return setSourcesRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return setSourcesRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (setSourcesRequestFailed != null) {
      return setSourcesRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$SetSourcesRequestFailedToJson(this);
  }
}

abstract class SetSourcesRequestFailed implements EngineEvent, FeedEngineEvent {
  const factory SetSourcesRequestFailed(Set<Source> duplicateSources) =
      _$SetSourcesRequestFailed;

  factory SetSourcesRequestFailed.fromJson(Map<String, dynamic> json) =
      _$SetSourcesRequestFailed.fromJson;

  Set<Source> get duplicateSources;
  @JsonKey(ignore: true)
  $SetSourcesRequestFailedCopyWith<SetSourcesRequestFailed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FetchingAssetsStartedCopyWith<$Res> {
  factory $FetchingAssetsStartedCopyWith(FetchingAssetsStarted value,
          $Res Function(FetchingAssetsStarted) then) =
      _$FetchingAssetsStartedCopyWithImpl<$Res>;
}

/// @nodoc
class _$FetchingAssetsStartedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $FetchingAssetsStartedCopyWith<$Res> {
  _$FetchingAssetsStartedCopyWithImpl(
      FetchingAssetsStarted _value, $Res Function(FetchingAssetsStarted) _then)
      : super(_value, (v) => _then(v as FetchingAssetsStarted));

  @override
  FetchingAssetsStarted get _value => super._value as FetchingAssetsStarted;
}

/// @nodoc
@JsonSerializable()
@Implements<AssetsStatusEngineEvent>()
class _$FetchingAssetsStarted implements FetchingAssetsStarted {
  const _$FetchingAssetsStarted({String? $type})
      : $type = $type ?? 'fetchingAssetsStarted';

  factory _$FetchingAssetsStarted.fromJson(Map<String, dynamic> json) =>
      _$$FetchingAssetsStartedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.fetchingAssetsStarted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is FetchingAssetsStarted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsStarted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsStarted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (fetchingAssetsStarted != null) {
      return fetchingAssetsStarted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsStarted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsStarted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (fetchingAssetsStarted != null) {
      return fetchingAssetsStarted(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$FetchingAssetsStartedToJson(this);
  }
}

abstract class FetchingAssetsStarted
    implements EngineEvent, AssetsStatusEngineEvent {
  const factory FetchingAssetsStarted() = _$FetchingAssetsStarted;

  factory FetchingAssetsStarted.fromJson(Map<String, dynamic> json) =
      _$FetchingAssetsStarted.fromJson;
}

/// @nodoc
abstract class $FetchingAssetsProgressedCopyWith<$Res> {
  factory $FetchingAssetsProgressedCopyWith(FetchingAssetsProgressed value,
          $Res Function(FetchingAssetsProgressed) then) =
      _$FetchingAssetsProgressedCopyWithImpl<$Res>;
  $Res call({double percentage});
}

/// @nodoc
class _$FetchingAssetsProgressedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $FetchingAssetsProgressedCopyWith<$Res> {
  _$FetchingAssetsProgressedCopyWithImpl(FetchingAssetsProgressed _value,
      $Res Function(FetchingAssetsProgressed) _then)
      : super(_value, (v) => _then(v as FetchingAssetsProgressed));

  @override
  FetchingAssetsProgressed get _value =>
      super._value as FetchingAssetsProgressed;

  @override
  $Res call({
    Object? percentage = freezed,
  }) {
    return _then(FetchingAssetsProgressed(
      percentage == freezed
          ? _value.percentage
          : percentage // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<AssetsStatusEngineEvent>()
class _$FetchingAssetsProgressed implements FetchingAssetsProgressed {
  const _$FetchingAssetsProgressed(this.percentage, {String? $type})
      : $type = $type ?? 'fetchingAssetsProgressed';

  factory _$FetchingAssetsProgressed.fromJson(Map<String, dynamic> json) =>
      _$$FetchingAssetsProgressedFromJson(json);

  @override
  final double percentage;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.fetchingAssetsProgressed(percentage: $percentage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchingAssetsProgressed &&
            const DeepCollectionEquality()
                .equals(other.percentage, percentage));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(percentage));

  @JsonKey(ignore: true)
  @override
  $FetchingAssetsProgressedCopyWith<FetchingAssetsProgressed> get copyWith =>
      _$FetchingAssetsProgressedCopyWithImpl<FetchingAssetsProgressed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsProgressed(percentage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsProgressed?.call(percentage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (fetchingAssetsProgressed != null) {
      return fetchingAssetsProgressed(percentage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsProgressed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsProgressed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (fetchingAssetsProgressed != null) {
      return fetchingAssetsProgressed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$FetchingAssetsProgressedToJson(this);
  }
}

abstract class FetchingAssetsProgressed
    implements EngineEvent, AssetsStatusEngineEvent {
  const factory FetchingAssetsProgressed(double percentage) =
      _$FetchingAssetsProgressed;

  factory FetchingAssetsProgressed.fromJson(Map<String, dynamic> json) =
      _$FetchingAssetsProgressed.fromJson;

  double get percentage;
  @JsonKey(ignore: true)
  $FetchingAssetsProgressedCopyWith<FetchingAssetsProgressed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FetchingAssetsFinishedCopyWith<$Res> {
  factory $FetchingAssetsFinishedCopyWith(FetchingAssetsFinished value,
          $Res Function(FetchingAssetsFinished) then) =
      _$FetchingAssetsFinishedCopyWithImpl<$Res>;
}

/// @nodoc
class _$FetchingAssetsFinishedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $FetchingAssetsFinishedCopyWith<$Res> {
  _$FetchingAssetsFinishedCopyWithImpl(FetchingAssetsFinished _value,
      $Res Function(FetchingAssetsFinished) _then)
      : super(_value, (v) => _then(v as FetchingAssetsFinished));

  @override
  FetchingAssetsFinished get _value => super._value as FetchingAssetsFinished;
}

/// @nodoc
@JsonSerializable()
@Implements<AssetsStatusEngineEvent>()
class _$FetchingAssetsFinished implements FetchingAssetsFinished {
  const _$FetchingAssetsFinished({String? $type})
      : $type = $type ?? 'fetchingAssetsFinished';

  factory _$FetchingAssetsFinished.fromJson(Map<String, dynamic> json) =>
      _$$FetchingAssetsFinishedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.fetchingAssetsFinished()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is FetchingAssetsFinished);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsFinished();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsFinished?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (fetchingAssetsFinished != null) {
      return fetchingAssetsFinished();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsFinished(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return fetchingAssetsFinished?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (fetchingAssetsFinished != null) {
      return fetchingAssetsFinished(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$FetchingAssetsFinishedToJson(this);
  }
}

abstract class FetchingAssetsFinished
    implements EngineEvent, AssetsStatusEngineEvent {
  const factory FetchingAssetsFinished() = _$FetchingAssetsFinished;

  factory FetchingAssetsFinished.fromJson(Map<String, dynamic> json) =
      _$FetchingAssetsFinished.fromJson;
}

/// @nodoc
abstract class $ClientEventSucceededCopyWith<$Res> {
  factory $ClientEventSucceededCopyWith(ClientEventSucceeded value,
          $Res Function(ClientEventSucceeded) then) =
      _$ClientEventSucceededCopyWithImpl<$Res>;
}

/// @nodoc
class _$ClientEventSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ClientEventSucceededCopyWith<$Res> {
  _$ClientEventSucceededCopyWithImpl(
      ClientEventSucceeded _value, $Res Function(ClientEventSucceeded) _then)
      : super(_value, (v) => _then(v as ClientEventSucceeded));

  @override
  ClientEventSucceeded get _value => super._value as ClientEventSucceeded;
}

/// @nodoc
@JsonSerializable()
@Implements<SystemEngineEvent>()
class _$ClientEventSucceeded implements ClientEventSucceeded {
  const _$ClientEventSucceeded({String? $type})
      : $type = $type ?? 'clientEventSucceeded';

  factory _$ClientEventSucceeded.fromJson(Map<String, dynamic> json) =>
      _$$ClientEventSucceededFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.clientEventSucceeded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ClientEventSucceeded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return clientEventSucceeded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return clientEventSucceeded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (clientEventSucceeded != null) {
      return clientEventSucceeded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return clientEventSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return clientEventSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (clientEventSucceeded != null) {
      return clientEventSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ClientEventSucceededToJson(this);
  }
}

abstract class ClientEventSucceeded implements EngineEvent, SystemEngineEvent {
  const factory ClientEventSucceeded() = _$ClientEventSucceeded;

  factory ClientEventSucceeded.fromJson(Map<String, dynamic> json) =
      _$ClientEventSucceeded.fromJson;
}

/// @nodoc
abstract class $ResetAiSucceededCopyWith<$Res> {
  factory $ResetAiSucceededCopyWith(
          ResetAiSucceeded value, $Res Function(ResetAiSucceeded) then) =
      _$ResetAiSucceededCopyWithImpl<$Res>;
}

/// @nodoc
class _$ResetAiSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ResetAiSucceededCopyWith<$Res> {
  _$ResetAiSucceededCopyWithImpl(
      ResetAiSucceeded _value, $Res Function(ResetAiSucceeded) _then)
      : super(_value, (v) => _then(v as ResetAiSucceeded));

  @override
  ResetAiSucceeded get _value => super._value as ResetAiSucceeded;
}

/// @nodoc
@JsonSerializable()
@Implements<SystemEngineEvent>()
class _$ResetAiSucceeded implements ResetAiSucceeded {
  const _$ResetAiSucceeded({String? $type})
      : $type = $type ?? 'resetAiSucceeded';

  factory _$ResetAiSucceeded.fromJson(Map<String, dynamic> json) =>
      _$$ResetAiSucceededFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.resetAiSucceeded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ResetAiSucceeded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return resetAiSucceeded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return resetAiSucceeded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (resetAiSucceeded != null) {
      return resetAiSucceeded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return resetAiSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return resetAiSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (resetAiSucceeded != null) {
      return resetAiSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ResetAiSucceededToJson(this);
  }
}

abstract class ResetAiSucceeded implements EngineEvent, SystemEngineEvent {
  const factory ResetAiSucceeded() = _$ResetAiSucceeded;

  factory ResetAiSucceeded.fromJson(Map<String, dynamic> json) =
      _$ResetAiSucceeded.fromJson;
}

/// @nodoc
abstract class $EngineExceptionRaisedCopyWith<$Res> {
  factory $EngineExceptionRaisedCopyWith(EngineExceptionRaised value,
          $Res Function(EngineExceptionRaised) then) =
      _$EngineExceptionRaisedCopyWithImpl<$Res>;
  $Res call(
      {EngineExceptionReason reason, String? message, String? stackTrace});
}

/// @nodoc
class _$EngineExceptionRaisedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $EngineExceptionRaisedCopyWith<$Res> {
  _$EngineExceptionRaisedCopyWithImpl(
      EngineExceptionRaised _value, $Res Function(EngineExceptionRaised) _then)
      : super(_value, (v) => _then(v as EngineExceptionRaised));

  @override
  EngineExceptionRaised get _value => super._value as EngineExceptionRaised;

  @override
  $Res call({
    Object? reason = freezed,
    Object? message = freezed,
    Object? stackTrace = freezed,
  }) {
    return _then(EngineExceptionRaised(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as EngineExceptionReason,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
      stackTrace: stackTrace == freezed
          ? _value.stackTrace
          : stackTrace // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SystemEngineEvent>()
class _$EngineExceptionRaised implements EngineExceptionRaised {
  const _$EngineExceptionRaised(this.reason,
      {this.message, this.stackTrace, String? $type})
      : $type = $type ?? 'engineExceptionRaised';

  factory _$EngineExceptionRaised.fromJson(Map<String, dynamic> json) =>
      _$$EngineExceptionRaisedFromJson(json);

  @override
  final EngineExceptionReason reason;
  @override
  final String? message;
  @override
  final String? stackTrace;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.engineExceptionRaised(reason: $reason, message: $message, stackTrace: $stackTrace)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is EngineExceptionRaised &&
            const DeepCollectionEquality().equals(other.reason, reason) &&
            const DeepCollectionEquality().equals(other.message, message) &&
            const DeepCollectionEquality()
                .equals(other.stackTrace, stackTrace));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(reason),
      const DeepCollectionEquality().hash(message),
      const DeepCollectionEquality().hash(stackTrace));

  @JsonKey(ignore: true)
  @override
  $EngineExceptionRaisedCopyWith<EngineExceptionRaised> get copyWith =>
      _$EngineExceptionRaisedCopyWithImpl<EngineExceptionRaised>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return engineExceptionRaised(reason, message, stackTrace);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return engineExceptionRaised?.call(reason, message, stackTrace);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (engineExceptionRaised != null) {
      return engineExceptionRaised(reason, message, stackTrace);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return engineExceptionRaised(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return engineExceptionRaised?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (engineExceptionRaised != null) {
      return engineExceptionRaised(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$EngineExceptionRaisedToJson(this);
  }
}

abstract class EngineExceptionRaised implements EngineEvent, SystemEngineEvent {
  const factory EngineExceptionRaised(EngineExceptionReason reason,
      {String? message, String? stackTrace}) = _$EngineExceptionRaised;

  factory EngineExceptionRaised.fromJson(Map<String, dynamic> json) =
      _$EngineExceptionRaised.fromJson;

  EngineExceptionReason get reason;
  String? get message;
  String? get stackTrace;
  @JsonKey(ignore: true)
  $EngineExceptionRaisedCopyWith<EngineExceptionRaised> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DocumentsUpdatedCopyWith<$Res> {
  factory $DocumentsUpdatedCopyWith(
          DocumentsUpdated value, $Res Function(DocumentsUpdated) then) =
      _$DocumentsUpdatedCopyWithImpl<$Res>;
  $Res call({List<Document> items});
}

/// @nodoc
class _$DocumentsUpdatedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $DocumentsUpdatedCopyWith<$Res> {
  _$DocumentsUpdatedCopyWithImpl(
      DocumentsUpdated _value, $Res Function(DocumentsUpdated) _then)
      : super(_value, (v) => _then(v as DocumentsUpdated));

  @override
  DocumentsUpdated get _value => super._value as DocumentsUpdated;

  @override
  $Res call({
    Object? items = freezed,
  }) {
    return _then(DocumentsUpdated(
      items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Document>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<DocumentEngineEvent>()
class _$DocumentsUpdated implements DocumentsUpdated {
  const _$DocumentsUpdated(this.items, {String? $type})
      : $type = $type ?? 'documentsUpdated';

  factory _$DocumentsUpdated.fromJson(Map<String, dynamic> json) =>
      _$$DocumentsUpdatedFromJson(json);

  @override
  final List<Document> items;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.documentsUpdated(items: $items)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is DocumentsUpdated &&
            const DeepCollectionEquality().equals(other.items, items));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(items));

  @JsonKey(ignore: true)
  @override
  $DocumentsUpdatedCopyWith<DocumentsUpdated> get copyWith =>
      _$DocumentsUpdatedCopyWithImpl<DocumentsUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return documentsUpdated(items);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return documentsUpdated?.call(items);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (documentsUpdated != null) {
      return documentsUpdated(items);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return documentsUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return documentsUpdated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (documentsUpdated != null) {
      return documentsUpdated(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$DocumentsUpdatedToJson(this);
  }
}

abstract class DocumentsUpdated implements EngineEvent, DocumentEngineEvent {
  const factory DocumentsUpdated(List<Document> items) = _$DocumentsUpdated;

  factory DocumentsUpdated.fromJson(Map<String, dynamic> json) =
      _$DocumentsUpdated.fromJson;

  List<Document> get items;
  @JsonKey(ignore: true)
  $DocumentsUpdatedCopyWith<DocumentsUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActiveSearchRequestSucceededCopyWith<$Res> {
  factory $ActiveSearchRequestSucceededCopyWith(
          ActiveSearchRequestSucceeded value,
          $Res Function(ActiveSearchRequestSucceeded) then) =
      _$ActiveSearchRequestSucceededCopyWithImpl<$Res>;
  $Res call({ActiveSearch search, List<Document> items});

  $ActiveSearchCopyWith<$Res> get search;
}

/// @nodoc
class _$ActiveSearchRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ActiveSearchRequestSucceededCopyWith<$Res> {
  _$ActiveSearchRequestSucceededCopyWithImpl(
      ActiveSearchRequestSucceeded _value,
      $Res Function(ActiveSearchRequestSucceeded) _then)
      : super(_value, (v) => _then(v as ActiveSearchRequestSucceeded));

  @override
  ActiveSearchRequestSucceeded get _value =>
      super._value as ActiveSearchRequestSucceeded;

  @override
  $Res call({
    Object? search = freezed,
    Object? items = freezed,
  }) {
    return _then(ActiveSearchRequestSucceeded(
      search == freezed
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as ActiveSearch,
      items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Document>,
    ));
  }

  @override
  $ActiveSearchCopyWith<$Res> get search {
    return $ActiveSearchCopyWith<$Res>(_value.search, (value) {
      return _then(_value.copyWith(search: value));
    });
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$ActiveSearchRequestSucceeded implements ActiveSearchRequestSucceeded {
  const _$ActiveSearchRequestSucceeded(this.search, this.items, {String? $type})
      : $type = $type ?? 'activeSearchRequestSucceeded';

  factory _$ActiveSearchRequestSucceeded.fromJson(Map<String, dynamic> json) =>
      _$$ActiveSearchRequestSucceededFromJson(json);

  @override
  final ActiveSearch search;
  @override
  final List<Document> items;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.activeSearchRequestSucceeded(search: $search, items: $items)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ActiveSearchRequestSucceeded &&
            const DeepCollectionEquality().equals(other.search, search) &&
            const DeepCollectionEquality().equals(other.items, items));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(search),
      const DeepCollectionEquality().hash(items));

  @JsonKey(ignore: true)
  @override
  $ActiveSearchRequestSucceededCopyWith<ActiveSearchRequestSucceeded>
      get copyWith => _$ActiveSearchRequestSucceededCopyWithImpl<
          ActiveSearchRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchRequestSucceeded(search, items);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return activeSearchRequestSucceeded?.call(search, items);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchRequestSucceeded != null) {
      return activeSearchRequestSucceeded(search, items);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return activeSearchRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchRequestSucceeded != null) {
      return activeSearchRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ActiveSearchRequestSucceededToJson(this);
  }
}

abstract class ActiveSearchRequestSucceeded
    implements EngineEvent, SearchEngineEvent {
  const factory ActiveSearchRequestSucceeded(
          ActiveSearch search, List<Document> items) =
      _$ActiveSearchRequestSucceeded;

  factory ActiveSearchRequestSucceeded.fromJson(Map<String, dynamic> json) =
      _$ActiveSearchRequestSucceeded.fromJson;

  ActiveSearch get search;
  List<Document> get items;
  @JsonKey(ignore: true)
  $ActiveSearchRequestSucceededCopyWith<ActiveSearchRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActiveSearchRequestFailedCopyWith<$Res> {
  factory $ActiveSearchRequestFailedCopyWith(ActiveSearchRequestFailed value,
          $Res Function(ActiveSearchRequestFailed) then) =
      _$ActiveSearchRequestFailedCopyWithImpl<$Res>;
  $Res call({SearchFailureReason reason});
}

/// @nodoc
class _$ActiveSearchRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ActiveSearchRequestFailedCopyWith<$Res> {
  _$ActiveSearchRequestFailedCopyWithImpl(ActiveSearchRequestFailed _value,
      $Res Function(ActiveSearchRequestFailed) _then)
      : super(_value, (v) => _then(v as ActiveSearchRequestFailed));

  @override
  ActiveSearchRequestFailed get _value =>
      super._value as ActiveSearchRequestFailed;

  @override
  $Res call({
    Object? reason = freezed,
  }) {
    return _then(ActiveSearchRequestFailed(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as SearchFailureReason,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$ActiveSearchRequestFailed implements ActiveSearchRequestFailed {
  const _$ActiveSearchRequestFailed(this.reason, {String? $type})
      : $type = $type ?? 'activeSearchRequestFailed';

  factory _$ActiveSearchRequestFailed.fromJson(Map<String, dynamic> json) =>
      _$$ActiveSearchRequestFailedFromJson(json);

  @override
  final SearchFailureReason reason;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.activeSearchRequestFailed(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ActiveSearchRequestFailed &&
            const DeepCollectionEquality().equals(other.reason, reason));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(reason));

  @JsonKey(ignore: true)
  @override
  $ActiveSearchRequestFailedCopyWith<ActiveSearchRequestFailed> get copyWith =>
      _$ActiveSearchRequestFailedCopyWithImpl<ActiveSearchRequestFailed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchRequestFailed(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return activeSearchRequestFailed?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchRequestFailed != null) {
      return activeSearchRequestFailed(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return activeSearchRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchRequestFailed != null) {
      return activeSearchRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ActiveSearchRequestFailedToJson(this);
  }
}

abstract class ActiveSearchRequestFailed
    implements EngineEvent, SearchEngineEvent {
  const factory ActiveSearchRequestFailed(SearchFailureReason reason) =
      _$ActiveSearchRequestFailed;

  factory ActiveSearchRequestFailed.fromJson(Map<String, dynamic> json) =
      _$ActiveSearchRequestFailed.fromJson;

  SearchFailureReason get reason;
  @JsonKey(ignore: true)
  $ActiveSearchRequestFailedCopyWith<ActiveSearchRequestFailed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NextActiveSearchBatchRequestSucceededCopyWith<$Res> {
  factory $NextActiveSearchBatchRequestSucceededCopyWith(
          NextActiveSearchBatchRequestSucceeded value,
          $Res Function(NextActiveSearchBatchRequestSucceeded) then) =
      _$NextActiveSearchBatchRequestSucceededCopyWithImpl<$Res>;
  $Res call({ActiveSearch search, List<Document> items});

  $ActiveSearchCopyWith<$Res> get search;
}

/// @nodoc
class _$NextActiveSearchBatchRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $NextActiveSearchBatchRequestSucceededCopyWith<$Res> {
  _$NextActiveSearchBatchRequestSucceededCopyWithImpl(
      NextActiveSearchBatchRequestSucceeded _value,
      $Res Function(NextActiveSearchBatchRequestSucceeded) _then)
      : super(_value, (v) => _then(v as NextActiveSearchBatchRequestSucceeded));

  @override
  NextActiveSearchBatchRequestSucceeded get _value =>
      super._value as NextActiveSearchBatchRequestSucceeded;

  @override
  $Res call({
    Object? search = freezed,
    Object? items = freezed,
  }) {
    return _then(NextActiveSearchBatchRequestSucceeded(
      search == freezed
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as ActiveSearch,
      items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Document>,
    ));
  }

  @override
  $ActiveSearchCopyWith<$Res> get search {
    return $ActiveSearchCopyWith<$Res>(_value.search, (value) {
      return _then(_value.copyWith(search: value));
    });
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$NextActiveSearchBatchRequestSucceeded
    implements NextActiveSearchBatchRequestSucceeded {
  const _$NextActiveSearchBatchRequestSucceeded(this.search, this.items,
      {String? $type})
      : $type = $type ?? 'nextActiveSearchBatchRequestSucceeded';

  factory _$NextActiveSearchBatchRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$NextActiveSearchBatchRequestSucceededFromJson(json);

  @override
  final ActiveSearch search;
  @override
  final List<Document> items;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.nextActiveSearchBatchRequestSucceeded(search: $search, items: $items)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is NextActiveSearchBatchRequestSucceeded &&
            const DeepCollectionEquality().equals(other.search, search) &&
            const DeepCollectionEquality().equals(other.items, items));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(search),
      const DeepCollectionEquality().hash(items));

  @JsonKey(ignore: true)
  @override
  $NextActiveSearchBatchRequestSucceededCopyWith<
          NextActiveSearchBatchRequestSucceeded>
      get copyWith => _$NextActiveSearchBatchRequestSucceededCopyWithImpl<
          NextActiveSearchBatchRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return nextActiveSearchBatchRequestSucceeded(search, items);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return nextActiveSearchBatchRequestSucceeded?.call(search, items);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextActiveSearchBatchRequestSucceeded != null) {
      return nextActiveSearchBatchRequestSucceeded(search, items);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return nextActiveSearchBatchRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return nextActiveSearchBatchRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextActiveSearchBatchRequestSucceeded != null) {
      return nextActiveSearchBatchRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NextActiveSearchBatchRequestSucceededToJson(this);
  }
}

abstract class NextActiveSearchBatchRequestSucceeded
    implements EngineEvent, SearchEngineEvent {
  const factory NextActiveSearchBatchRequestSucceeded(
          ActiveSearch search, List<Document> items) =
      _$NextActiveSearchBatchRequestSucceeded;

  factory NextActiveSearchBatchRequestSucceeded.fromJson(
          Map<String, dynamic> json) =
      _$NextActiveSearchBatchRequestSucceeded.fromJson;

  ActiveSearch get search;
  List<Document> get items;
  @JsonKey(ignore: true)
  $NextActiveSearchBatchRequestSucceededCopyWith<
          NextActiveSearchBatchRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NextActiveSearchBatchRequestFailedCopyWith<$Res> {
  factory $NextActiveSearchBatchRequestFailedCopyWith(
          NextActiveSearchBatchRequestFailed value,
          $Res Function(NextActiveSearchBatchRequestFailed) then) =
      _$NextActiveSearchBatchRequestFailedCopyWithImpl<$Res>;
  $Res call({SearchFailureReason reason});
}

/// @nodoc
class _$NextActiveSearchBatchRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $NextActiveSearchBatchRequestFailedCopyWith<$Res> {
  _$NextActiveSearchBatchRequestFailedCopyWithImpl(
      NextActiveSearchBatchRequestFailed _value,
      $Res Function(NextActiveSearchBatchRequestFailed) _then)
      : super(_value, (v) => _then(v as NextActiveSearchBatchRequestFailed));

  @override
  NextActiveSearchBatchRequestFailed get _value =>
      super._value as NextActiveSearchBatchRequestFailed;

  @override
  $Res call({
    Object? reason = freezed,
  }) {
    return _then(NextActiveSearchBatchRequestFailed(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as SearchFailureReason,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$NextActiveSearchBatchRequestFailed
    implements NextActiveSearchBatchRequestFailed {
  const _$NextActiveSearchBatchRequestFailed(this.reason, {String? $type})
      : $type = $type ?? 'nextActiveSearchBatchRequestFailed';

  factory _$NextActiveSearchBatchRequestFailed.fromJson(
          Map<String, dynamic> json) =>
      _$$NextActiveSearchBatchRequestFailedFromJson(json);

  @override
  final SearchFailureReason reason;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.nextActiveSearchBatchRequestFailed(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is NextActiveSearchBatchRequestFailed &&
            const DeepCollectionEquality().equals(other.reason, reason));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(reason));

  @JsonKey(ignore: true)
  @override
  $NextActiveSearchBatchRequestFailedCopyWith<
          NextActiveSearchBatchRequestFailed>
      get copyWith => _$NextActiveSearchBatchRequestFailedCopyWithImpl<
          NextActiveSearchBatchRequestFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return nextActiveSearchBatchRequestFailed(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return nextActiveSearchBatchRequestFailed?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextActiveSearchBatchRequestFailed != null) {
      return nextActiveSearchBatchRequestFailed(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return nextActiveSearchBatchRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return nextActiveSearchBatchRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (nextActiveSearchBatchRequestFailed != null) {
      return nextActiveSearchBatchRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NextActiveSearchBatchRequestFailedToJson(this);
  }
}

abstract class NextActiveSearchBatchRequestFailed
    implements EngineEvent, SearchEngineEvent {
  const factory NextActiveSearchBatchRequestFailed(SearchFailureReason reason) =
      _$NextActiveSearchBatchRequestFailed;

  factory NextActiveSearchBatchRequestFailed.fromJson(
          Map<String, dynamic> json) =
      _$NextActiveSearchBatchRequestFailed.fromJson;

  SearchFailureReason get reason;
  @JsonKey(ignore: true)
  $NextActiveSearchBatchRequestFailedCopyWith<
          NextActiveSearchBatchRequestFailed>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RestoreActiveSearchSucceededCopyWith<$Res> {
  factory $RestoreActiveSearchSucceededCopyWith(
          RestoreActiveSearchSucceeded value,
          $Res Function(RestoreActiveSearchSucceeded) then) =
      _$RestoreActiveSearchSucceededCopyWithImpl<$Res>;
  $Res call({ActiveSearch search, List<Document> items});

  $ActiveSearchCopyWith<$Res> get search;
}

/// @nodoc
class _$RestoreActiveSearchSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $RestoreActiveSearchSucceededCopyWith<$Res> {
  _$RestoreActiveSearchSucceededCopyWithImpl(
      RestoreActiveSearchSucceeded _value,
      $Res Function(RestoreActiveSearchSucceeded) _then)
      : super(_value, (v) => _then(v as RestoreActiveSearchSucceeded));

  @override
  RestoreActiveSearchSucceeded get _value =>
      super._value as RestoreActiveSearchSucceeded;

  @override
  $Res call({
    Object? search = freezed,
    Object? items = freezed,
  }) {
    return _then(RestoreActiveSearchSucceeded(
      search == freezed
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as ActiveSearch,
      items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Document>,
    ));
  }

  @override
  $ActiveSearchCopyWith<$Res> get search {
    return $ActiveSearchCopyWith<$Res>(_value.search, (value) {
      return _then(_value.copyWith(search: value));
    });
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$RestoreActiveSearchSucceeded implements RestoreActiveSearchSucceeded {
  const _$RestoreActiveSearchSucceeded(this.search, this.items, {String? $type})
      : $type = $type ?? 'restoreActiveSearchSucceeded';

  factory _$RestoreActiveSearchSucceeded.fromJson(Map<String, dynamic> json) =>
      _$$RestoreActiveSearchSucceededFromJson(json);

  @override
  final ActiveSearch search;
  @override
  final List<Document> items;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.restoreActiveSearchSucceeded(search: $search, items: $items)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is RestoreActiveSearchSucceeded &&
            const DeepCollectionEquality().equals(other.search, search) &&
            const DeepCollectionEquality().equals(other.items, items));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(search),
      const DeepCollectionEquality().hash(items));

  @JsonKey(ignore: true)
  @override
  $RestoreActiveSearchSucceededCopyWith<RestoreActiveSearchSucceeded>
      get copyWith => _$RestoreActiveSearchSucceededCopyWithImpl<
          RestoreActiveSearchSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return restoreActiveSearchSucceeded(search, items);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return restoreActiveSearchSucceeded?.call(search, items);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (restoreActiveSearchSucceeded != null) {
      return restoreActiveSearchSucceeded(search, items);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return restoreActiveSearchSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return restoreActiveSearchSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (restoreActiveSearchSucceeded != null) {
      return restoreActiveSearchSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$RestoreActiveSearchSucceededToJson(this);
  }
}

abstract class RestoreActiveSearchSucceeded
    implements EngineEvent, SearchEngineEvent {
  const factory RestoreActiveSearchSucceeded(
          ActiveSearch search, List<Document> items) =
      _$RestoreActiveSearchSucceeded;

  factory RestoreActiveSearchSucceeded.fromJson(Map<String, dynamic> json) =
      _$RestoreActiveSearchSucceeded.fromJson;

  ActiveSearch get search;
  List<Document> get items;
  @JsonKey(ignore: true)
  $RestoreActiveSearchSucceededCopyWith<RestoreActiveSearchSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RestoreActiveSearchFailedCopyWith<$Res> {
  factory $RestoreActiveSearchFailedCopyWith(RestoreActiveSearchFailed value,
          $Res Function(RestoreActiveSearchFailed) then) =
      _$RestoreActiveSearchFailedCopyWithImpl<$Res>;
  $Res call({SearchFailureReason reason});
}

/// @nodoc
class _$RestoreActiveSearchFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $RestoreActiveSearchFailedCopyWith<$Res> {
  _$RestoreActiveSearchFailedCopyWithImpl(RestoreActiveSearchFailed _value,
      $Res Function(RestoreActiveSearchFailed) _then)
      : super(_value, (v) => _then(v as RestoreActiveSearchFailed));

  @override
  RestoreActiveSearchFailed get _value =>
      super._value as RestoreActiveSearchFailed;

  @override
  $Res call({
    Object? reason = freezed,
  }) {
    return _then(RestoreActiveSearchFailed(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as SearchFailureReason,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$RestoreActiveSearchFailed implements RestoreActiveSearchFailed {
  const _$RestoreActiveSearchFailed(this.reason, {String? $type})
      : $type = $type ?? 'restoreActiveSearchFailed';

  factory _$RestoreActiveSearchFailed.fromJson(Map<String, dynamic> json) =>
      _$$RestoreActiveSearchFailedFromJson(json);

  @override
  final SearchFailureReason reason;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.restoreActiveSearchFailed(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is RestoreActiveSearchFailed &&
            const DeepCollectionEquality().equals(other.reason, reason));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(reason));

  @JsonKey(ignore: true)
  @override
  $RestoreActiveSearchFailedCopyWith<RestoreActiveSearchFailed> get copyWith =>
      _$RestoreActiveSearchFailedCopyWithImpl<RestoreActiveSearchFailed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return restoreActiveSearchFailed(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return restoreActiveSearchFailed?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (restoreActiveSearchFailed != null) {
      return restoreActiveSearchFailed(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return restoreActiveSearchFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return restoreActiveSearchFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (restoreActiveSearchFailed != null) {
      return restoreActiveSearchFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$RestoreActiveSearchFailedToJson(this);
  }
}

abstract class RestoreActiveSearchFailed
    implements EngineEvent, SearchEngineEvent {
  const factory RestoreActiveSearchFailed(SearchFailureReason reason) =
      _$RestoreActiveSearchFailed;

  factory RestoreActiveSearchFailed.fromJson(Map<String, dynamic> json) =
      _$RestoreActiveSearchFailed.fromJson;

  SearchFailureReason get reason;
  @JsonKey(ignore: true)
  $RestoreActiveSearchFailedCopyWith<RestoreActiveSearchFailed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActiveSearchTermRequestSucceededCopyWith<$Res> {
  factory $ActiveSearchTermRequestSucceededCopyWith(
          ActiveSearchTermRequestSucceeded value,
          $Res Function(ActiveSearchTermRequestSucceeded) then) =
      _$ActiveSearchTermRequestSucceededCopyWithImpl<$Res>;
  $Res call({String searchTerm});
}

/// @nodoc
class _$ActiveSearchTermRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ActiveSearchTermRequestSucceededCopyWith<$Res> {
  _$ActiveSearchTermRequestSucceededCopyWithImpl(
      ActiveSearchTermRequestSucceeded _value,
      $Res Function(ActiveSearchTermRequestSucceeded) _then)
      : super(_value, (v) => _then(v as ActiveSearchTermRequestSucceeded));

  @override
  ActiveSearchTermRequestSucceeded get _value =>
      super._value as ActiveSearchTermRequestSucceeded;

  @override
  $Res call({
    Object? searchTerm = freezed,
  }) {
    return _then(ActiveSearchTermRequestSucceeded(
      searchTerm == freezed
          ? _value.searchTerm
          : searchTerm // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$ActiveSearchTermRequestSucceeded
    implements ActiveSearchTermRequestSucceeded {
  const _$ActiveSearchTermRequestSucceeded(this.searchTerm, {String? $type})
      : $type = $type ?? 'activeSearchTermRequestSucceeded';

  factory _$ActiveSearchTermRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$ActiveSearchTermRequestSucceededFromJson(json);

  @override
  final String searchTerm;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.activeSearchTermRequestSucceeded(searchTerm: $searchTerm)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ActiveSearchTermRequestSucceeded &&
            const DeepCollectionEquality()
                .equals(other.searchTerm, searchTerm));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(searchTerm));

  @JsonKey(ignore: true)
  @override
  $ActiveSearchTermRequestSucceededCopyWith<ActiveSearchTermRequestSucceeded>
      get copyWith => _$ActiveSearchTermRequestSucceededCopyWithImpl<
          ActiveSearchTermRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchTermRequestSucceeded(searchTerm);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return activeSearchTermRequestSucceeded?.call(searchTerm);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchTermRequestSucceeded != null) {
      return activeSearchTermRequestSucceeded(searchTerm);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchTermRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return activeSearchTermRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchTermRequestSucceeded != null) {
      return activeSearchTermRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ActiveSearchTermRequestSucceededToJson(this);
  }
}

abstract class ActiveSearchTermRequestSucceeded
    implements EngineEvent, SearchEngineEvent {
  const factory ActiveSearchTermRequestSucceeded(String searchTerm) =
      _$ActiveSearchTermRequestSucceeded;

  factory ActiveSearchTermRequestSucceeded.fromJson(Map<String, dynamic> json) =
      _$ActiveSearchTermRequestSucceeded.fromJson;

  String get searchTerm;
  @JsonKey(ignore: true)
  $ActiveSearchTermRequestSucceededCopyWith<ActiveSearchTermRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActiveSearchTermRequestFailedCopyWith<$Res> {
  factory $ActiveSearchTermRequestFailedCopyWith(
          ActiveSearchTermRequestFailed value,
          $Res Function(ActiveSearchTermRequestFailed) then) =
      _$ActiveSearchTermRequestFailedCopyWithImpl<$Res>;
  $Res call({SearchFailureReason reason});
}

/// @nodoc
class _$ActiveSearchTermRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ActiveSearchTermRequestFailedCopyWith<$Res> {
  _$ActiveSearchTermRequestFailedCopyWithImpl(
      ActiveSearchTermRequestFailed _value,
      $Res Function(ActiveSearchTermRequestFailed) _then)
      : super(_value, (v) => _then(v as ActiveSearchTermRequestFailed));

  @override
  ActiveSearchTermRequestFailed get _value =>
      super._value as ActiveSearchTermRequestFailed;

  @override
  $Res call({
    Object? reason = freezed,
  }) {
    return _then(ActiveSearchTermRequestFailed(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as SearchFailureReason,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$ActiveSearchTermRequestFailed implements ActiveSearchTermRequestFailed {
  const _$ActiveSearchTermRequestFailed(this.reason, {String? $type})
      : $type = $type ?? 'activeSearchTermRequestFailed';

  factory _$ActiveSearchTermRequestFailed.fromJson(Map<String, dynamic> json) =>
      _$$ActiveSearchTermRequestFailedFromJson(json);

  @override
  final SearchFailureReason reason;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.activeSearchTermRequestFailed(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ActiveSearchTermRequestFailed &&
            const DeepCollectionEquality().equals(other.reason, reason));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(reason));

  @JsonKey(ignore: true)
  @override
  $ActiveSearchTermRequestFailedCopyWith<ActiveSearchTermRequestFailed>
      get copyWith => _$ActiveSearchTermRequestFailedCopyWithImpl<
          ActiveSearchTermRequestFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchTermRequestFailed(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return activeSearchTermRequestFailed?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchTermRequestFailed != null) {
      return activeSearchTermRequestFailed(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchTermRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return activeSearchTermRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchTermRequestFailed != null) {
      return activeSearchTermRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ActiveSearchTermRequestFailedToJson(this);
  }
}

abstract class ActiveSearchTermRequestFailed
    implements EngineEvent, SearchEngineEvent {
  const factory ActiveSearchTermRequestFailed(SearchFailureReason reason) =
      _$ActiveSearchTermRequestFailed;

  factory ActiveSearchTermRequestFailed.fromJson(Map<String, dynamic> json) =
      _$ActiveSearchTermRequestFailed.fromJson;

  SearchFailureReason get reason;
  @JsonKey(ignore: true)
  $ActiveSearchTermRequestFailedCopyWith<ActiveSearchTermRequestFailed>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActiveSearchClosedSucceededCopyWith<$Res> {
  factory $ActiveSearchClosedSucceededCopyWith(
          ActiveSearchClosedSucceeded value,
          $Res Function(ActiveSearchClosedSucceeded) then) =
      _$ActiveSearchClosedSucceededCopyWithImpl<$Res>;
}

/// @nodoc
class _$ActiveSearchClosedSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ActiveSearchClosedSucceededCopyWith<$Res> {
  _$ActiveSearchClosedSucceededCopyWithImpl(ActiveSearchClosedSucceeded _value,
      $Res Function(ActiveSearchClosedSucceeded) _then)
      : super(_value, (v) => _then(v as ActiveSearchClosedSucceeded));

  @override
  ActiveSearchClosedSucceeded get _value =>
      super._value as ActiveSearchClosedSucceeded;
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$ActiveSearchClosedSucceeded implements ActiveSearchClosedSucceeded {
  const _$ActiveSearchClosedSucceeded({String? $type})
      : $type = $type ?? 'activeSearchClosedSucceeded';

  factory _$ActiveSearchClosedSucceeded.fromJson(Map<String, dynamic> json) =>
      _$$ActiveSearchClosedSucceededFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.activeSearchClosedSucceeded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ActiveSearchClosedSucceeded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchClosedSucceeded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return activeSearchClosedSucceeded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchClosedSucceeded != null) {
      return activeSearchClosedSucceeded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchClosedSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return activeSearchClosedSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchClosedSucceeded != null) {
      return activeSearchClosedSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ActiveSearchClosedSucceededToJson(this);
  }
}

abstract class ActiveSearchClosedSucceeded
    implements EngineEvent, SearchEngineEvent {
  const factory ActiveSearchClosedSucceeded() = _$ActiveSearchClosedSucceeded;

  factory ActiveSearchClosedSucceeded.fromJson(Map<String, dynamic> json) =
      _$ActiveSearchClosedSucceeded.fromJson;
}

/// @nodoc
abstract class $ActiveSearchClosedFailedCopyWith<$Res> {
  factory $ActiveSearchClosedFailedCopyWith(ActiveSearchClosedFailed value,
          $Res Function(ActiveSearchClosedFailed) then) =
      _$ActiveSearchClosedFailedCopyWithImpl<$Res>;
  $Res call({SearchFailureReason reason});
}

/// @nodoc
class _$ActiveSearchClosedFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $ActiveSearchClosedFailedCopyWith<$Res> {
  _$ActiveSearchClosedFailedCopyWithImpl(ActiveSearchClosedFailed _value,
      $Res Function(ActiveSearchClosedFailed) _then)
      : super(_value, (v) => _then(v as ActiveSearchClosedFailed));

  @override
  ActiveSearchClosedFailed get _value =>
      super._value as ActiveSearchClosedFailed;

  @override
  $Res call({
    Object? reason = freezed,
  }) {
    return _then(ActiveSearchClosedFailed(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as SearchFailureReason,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$ActiveSearchClosedFailed implements ActiveSearchClosedFailed {
  const _$ActiveSearchClosedFailed(this.reason, {String? $type})
      : $type = $type ?? 'activeSearchClosedFailed';

  factory _$ActiveSearchClosedFailed.fromJson(Map<String, dynamic> json) =>
      _$$ActiveSearchClosedFailedFromJson(json);

  @override
  final SearchFailureReason reason;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.activeSearchClosedFailed(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ActiveSearchClosedFailed &&
            const DeepCollectionEquality().equals(other.reason, reason));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(reason));

  @JsonKey(ignore: true)
  @override
  $ActiveSearchClosedFailedCopyWith<ActiveSearchClosedFailed> get copyWith =>
      _$ActiveSearchClosedFailedCopyWithImpl<ActiveSearchClosedFailed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchClosedFailed(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return activeSearchClosedFailed?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchClosedFailed != null) {
      return activeSearchClosedFailed(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return activeSearchClosedFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return activeSearchClosedFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (activeSearchClosedFailed != null) {
      return activeSearchClosedFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ActiveSearchClosedFailedToJson(this);
  }
}

abstract class ActiveSearchClosedFailed
    implements EngineEvent, SearchEngineEvent {
  const factory ActiveSearchClosedFailed(SearchFailureReason reason) =
      _$ActiveSearchClosedFailed;

  factory ActiveSearchClosedFailed.fromJson(Map<String, dynamic> json) =
      _$ActiveSearchClosedFailed.fromJson;

  SearchFailureReason get reason;
  @JsonKey(ignore: true)
  $ActiveSearchClosedFailedCopyWith<ActiveSearchClosedFailed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeepSearchRequestSucceededCopyWith<$Res> {
  factory $DeepSearchRequestSucceededCopyWith(DeepSearchRequestSucceeded value,
          $Res Function(DeepSearchRequestSucceeded) then) =
      _$DeepSearchRequestSucceededCopyWithImpl<$Res>;
  $Res call({List<Document> items});
}

/// @nodoc
class _$DeepSearchRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $DeepSearchRequestSucceededCopyWith<$Res> {
  _$DeepSearchRequestSucceededCopyWithImpl(DeepSearchRequestSucceeded _value,
      $Res Function(DeepSearchRequestSucceeded) _then)
      : super(_value, (v) => _then(v as DeepSearchRequestSucceeded));

  @override
  DeepSearchRequestSucceeded get _value =>
      super._value as DeepSearchRequestSucceeded;

  @override
  $Res call({
    Object? items = freezed,
  }) {
    return _then(DeepSearchRequestSucceeded(
      items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Document>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$DeepSearchRequestSucceeded implements DeepSearchRequestSucceeded {
  const _$DeepSearchRequestSucceeded(this.items, {String? $type})
      : $type = $type ?? 'deepSearchRequestSucceeded';

  factory _$DeepSearchRequestSucceeded.fromJson(Map<String, dynamic> json) =>
      _$$DeepSearchRequestSucceededFromJson(json);

  @override
  final List<Document> items;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.deepSearchRequestSucceeded(items: $items)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is DeepSearchRequestSucceeded &&
            const DeepCollectionEquality().equals(other.items, items));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(items));

  @JsonKey(ignore: true)
  @override
  $DeepSearchRequestSucceededCopyWith<DeepSearchRequestSucceeded>
      get copyWith =>
          _$DeepSearchRequestSucceededCopyWithImpl<DeepSearchRequestSucceeded>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return deepSearchRequestSucceeded(items);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return deepSearchRequestSucceeded?.call(items);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (deepSearchRequestSucceeded != null) {
      return deepSearchRequestSucceeded(items);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return deepSearchRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return deepSearchRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (deepSearchRequestSucceeded != null) {
      return deepSearchRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$DeepSearchRequestSucceededToJson(this);
  }
}

abstract class DeepSearchRequestSucceeded
    implements EngineEvent, SearchEngineEvent {
  const factory DeepSearchRequestSucceeded(List<Document> items) =
      _$DeepSearchRequestSucceeded;

  factory DeepSearchRequestSucceeded.fromJson(Map<String, dynamic> json) =
      _$DeepSearchRequestSucceeded.fromJson;

  List<Document> get items;
  @JsonKey(ignore: true)
  $DeepSearchRequestSucceededCopyWith<DeepSearchRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeepSearchRequestFailedCopyWith<$Res> {
  factory $DeepSearchRequestFailedCopyWith(DeepSearchRequestFailed value,
          $Res Function(DeepSearchRequestFailed) then) =
      _$DeepSearchRequestFailedCopyWithImpl<$Res>;
  $Res call({SearchFailureReason reason});
}

/// @nodoc
class _$DeepSearchRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $DeepSearchRequestFailedCopyWith<$Res> {
  _$DeepSearchRequestFailedCopyWithImpl(DeepSearchRequestFailed _value,
      $Res Function(DeepSearchRequestFailed) _then)
      : super(_value, (v) => _then(v as DeepSearchRequestFailed));

  @override
  DeepSearchRequestFailed get _value => super._value as DeepSearchRequestFailed;

  @override
  $Res call({
    Object? reason = freezed,
  }) {
    return _then(DeepSearchRequestFailed(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as SearchFailureReason,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$DeepSearchRequestFailed implements DeepSearchRequestFailed {
  const _$DeepSearchRequestFailed(this.reason, {String? $type})
      : $type = $type ?? 'deepSearchRequestFailed';

  factory _$DeepSearchRequestFailed.fromJson(Map<String, dynamic> json) =>
      _$$DeepSearchRequestFailedFromJson(json);

  @override
  final SearchFailureReason reason;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.deepSearchRequestFailed(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is DeepSearchRequestFailed &&
            const DeepCollectionEquality().equals(other.reason, reason));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(reason));

  @JsonKey(ignore: true)
  @override
  $DeepSearchRequestFailedCopyWith<DeepSearchRequestFailed> get copyWith =>
      _$DeepSearchRequestFailedCopyWithImpl<DeepSearchRequestFailed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return deepSearchRequestFailed(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return deepSearchRequestFailed?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (deepSearchRequestFailed != null) {
      return deepSearchRequestFailed(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return deepSearchRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return deepSearchRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (deepSearchRequestFailed != null) {
      return deepSearchRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$DeepSearchRequestFailedToJson(this);
  }
}

abstract class DeepSearchRequestFailed
    implements EngineEvent, SearchEngineEvent {
  const factory DeepSearchRequestFailed(SearchFailureReason reason) =
      _$DeepSearchRequestFailed;

  factory DeepSearchRequestFailed.fromJson(Map<String, dynamic> json) =
      _$DeepSearchRequestFailed.fromJson;

  SearchFailureReason get reason;
  @JsonKey(ignore: true)
  $DeepSearchRequestFailedCopyWith<DeepSearchRequestFailed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrendingTopicsRequestSucceededCopyWith<$Res> {
  factory $TrendingTopicsRequestSucceededCopyWith(
          TrendingTopicsRequestSucceeded value,
          $Res Function(TrendingTopicsRequestSucceeded) then) =
      _$TrendingTopicsRequestSucceededCopyWithImpl<$Res>;
  $Res call({List<TrendingTopic> topics});
}

/// @nodoc
class _$TrendingTopicsRequestSucceededCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $TrendingTopicsRequestSucceededCopyWith<$Res> {
  _$TrendingTopicsRequestSucceededCopyWithImpl(
      TrendingTopicsRequestSucceeded _value,
      $Res Function(TrendingTopicsRequestSucceeded) _then)
      : super(_value, (v) => _then(v as TrendingTopicsRequestSucceeded));

  @override
  TrendingTopicsRequestSucceeded get _value =>
      super._value as TrendingTopicsRequestSucceeded;

  @override
  $Res call({
    Object? topics = freezed,
  }) {
    return _then(TrendingTopicsRequestSucceeded(
      topics == freezed
          ? _value.topics
          : topics // ignore: cast_nullable_to_non_nullable
              as List<TrendingTopic>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$TrendingTopicsRequestSucceeded
    implements TrendingTopicsRequestSucceeded {
  const _$TrendingTopicsRequestSucceeded(this.topics, {String? $type})
      : $type = $type ?? 'trendingTopicsRequestSucceeded';

  factory _$TrendingTopicsRequestSucceeded.fromJson(
          Map<String, dynamic> json) =>
      _$$TrendingTopicsRequestSucceededFromJson(json);

  @override
  final List<TrendingTopic> topics;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.trendingTopicsRequestSucceeded(topics: $topics)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is TrendingTopicsRequestSucceeded &&
            const DeepCollectionEquality().equals(other.topics, topics));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(topics));

  @JsonKey(ignore: true)
  @override
  $TrendingTopicsRequestSucceededCopyWith<TrendingTopicsRequestSucceeded>
      get copyWith => _$TrendingTopicsRequestSucceededCopyWithImpl<
          TrendingTopicsRequestSucceeded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return trendingTopicsRequestSucceeded(topics);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return trendingTopicsRequestSucceeded?.call(topics);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (trendingTopicsRequestSucceeded != null) {
      return trendingTopicsRequestSucceeded(topics);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return trendingTopicsRequestSucceeded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return trendingTopicsRequestSucceeded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (trendingTopicsRequestSucceeded != null) {
      return trendingTopicsRequestSucceeded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$TrendingTopicsRequestSucceededToJson(this);
  }
}

abstract class TrendingTopicsRequestSucceeded
    implements EngineEvent, SearchEngineEvent {
  const factory TrendingTopicsRequestSucceeded(List<TrendingTopic> topics) =
      _$TrendingTopicsRequestSucceeded;

  factory TrendingTopicsRequestSucceeded.fromJson(Map<String, dynamic> json) =
      _$TrendingTopicsRequestSucceeded.fromJson;

  List<TrendingTopic> get topics;
  @JsonKey(ignore: true)
  $TrendingTopicsRequestSucceededCopyWith<TrendingTopicsRequestSucceeded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrendingTopicsRequestFailedCopyWith<$Res> {
  factory $TrendingTopicsRequestFailedCopyWith(
          TrendingTopicsRequestFailed value,
          $Res Function(TrendingTopicsRequestFailed) then) =
      _$TrendingTopicsRequestFailedCopyWithImpl<$Res>;
  $Res call({SearchFailureReason reason});
}

/// @nodoc
class _$TrendingTopicsRequestFailedCopyWithImpl<$Res>
    extends _$EngineEventCopyWithImpl<$Res>
    implements $TrendingTopicsRequestFailedCopyWith<$Res> {
  _$TrendingTopicsRequestFailedCopyWithImpl(TrendingTopicsRequestFailed _value,
      $Res Function(TrendingTopicsRequestFailed) _then)
      : super(_value, (v) => _then(v as TrendingTopicsRequestFailed));

  @override
  TrendingTopicsRequestFailed get _value =>
      super._value as TrendingTopicsRequestFailed;

  @override
  $Res call({
    Object? reason = freezed,
  }) {
    return _then(TrendingTopicsRequestFailed(
      reason == freezed
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as SearchFailureReason,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchEngineEvent>()
class _$TrendingTopicsRequestFailed implements TrendingTopicsRequestFailed {
  const _$TrendingTopicsRequestFailed(this.reason, {String? $type})
      : $type = $type ?? 'trendingTopicsRequestFailed';

  factory _$TrendingTopicsRequestFailed.fromJson(Map<String, dynamic> json) =>
      _$$TrendingTopicsRequestFailedFromJson(json);

  @override
  final SearchFailureReason reason;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EngineEvent.trendingTopicsRequestFailed(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is TrendingTopicsRequestFailed &&
            const DeepCollectionEquality().equals(other.reason, reason));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(reason));

  @JsonKey(ignore: true)
  @override
  $TrendingTopicsRequestFailedCopyWith<TrendingTopicsRequestFailed>
      get copyWith => _$TrendingTopicsRequestFailedCopyWithImpl<
          TrendingTopicsRequestFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Document> items) restoreFeedSucceeded,
    required TResult Function(FeedFailureReason reason) restoreFeedFailed,
    required TResult Function(List<Document> items)
        nextFeedBatchRequestSucceeded,
    required TResult Function(FeedFailureReason reason, String? errors)
        nextFeedBatchRequestFailed,
    required TResult Function() nextFeedBatchAvailable,
    required TResult Function(Source source) addExcludedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(Source source) addTrustedSourceRequestSucceeded,
    required TResult Function(Source source)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(Set<Source> excludedSources)
        excludedSourcesListRequestSucceeded,
    required TResult Function() excludedSourcesListRequestFailed,
    required TResult Function(Set<Source> sources)
        trustedSourcesListRequestSucceeded,
    required TResult Function() trustedSourcesListRequestFailed,
    required TResult Function(List<AvailableSource> availableSources)
        availableSourcesListRequestSucceeded,
    required TResult Function() availableSourcesListRequestFailed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequestSucceeded,
    required TResult Function(Set<Source> duplicateSources)
        setSourcesRequestFailed,
    required TResult Function() fetchingAssetsStarted,
    required TResult Function(double percentage) fetchingAssetsProgressed,
    required TResult Function() fetchingAssetsFinished,
    required TResult Function() clientEventSucceeded,
    required TResult Function() resetAiSucceeded,
    required TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)
        engineExceptionRaised,
    required TResult Function(List<Document> items) documentsUpdated,
    required TResult Function(ActiveSearch search, List<Document> items)
        activeSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(ActiveSearch search, List<Document> items)
        restoreActiveSearchSucceeded,
    required TResult Function(SearchFailureReason reason)
        restoreActiveSearchFailed,
    required TResult Function(String searchTerm)
        activeSearchTermRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchTermRequestFailed,
    required TResult Function() activeSearchClosedSucceeded,
    required TResult Function(SearchFailureReason reason)
        activeSearchClosedFailed,
    required TResult Function(List<Document> items) deepSearchRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        deepSearchRequestFailed,
    required TResult Function(List<TrendingTopic> topics)
        trendingTopicsRequestSucceeded,
    required TResult Function(SearchFailureReason reason)
        trendingTopicsRequestFailed,
  }) {
    return trendingTopicsRequestFailed(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
  }) {
    return trendingTopicsRequestFailed?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Document> items)? restoreFeedSucceeded,
    TResult Function(FeedFailureReason reason)? restoreFeedFailed,
    TResult Function(List<Document> items)? nextFeedBatchRequestSucceeded,
    TResult Function(FeedFailureReason reason, String? errors)?
        nextFeedBatchRequestFailed,
    TResult Function()? nextFeedBatchAvailable,
    TResult Function(Source source)? addExcludedSourceRequestSucceeded,
    TResult Function(Source source)? removeExcludedSourceRequestSucceeded,
    TResult Function(Source source)? addTrustedSourceRequestSucceeded,
    TResult Function(Source source)? removeTrustedSourceRequestSucceeded,
    TResult Function(Set<Source> excludedSources)?
        excludedSourcesListRequestSucceeded,
    TResult Function()? excludedSourcesListRequestFailed,
    TResult Function(Set<Source> sources)? trustedSourcesListRequestSucceeded,
    TResult Function()? trustedSourcesListRequestFailed,
    TResult Function(List<AvailableSource> availableSources)?
        availableSourcesListRequestSucceeded,
    TResult Function()? availableSourcesListRequestFailed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequestSucceeded,
    TResult Function(Set<Source> duplicateSources)? setSourcesRequestFailed,
    TResult Function()? fetchingAssetsStarted,
    TResult Function(double percentage)? fetchingAssetsProgressed,
    TResult Function()? fetchingAssetsFinished,
    TResult Function()? clientEventSucceeded,
    TResult Function()? resetAiSucceeded,
    TResult Function(
            EngineExceptionReason reason, String? message, String? stackTrace)?
        engineExceptionRaised,
    TResult Function(List<Document> items)? documentsUpdated,
    TResult Function(ActiveSearch search, List<Document> items)?
        activeSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(SearchFailureReason reason)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(ActiveSearch search, List<Document> items)?
        restoreActiveSearchSucceeded,
    TResult Function(SearchFailureReason reason)? restoreActiveSearchFailed,
    TResult Function(String searchTerm)? activeSearchTermRequestSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchTermRequestFailed,
    TResult Function()? activeSearchClosedSucceeded,
    TResult Function(SearchFailureReason reason)? activeSearchClosedFailed,
    TResult Function(List<Document> items)? deepSearchRequestSucceeded,
    TResult Function(SearchFailureReason reason)? deepSearchRequestFailed,
    TResult Function(List<TrendingTopic> topics)?
        trendingTopicsRequestSucceeded,
    TResult Function(SearchFailureReason reason)? trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (trendingTopicsRequestFailed != null) {
      return trendingTopicsRequestFailed(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RestoreFeedSucceeded value) restoreFeedSucceeded,
    required TResult Function(RestoreFeedFailed value) restoreFeedFailed,
    required TResult Function(NextFeedBatchRequestSucceeded value)
        nextFeedBatchRequestSucceeded,
    required TResult Function(NextFeedBatchRequestFailed value)
        nextFeedBatchRequestFailed,
    required TResult Function(NextFeedBatchAvailable value)
        nextFeedBatchAvailable,
    required TResult Function(AddExcludedSourceRequestSucceeded value)
        addExcludedSourceRequestSucceeded,
    required TResult Function(RemoveExcludedSourceRequestSucceeded value)
        removeExcludedSourceRequestSucceeded,
    required TResult Function(AddTrustedSourceRequestSucceeded value)
        addTrustedSourceRequestSucceeded,
    required TResult Function(RemoveTrustedSourceRequestSucceeded value)
        removeTrustedSourceRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestSucceeded value)
        excludedSourcesListRequestSucceeded,
    required TResult Function(ExcludedSourcesListRequestFailed value)
        excludedSourcesListRequestFailed,
    required TResult Function(TrustedSourcesListRequestSucceeded value)
        trustedSourcesListRequestSucceeded,
    required TResult Function(TrustedSourcesListRequestFailed value)
        trustedSourcesListRequestFailed,
    required TResult Function(AvailableSourcesListRequestSucceeded value)
        availableSourcesListRequestSucceeded,
    required TResult Function(AvailableSourcesListRequestFailed value)
        availableSourcesListRequestFailed,
    required TResult Function(SetSourcesRequestSucceeded value)
        setSourcesRequestSucceeded,
    required TResult Function(SetSourcesRequestFailed value)
        setSourcesRequestFailed,
    required TResult Function(FetchingAssetsStarted value)
        fetchingAssetsStarted,
    required TResult Function(FetchingAssetsProgressed value)
        fetchingAssetsProgressed,
    required TResult Function(FetchingAssetsFinished value)
        fetchingAssetsFinished,
    required TResult Function(ClientEventSucceeded value) clientEventSucceeded,
    required TResult Function(ResetAiSucceeded value) resetAiSucceeded,
    required TResult Function(EngineExceptionRaised value)
        engineExceptionRaised,
    required TResult Function(DocumentsUpdated value) documentsUpdated,
    required TResult Function(ActiveSearchRequestSucceeded value)
        activeSearchRequestSucceeded,
    required TResult Function(ActiveSearchRequestFailed value)
        activeSearchRequestFailed,
    required TResult Function(NextActiveSearchBatchRequestSucceeded value)
        nextActiveSearchBatchRequestSucceeded,
    required TResult Function(NextActiveSearchBatchRequestFailed value)
        nextActiveSearchBatchRequestFailed,
    required TResult Function(RestoreActiveSearchSucceeded value)
        restoreActiveSearchSucceeded,
    required TResult Function(RestoreActiveSearchFailed value)
        restoreActiveSearchFailed,
    required TResult Function(ActiveSearchTermRequestSucceeded value)
        activeSearchTermRequestSucceeded,
    required TResult Function(ActiveSearchTermRequestFailed value)
        activeSearchTermRequestFailed,
    required TResult Function(ActiveSearchClosedSucceeded value)
        activeSearchClosedSucceeded,
    required TResult Function(ActiveSearchClosedFailed value)
        activeSearchClosedFailed,
    required TResult Function(DeepSearchRequestSucceeded value)
        deepSearchRequestSucceeded,
    required TResult Function(DeepSearchRequestFailed value)
        deepSearchRequestFailed,
    required TResult Function(TrendingTopicsRequestSucceeded value)
        trendingTopicsRequestSucceeded,
    required TResult Function(TrendingTopicsRequestFailed value)
        trendingTopicsRequestFailed,
  }) {
    return trendingTopicsRequestFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
  }) {
    return trendingTopicsRequestFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RestoreFeedSucceeded value)? restoreFeedSucceeded,
    TResult Function(RestoreFeedFailed value)? restoreFeedFailed,
    TResult Function(NextFeedBatchRequestSucceeded value)?
        nextFeedBatchRequestSucceeded,
    TResult Function(NextFeedBatchRequestFailed value)?
        nextFeedBatchRequestFailed,
    TResult Function(NextFeedBatchAvailable value)? nextFeedBatchAvailable,
    TResult Function(AddExcludedSourceRequestSucceeded value)?
        addExcludedSourceRequestSucceeded,
    TResult Function(RemoveExcludedSourceRequestSucceeded value)?
        removeExcludedSourceRequestSucceeded,
    TResult Function(AddTrustedSourceRequestSucceeded value)?
        addTrustedSourceRequestSucceeded,
    TResult Function(RemoveTrustedSourceRequestSucceeded value)?
        removeTrustedSourceRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestSucceeded value)?
        excludedSourcesListRequestSucceeded,
    TResult Function(ExcludedSourcesListRequestFailed value)?
        excludedSourcesListRequestFailed,
    TResult Function(TrustedSourcesListRequestSucceeded value)?
        trustedSourcesListRequestSucceeded,
    TResult Function(TrustedSourcesListRequestFailed value)?
        trustedSourcesListRequestFailed,
    TResult Function(AvailableSourcesListRequestSucceeded value)?
        availableSourcesListRequestSucceeded,
    TResult Function(AvailableSourcesListRequestFailed value)?
        availableSourcesListRequestFailed,
    TResult Function(SetSourcesRequestSucceeded value)?
        setSourcesRequestSucceeded,
    TResult Function(SetSourcesRequestFailed value)? setSourcesRequestFailed,
    TResult Function(FetchingAssetsStarted value)? fetchingAssetsStarted,
    TResult Function(FetchingAssetsProgressed value)? fetchingAssetsProgressed,
    TResult Function(FetchingAssetsFinished value)? fetchingAssetsFinished,
    TResult Function(ClientEventSucceeded value)? clientEventSucceeded,
    TResult Function(ResetAiSucceeded value)? resetAiSucceeded,
    TResult Function(EngineExceptionRaised value)? engineExceptionRaised,
    TResult Function(DocumentsUpdated value)? documentsUpdated,
    TResult Function(ActiveSearchRequestSucceeded value)?
        activeSearchRequestSucceeded,
    TResult Function(ActiveSearchRequestFailed value)?
        activeSearchRequestFailed,
    TResult Function(NextActiveSearchBatchRequestSucceeded value)?
        nextActiveSearchBatchRequestSucceeded,
    TResult Function(NextActiveSearchBatchRequestFailed value)?
        nextActiveSearchBatchRequestFailed,
    TResult Function(RestoreActiveSearchSucceeded value)?
        restoreActiveSearchSucceeded,
    TResult Function(RestoreActiveSearchFailed value)?
        restoreActiveSearchFailed,
    TResult Function(ActiveSearchTermRequestSucceeded value)?
        activeSearchTermRequestSucceeded,
    TResult Function(ActiveSearchTermRequestFailed value)?
        activeSearchTermRequestFailed,
    TResult Function(ActiveSearchClosedSucceeded value)?
        activeSearchClosedSucceeded,
    TResult Function(ActiveSearchClosedFailed value)? activeSearchClosedFailed,
    TResult Function(DeepSearchRequestSucceeded value)?
        deepSearchRequestSucceeded,
    TResult Function(DeepSearchRequestFailed value)? deepSearchRequestFailed,
    TResult Function(TrendingTopicsRequestSucceeded value)?
        trendingTopicsRequestSucceeded,
    TResult Function(TrendingTopicsRequestFailed value)?
        trendingTopicsRequestFailed,
    required TResult orElse(),
  }) {
    if (trendingTopicsRequestFailed != null) {
      return trendingTopicsRequestFailed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$TrendingTopicsRequestFailedToJson(this);
  }
}

abstract class TrendingTopicsRequestFailed
    implements EngineEvent, SearchEngineEvent {
  const factory TrendingTopicsRequestFailed(SearchFailureReason reason) =
      _$TrendingTopicsRequestFailed;

  factory TrendingTopicsRequestFailed.fromJson(Map<String, dynamic> json) =
      _$TrendingTopicsRequestFailed.fromJson;

  SearchFailureReason get reason;
  @JsonKey(ignore: true)
  $TrendingTopicsRequestFailedCopyWith<TrendingTopicsRequestFailed>
      get copyWith => throw _privateConstructorUsedError;
}
