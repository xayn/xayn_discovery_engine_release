// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client_events.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$Init _$$InitFromJson(Map json) => _$Init(
      Configuration.fromJson(
          Map<String, Object?>.from(json['configuration'] as Map)),
      deConfig: json['deConfig'] as String?,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$InitToJson(_$Init instance) => <String, dynamic>{
      'configuration': instance.configuration.toJson(),
      'deConfig': instance.deConfig,
      'runtimeType': instance.$type,
    };

_$ConfigurationChanged _$$ConfigurationChangedFromJson(Map json) =>
    _$ConfigurationChanged(
      feedMarkets: (json['feedMarkets'] as List<dynamic>?)
          ?.map((e) => FeedMarket.fromJson(Map<String, Object?>.from(e as Map)))
          .toSet(),
      maxItemsPerFeedBatch: json['maxItemsPerFeedBatch'] as int?,
      maxItemsPerSearchBatch: json['maxItemsPerSearchBatch'] as int?,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ConfigurationChangedToJson(
        _$ConfigurationChanged instance) =>
    <String, dynamic>{
      'feedMarkets': instance.feedMarkets?.map((e) => e.toJson()).toList(),
      'maxItemsPerFeedBatch': instance.maxItemsPerFeedBatch,
      'maxItemsPerSearchBatch': instance.maxItemsPerSearchBatch,
      'runtimeType': instance.$type,
    };

_$RestoreFeedRequested _$$RestoreFeedRequestedFromJson(Map json) =>
    _$RestoreFeedRequested(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$RestoreFeedRequestedToJson(
        _$RestoreFeedRequested instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$NextFeedBatchRequested _$$NextFeedBatchRequestedFromJson(Map json) =>
    _$NextFeedBatchRequested(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$NextFeedBatchRequestedToJson(
        _$NextFeedBatchRequested instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$FeedDocumentsClosed _$$FeedDocumentsClosedFromJson(Map json) =>
    _$FeedDocumentsClosed(
      (json['documentIds'] as List<dynamic>)
          .map((e) => DocumentId.fromJson((e as Map).map(
                (k, e) => MapEntry(k as String, e as Object),
              )))
          .toSet(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$FeedDocumentsClosedToJson(
        _$FeedDocumentsClosed instance) =>
    <String, dynamic>{
      'documentIds': instance.documentIds.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$SetSourcesRequested _$$SetSourcesRequestedFromJson(Map json) =>
    _$SetSourcesRequested(
      trustedSources: (json['trustedSources'] as List<dynamic>)
          .map((e) => Source.fromJson(e as Object))
          .toSet(),
      excludedSources: (json['excludedSources'] as List<dynamic>)
          .map((e) => Source.fromJson(e as Object))
          .toSet(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$SetSourcesRequestedToJson(
        _$SetSourcesRequested instance) =>
    <String, dynamic>{
      'trustedSources': instance.trustedSources.map((e) => e.toJson()).toList(),
      'excludedSources':
          instance.excludedSources.map((e) => e.toJson()).toList(),
      'runtimeType': instance.$type,
    };

_$ExcludedSourceAdded _$$ExcludedSourceAddedFromJson(Map json) =>
    _$ExcludedSourceAdded(
      Source.fromJson(json['source'] as Object),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ExcludedSourceAddedToJson(
        _$ExcludedSourceAdded instance) =>
    <String, dynamic>{
      'source': instance.source.toJson(),
      'runtimeType': instance.$type,
    };

_$ExcludedSourceRemoved _$$ExcludedSourceRemovedFromJson(Map json) =>
    _$ExcludedSourceRemoved(
      Source.fromJson(json['source'] as Object),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ExcludedSourceRemovedToJson(
        _$ExcludedSourceRemoved instance) =>
    <String, dynamic>{
      'source': instance.source.toJson(),
      'runtimeType': instance.$type,
    };

_$ExcludedSourcesListRequested _$$ExcludedSourcesListRequestedFromJson(
        Map json) =>
    _$ExcludedSourcesListRequested(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ExcludedSourcesListRequestedToJson(
        _$ExcludedSourcesListRequested instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$TrustedSourceAdded _$$TrustedSourceAddedFromJson(Map json) =>
    _$TrustedSourceAdded(
      Source.fromJson(json['source'] as Object),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$TrustedSourceAddedToJson(
        _$TrustedSourceAdded instance) =>
    <String, dynamic>{
      'source': instance.source.toJson(),
      'runtimeType': instance.$type,
    };

_$TrustedSourceRemoved _$$TrustedSourceRemovedFromJson(Map json) =>
    _$TrustedSourceRemoved(
      Source.fromJson(json['source'] as Object),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$TrustedSourceRemovedToJson(
        _$TrustedSourceRemoved instance) =>
    <String, dynamic>{
      'source': instance.source.toJson(),
      'runtimeType': instance.$type,
    };

_$TrustedSourcesListRequested _$$TrustedSourcesListRequestedFromJson(
        Map json) =>
    _$TrustedSourcesListRequested(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$TrustedSourcesListRequestedToJson(
        _$TrustedSourcesListRequested instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$AvailableSourcesListRequested _$$AvailableSourcesListRequestedFromJson(
        Map json) =>
    _$AvailableSourcesListRequested(
      json['fuzzySearchTerm'] as String,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$AvailableSourcesListRequestedToJson(
        _$AvailableSourcesListRequested instance) =>
    <String, dynamic>{
      'fuzzySearchTerm': instance.fuzzySearchTerm,
      'runtimeType': instance.$type,
    };

_$DocumentTimeSpent _$$DocumentTimeSpentFromJson(Map json) =>
    _$DocumentTimeSpent(
      DocumentId.fromJson((json['documentId'] as Map).map(
        (k, e) => MapEntry(k as String, e as Object),
      )),
      $enumDecode(_$DocumentViewModeEnumMap, json['mode']),
      json['seconds'] as int,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$DocumentTimeSpentToJson(_$DocumentTimeSpent instance) =>
    <String, dynamic>{
      'documentId': instance.documentId.toJson(),
      'mode': _$DocumentViewModeEnumMap[instance.mode]!,
      'seconds': instance.seconds,
      'runtimeType': instance.$type,
    };

const _$DocumentViewModeEnumMap = {
  DocumentViewMode.story: 'story',
  DocumentViewMode.reader: 'reader',
  DocumentViewMode.web: 'web',
};

_$UserReactionChanged _$$UserReactionChangedFromJson(Map json) =>
    _$UserReactionChanged(
      DocumentId.fromJson((json['documentId'] as Map).map(
        (k, e) => MapEntry(k as String, e as Object),
      )),
      $enumDecode(_$UserReactionEnumMap, json['userReaction']),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$UserReactionChangedToJson(
        _$UserReactionChanged instance) =>
    <String, dynamic>{
      'documentId': instance.documentId.toJson(),
      'userReaction': _$UserReactionEnumMap[instance.userReaction]!,
      'runtimeType': instance.$type,
    };

const _$UserReactionEnumMap = {
  UserReaction.neutral: 0,
  UserReaction.positive: 1,
  UserReaction.negative: 2,
};

_$ActiveSearchRequested _$$ActiveSearchRequestedFromJson(Map json) =>
    _$ActiveSearchRequested(
      json['term'] as String,
      $enumDecode(_$SearchByEnumMap, json['searchBy']),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ActiveSearchRequestedToJson(
        _$ActiveSearchRequested instance) =>
    <String, dynamic>{
      'term': instance.term,
      'searchBy': _$SearchByEnumMap[instance.searchBy]!,
      'runtimeType': instance.$type,
    };

const _$SearchByEnumMap = {
  SearchBy.query: 'query',
  SearchBy.topic: 'topic',
};

_$NextActiveSearchBatchRequested _$$NextActiveSearchBatchRequestedFromJson(
        Map json) =>
    _$NextActiveSearchBatchRequested(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$NextActiveSearchBatchRequestedToJson(
        _$NextActiveSearchBatchRequested instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$RestoreActiveSearchRequested _$$RestoreActiveSearchRequestedFromJson(
        Map json) =>
    _$RestoreActiveSearchRequested(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$RestoreActiveSearchRequestedToJson(
        _$RestoreActiveSearchRequested instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$ActiveSearchTermRequested _$$ActiveSearchTermRequestedFromJson(Map json) =>
    _$ActiveSearchTermRequested(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ActiveSearchTermRequestedToJson(
        _$ActiveSearchTermRequested instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$ActiveSearchClosed _$$ActiveSearchClosedFromJson(Map json) =>
    _$ActiveSearchClosed(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ActiveSearchClosedToJson(
        _$ActiveSearchClosed instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$DeepSearchRequested _$$DeepSearchRequestedFromJson(Map json) =>
    _$DeepSearchRequested(
      DocumentId.fromJson((json['id'] as Map).map(
        (k, e) => MapEntry(k as String, e as Object),
      )),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$DeepSearchRequestedToJson(
        _$DeepSearchRequested instance) =>
    <String, dynamic>{
      'id': instance.id.toJson(),
      'runtimeType': instance.$type,
    };

_$TrendingTopicsRequested _$$TrendingTopicsRequestedFromJson(Map json) =>
    _$TrendingTopicsRequested(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$TrendingTopicsRequestedToJson(
        _$TrendingTopicsRequested instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$ResetAiRequested _$$ResetAiRequestedFromJson(Map json) => _$ResetAiRequested(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ResetAiRequestedToJson(_$ResetAiRequested instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };
