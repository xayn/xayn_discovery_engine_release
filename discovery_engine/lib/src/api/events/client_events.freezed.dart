// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'client_events.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ClientEvent _$ClientEventFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType']) {
    case 'init':
      return Init.fromJson(json);
    case 'configurationChanged':
      return ConfigurationChanged.fromJson(json);
    case 'restoreFeedRequested':
      return RestoreFeedRequested.fromJson(json);
    case 'nextFeedBatchRequested':
      return NextFeedBatchRequested.fromJson(json);
    case 'feedDocumentsClosed':
      return FeedDocumentsClosed.fromJson(json);
    case 'setSourcesRequested':
      return SetSourcesRequested.fromJson(json);
    case 'excludedSourceAdded':
      return ExcludedSourceAdded.fromJson(json);
    case 'excludedSourceRemoved':
      return ExcludedSourceRemoved.fromJson(json);
    case 'excludedSourcesListRequested':
      return ExcludedSourcesListRequested.fromJson(json);
    case 'trustedSourceAdded':
      return TrustedSourceAdded.fromJson(json);
    case 'trustedSourceRemoved':
      return TrustedSourceRemoved.fromJson(json);
    case 'trustedSourcesListRequested':
      return TrustedSourcesListRequested.fromJson(json);
    case 'availableSourcesListRequested':
      return AvailableSourcesListRequested.fromJson(json);
    case 'documentTimeSpent':
      return DocumentTimeSpent.fromJson(json);
    case 'userReactionChanged':
      return UserReactionChanged.fromJson(json);
    case 'activeSearchRequested':
      return ActiveSearchRequested.fromJson(json);
    case 'nextActiveSearchBatchRequested':
      return NextActiveSearchBatchRequested.fromJson(json);
    case 'restoreActiveSearchRequested':
      return RestoreActiveSearchRequested.fromJson(json);
    case 'activeSearchTermRequested':
      return ActiveSearchTermRequested.fromJson(json);
    case 'activeSearchClosed':
      return ActiveSearchClosed.fromJson(json);
    case 'deepSearchRequested':
      return DeepSearchRequested.fromJson(json);
    case 'trendingTopicsRequested':
      return TrendingTopicsRequested.fromJson(json);
    case 'resetAi':
      return ResetAiRequested.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'runtimeType', 'ClientEvent',
          'Invalid union type "${json['runtimeType']}"!');
  }
}

/// @nodoc
class _$ClientEventTearOff {
  const _$ClientEventTearOff();

  Init init(Configuration configuration, {String? deConfig}) {
    return Init(
      configuration,
      deConfig: deConfig,
    );
  }

  ConfigurationChanged configurationChanged(
      {Set<FeedMarket>? feedMarkets,
      int? maxItemsPerFeedBatch,
      int? maxItemsPerSearchBatch}) {
    return ConfigurationChanged(
      feedMarkets: feedMarkets,
      maxItemsPerFeedBatch: maxItemsPerFeedBatch,
      maxItemsPerSearchBatch: maxItemsPerSearchBatch,
    );
  }

  RestoreFeedRequested restoreFeedRequested() {
    return const RestoreFeedRequested();
  }

  NextFeedBatchRequested nextFeedBatchRequested() {
    return const NextFeedBatchRequested();
  }

  FeedDocumentsClosed feedDocumentsClosed(Set<DocumentId> documentIds) {
    return FeedDocumentsClosed(
      documentIds,
    );
  }

  SetSourcesRequested setSourcesRequested(
      {required Set<Source> trustedSources,
      required Set<Source> excludedSources}) {
    return SetSourcesRequested(
      trustedSources: trustedSources,
      excludedSources: excludedSources,
    );
  }

  ExcludedSourceAdded excludedSourceAdded(Source source) {
    return ExcludedSourceAdded(
      source,
    );
  }

  ExcludedSourceRemoved excludedSourceRemoved(Source source) {
    return ExcludedSourceRemoved(
      source,
    );
  }

  ExcludedSourcesListRequested excludedSourcesListRequested() {
    return const ExcludedSourcesListRequested();
  }

  TrustedSourceAdded trustedSourceAdded(Source source) {
    return TrustedSourceAdded(
      source,
    );
  }

  TrustedSourceRemoved trustedSourceRemoved(Source source) {
    return TrustedSourceRemoved(
      source,
    );
  }

  TrustedSourcesListRequested trustedSourcesListRequested() {
    return const TrustedSourcesListRequested();
  }

  AvailableSourcesListRequested availableSourcesListRequested(
      String fuzzySearchTerm) {
    return AvailableSourcesListRequested(
      fuzzySearchTerm,
    );
  }

  DocumentTimeSpent documentTimeSpent(
      DocumentId documentId, DocumentViewMode mode, int seconds) {
    return DocumentTimeSpent(
      documentId,
      mode,
      seconds,
    );
  }

  UserReactionChanged userReactionChanged(
      DocumentId documentId, UserReaction userReaction) {
    return UserReactionChanged(
      documentId,
      userReaction,
    );
  }

  ActiveSearchRequested activeSearchRequested(String term, SearchBy searchBy) {
    return ActiveSearchRequested(
      term,
      searchBy,
    );
  }

  NextActiveSearchBatchRequested nextActiveSearchBatchRequested() {
    return const NextActiveSearchBatchRequested();
  }

  RestoreActiveSearchRequested restoreActiveSearchRequested() {
    return const RestoreActiveSearchRequested();
  }

  ActiveSearchTermRequested activeSearchTermRequested() {
    return const ActiveSearchTermRequested();
  }

  ActiveSearchClosed activeSearchClosed() {
    return const ActiveSearchClosed();
  }

  DeepSearchRequested deepSearchRequested(DocumentId id) {
    return DeepSearchRequested(
      id,
    );
  }

  TrendingTopicsRequested trendingTopicsRequested() {
    return const TrendingTopicsRequested();
  }

  ResetAiRequested resetAi() {
    return const ResetAiRequested();
  }

  ClientEvent fromJson(Map<String, Object?> json) {
    return ClientEvent.fromJson(json);
  }
}

/// @nodoc
const $ClientEvent = _$ClientEventTearOff();

/// @nodoc
mixin _$ClientEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ClientEventCopyWith<$Res> {
  factory $ClientEventCopyWith(
          ClientEvent value, $Res Function(ClientEvent) then) =
      _$ClientEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ClientEventCopyWithImpl<$Res> implements $ClientEventCopyWith<$Res> {
  _$ClientEventCopyWithImpl(this._value, this._then);

  final ClientEvent _value;
  // ignore: unused_field
  final $Res Function(ClientEvent) _then;
}

/// @nodoc
abstract class $InitCopyWith<$Res> {
  factory $InitCopyWith(Init value, $Res Function(Init) then) =
      _$InitCopyWithImpl<$Res>;
  $Res call({Configuration configuration, String? deConfig});

  $ConfigurationCopyWith<$Res> get configuration;
}

/// @nodoc
class _$InitCopyWithImpl<$Res> extends _$ClientEventCopyWithImpl<$Res>
    implements $InitCopyWith<$Res> {
  _$InitCopyWithImpl(Init _value, $Res Function(Init) _then)
      : super(_value, (v) => _then(v as Init));

  @override
  Init get _value => super._value as Init;

  @override
  $Res call({
    Object? configuration = freezed,
    Object? deConfig = freezed,
  }) {
    return _then(Init(
      configuration == freezed
          ? _value.configuration
          : configuration // ignore: cast_nullable_to_non_nullable
              as Configuration,
      deConfig: deConfig == freezed
          ? _value.deConfig
          : deConfig // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $ConfigurationCopyWith<$Res> get configuration {
    return $ConfigurationCopyWith<$Res>(_value.configuration, (value) {
      return _then(_value.copyWith(configuration: value));
    });
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SystemClientEvent>()
class _$Init implements Init {
  const _$Init(this.configuration, {this.deConfig, String? $type})
      : assert(deConfig == null || deConfig != ""),
        $type = $type ?? 'init';

  factory _$Init.fromJson(Map<String, dynamic> json) => _$$InitFromJson(json);

  @override
  final Configuration configuration;
  @override
  final String? deConfig;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.init(configuration: $configuration, deConfig: $deConfig)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is Init &&
            const DeepCollectionEquality()
                .equals(other.configuration, configuration) &&
            const DeepCollectionEquality().equals(other.deConfig, deConfig));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(configuration),
      const DeepCollectionEquality().hash(deConfig));

  @JsonKey(ignore: true)
  @override
  $InitCopyWith<Init> get copyWith =>
      _$InitCopyWithImpl<Init>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return init(configuration, deConfig);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return init?.call(configuration, deConfig);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(configuration, deConfig);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$InitToJson(this);
  }
}

abstract class Init implements ClientEvent, SystemClientEvent {
  const factory Init(Configuration configuration, {String? deConfig}) = _$Init;

  factory Init.fromJson(Map<String, dynamic> json) = _$Init.fromJson;

  Configuration get configuration;
  String? get deConfig;
  @JsonKey(ignore: true)
  $InitCopyWith<Init> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ConfigurationChangedCopyWith<$Res> {
  factory $ConfigurationChangedCopyWith(ConfigurationChanged value,
          $Res Function(ConfigurationChanged) then) =
      _$ConfigurationChangedCopyWithImpl<$Res>;
  $Res call(
      {Set<FeedMarket>? feedMarkets,
      int? maxItemsPerFeedBatch,
      int? maxItemsPerSearchBatch});
}

/// @nodoc
class _$ConfigurationChangedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $ConfigurationChangedCopyWith<$Res> {
  _$ConfigurationChangedCopyWithImpl(
      ConfigurationChanged _value, $Res Function(ConfigurationChanged) _then)
      : super(_value, (v) => _then(v as ConfigurationChanged));

  @override
  ConfigurationChanged get _value => super._value as ConfigurationChanged;

  @override
  $Res call({
    Object? feedMarkets = freezed,
    Object? maxItemsPerFeedBatch = freezed,
    Object? maxItemsPerSearchBatch = freezed,
  }) {
    return _then(ConfigurationChanged(
      feedMarkets: feedMarkets == freezed
          ? _value.feedMarkets
          : feedMarkets // ignore: cast_nullable_to_non_nullable
              as Set<FeedMarket>?,
      maxItemsPerFeedBatch: maxItemsPerFeedBatch == freezed
          ? _value.maxItemsPerFeedBatch
          : maxItemsPerFeedBatch // ignore: cast_nullable_to_non_nullable
              as int?,
      maxItemsPerSearchBatch: maxItemsPerSearchBatch == freezed
          ? _value.maxItemsPerSearchBatch
          : maxItemsPerSearchBatch // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SystemClientEvent>()
class _$ConfigurationChanged implements ConfigurationChanged {
  const _$ConfigurationChanged(
      {this.feedMarkets,
      this.maxItemsPerFeedBatch,
      this.maxItemsPerSearchBatch,
      String? $type})
      : assert(feedMarkets == null || feedMarkets.length > 0),
        assert(maxItemsPerFeedBatch == null || maxItemsPerFeedBatch > 0),
        assert(maxItemsPerSearchBatch == null || maxItemsPerSearchBatch > 0),
        $type = $type ?? 'configurationChanged';

  factory _$ConfigurationChanged.fromJson(Map<String, dynamic> json) =>
      _$$ConfigurationChangedFromJson(json);

  @override
  final Set<FeedMarket>? feedMarkets;
  @override
  final int? maxItemsPerFeedBatch;
  @override
  final int? maxItemsPerSearchBatch;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.configurationChanged(feedMarkets: $feedMarkets, maxItemsPerFeedBatch: $maxItemsPerFeedBatch, maxItemsPerSearchBatch: $maxItemsPerSearchBatch)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ConfigurationChanged &&
            const DeepCollectionEquality()
                .equals(other.feedMarkets, feedMarkets) &&
            const DeepCollectionEquality()
                .equals(other.maxItemsPerFeedBatch, maxItemsPerFeedBatch) &&
            const DeepCollectionEquality()
                .equals(other.maxItemsPerSearchBatch, maxItemsPerSearchBatch));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(feedMarkets),
      const DeepCollectionEquality().hash(maxItemsPerFeedBatch),
      const DeepCollectionEquality().hash(maxItemsPerSearchBatch));

  @JsonKey(ignore: true)
  @override
  $ConfigurationChangedCopyWith<ConfigurationChanged> get copyWith =>
      _$ConfigurationChangedCopyWithImpl<ConfigurationChanged>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return configurationChanged(
        feedMarkets, maxItemsPerFeedBatch, maxItemsPerSearchBatch);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return configurationChanged?.call(
        feedMarkets, maxItemsPerFeedBatch, maxItemsPerSearchBatch);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (configurationChanged != null) {
      return configurationChanged(
          feedMarkets, maxItemsPerFeedBatch, maxItemsPerSearchBatch);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return configurationChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return configurationChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (configurationChanged != null) {
      return configurationChanged(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ConfigurationChangedToJson(this);
  }
}

abstract class ConfigurationChanged implements ClientEvent, SystemClientEvent {
  const factory ConfigurationChanged(
      {Set<FeedMarket>? feedMarkets,
      int? maxItemsPerFeedBatch,
      int? maxItemsPerSearchBatch}) = _$ConfigurationChanged;

  factory ConfigurationChanged.fromJson(Map<String, dynamic> json) =
      _$ConfigurationChanged.fromJson;

  Set<FeedMarket>? get feedMarkets;
  int? get maxItemsPerFeedBatch;
  int? get maxItemsPerSearchBatch;
  @JsonKey(ignore: true)
  $ConfigurationChangedCopyWith<ConfigurationChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RestoreFeedRequestedCopyWith<$Res> {
  factory $RestoreFeedRequestedCopyWith(RestoreFeedRequested value,
          $Res Function(RestoreFeedRequested) then) =
      _$RestoreFeedRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$RestoreFeedRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $RestoreFeedRequestedCopyWith<$Res> {
  _$RestoreFeedRequestedCopyWithImpl(
      RestoreFeedRequested _value, $Res Function(RestoreFeedRequested) _then)
      : super(_value, (v) => _then(v as RestoreFeedRequested));

  @override
  RestoreFeedRequested get _value => super._value as RestoreFeedRequested;
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$RestoreFeedRequested implements RestoreFeedRequested {
  const _$RestoreFeedRequested({String? $type})
      : $type = $type ?? 'restoreFeedRequested';

  factory _$RestoreFeedRequested.fromJson(Map<String, dynamic> json) =>
      _$$RestoreFeedRequestedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.restoreFeedRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is RestoreFeedRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return restoreFeedRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return restoreFeedRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (restoreFeedRequested != null) {
      return restoreFeedRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return restoreFeedRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return restoreFeedRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (restoreFeedRequested != null) {
      return restoreFeedRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$RestoreFeedRequestedToJson(this);
  }
}

abstract class RestoreFeedRequested implements ClientEvent, FeedClientEvent {
  const factory RestoreFeedRequested() = _$RestoreFeedRequested;

  factory RestoreFeedRequested.fromJson(Map<String, dynamic> json) =
      _$RestoreFeedRequested.fromJson;
}

/// @nodoc
abstract class $NextFeedBatchRequestedCopyWith<$Res> {
  factory $NextFeedBatchRequestedCopyWith(NextFeedBatchRequested value,
          $Res Function(NextFeedBatchRequested) then) =
      _$NextFeedBatchRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$NextFeedBatchRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $NextFeedBatchRequestedCopyWith<$Res> {
  _$NextFeedBatchRequestedCopyWithImpl(NextFeedBatchRequested _value,
      $Res Function(NextFeedBatchRequested) _then)
      : super(_value, (v) => _then(v as NextFeedBatchRequested));

  @override
  NextFeedBatchRequested get _value => super._value as NextFeedBatchRequested;
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$NextFeedBatchRequested implements NextFeedBatchRequested {
  const _$NextFeedBatchRequested({String? $type})
      : $type = $type ?? 'nextFeedBatchRequested';

  factory _$NextFeedBatchRequested.fromJson(Map<String, dynamic> json) =>
      _$$NextFeedBatchRequestedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.nextFeedBatchRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is NextFeedBatchRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return nextFeedBatchRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return nextFeedBatchRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (nextFeedBatchRequested != null) {
      return nextFeedBatchRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return nextFeedBatchRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return nextFeedBatchRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (nextFeedBatchRequested != null) {
      return nextFeedBatchRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NextFeedBatchRequestedToJson(this);
  }
}

abstract class NextFeedBatchRequested implements ClientEvent, FeedClientEvent {
  const factory NextFeedBatchRequested() = _$NextFeedBatchRequested;

  factory NextFeedBatchRequested.fromJson(Map<String, dynamic> json) =
      _$NextFeedBatchRequested.fromJson;
}

/// @nodoc
abstract class $FeedDocumentsClosedCopyWith<$Res> {
  factory $FeedDocumentsClosedCopyWith(
          FeedDocumentsClosed value, $Res Function(FeedDocumentsClosed) then) =
      _$FeedDocumentsClosedCopyWithImpl<$Res>;
  $Res call({Set<DocumentId> documentIds});
}

/// @nodoc
class _$FeedDocumentsClosedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $FeedDocumentsClosedCopyWith<$Res> {
  _$FeedDocumentsClosedCopyWithImpl(
      FeedDocumentsClosed _value, $Res Function(FeedDocumentsClosed) _then)
      : super(_value, (v) => _then(v as FeedDocumentsClosed));

  @override
  FeedDocumentsClosed get _value => super._value as FeedDocumentsClosed;

  @override
  $Res call({
    Object? documentIds = freezed,
  }) {
    return _then(FeedDocumentsClosed(
      documentIds == freezed
          ? _value.documentIds
          : documentIds // ignore: cast_nullable_to_non_nullable
              as Set<DocumentId>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$FeedDocumentsClosed implements FeedDocumentsClosed {
  const _$FeedDocumentsClosed(this.documentIds, {String? $type})
      : $type = $type ?? 'feedDocumentsClosed';

  factory _$FeedDocumentsClosed.fromJson(Map<String, dynamic> json) =>
      _$$FeedDocumentsClosedFromJson(json);

  @override
  final Set<DocumentId> documentIds;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.feedDocumentsClosed(documentIds: $documentIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FeedDocumentsClosed &&
            const DeepCollectionEquality()
                .equals(other.documentIds, documentIds));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(documentIds));

  @JsonKey(ignore: true)
  @override
  $FeedDocumentsClosedCopyWith<FeedDocumentsClosed> get copyWith =>
      _$FeedDocumentsClosedCopyWithImpl<FeedDocumentsClosed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return feedDocumentsClosed(documentIds);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return feedDocumentsClosed?.call(documentIds);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (feedDocumentsClosed != null) {
      return feedDocumentsClosed(documentIds);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return feedDocumentsClosed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return feedDocumentsClosed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (feedDocumentsClosed != null) {
      return feedDocumentsClosed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$FeedDocumentsClosedToJson(this);
  }
}

abstract class FeedDocumentsClosed implements ClientEvent, FeedClientEvent {
  const factory FeedDocumentsClosed(Set<DocumentId> documentIds) =
      _$FeedDocumentsClosed;

  factory FeedDocumentsClosed.fromJson(Map<String, dynamic> json) =
      _$FeedDocumentsClosed.fromJson;

  Set<DocumentId> get documentIds;
  @JsonKey(ignore: true)
  $FeedDocumentsClosedCopyWith<FeedDocumentsClosed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SetSourcesRequestedCopyWith<$Res> {
  factory $SetSourcesRequestedCopyWith(
          SetSourcesRequested value, $Res Function(SetSourcesRequested) then) =
      _$SetSourcesRequestedCopyWithImpl<$Res>;
  $Res call({Set<Source> trustedSources, Set<Source> excludedSources});
}

/// @nodoc
class _$SetSourcesRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $SetSourcesRequestedCopyWith<$Res> {
  _$SetSourcesRequestedCopyWithImpl(
      SetSourcesRequested _value, $Res Function(SetSourcesRequested) _then)
      : super(_value, (v) => _then(v as SetSourcesRequested));

  @override
  SetSourcesRequested get _value => super._value as SetSourcesRequested;

  @override
  $Res call({
    Object? trustedSources = freezed,
    Object? excludedSources = freezed,
  }) {
    return _then(SetSourcesRequested(
      trustedSources: trustedSources == freezed
          ? _value.trustedSources
          : trustedSources // ignore: cast_nullable_to_non_nullable
              as Set<Source>,
      excludedSources: excludedSources == freezed
          ? _value.excludedSources
          : excludedSources // ignore: cast_nullable_to_non_nullable
              as Set<Source>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$SetSourcesRequested implements SetSourcesRequested {
  const _$SetSourcesRequested(
      {required this.trustedSources,
      required this.excludedSources,
      String? $type})
      : $type = $type ?? 'setSourcesRequested';

  factory _$SetSourcesRequested.fromJson(Map<String, dynamic> json) =>
      _$$SetSourcesRequestedFromJson(json);

  @override
  final Set<Source> trustedSources;
  @override
  final Set<Source> excludedSources;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.setSourcesRequested(trustedSources: $trustedSources, excludedSources: $excludedSources)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is SetSourcesRequested &&
            const DeepCollectionEquality()
                .equals(other.trustedSources, trustedSources) &&
            const DeepCollectionEquality()
                .equals(other.excludedSources, excludedSources));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(trustedSources),
      const DeepCollectionEquality().hash(excludedSources));

  @JsonKey(ignore: true)
  @override
  $SetSourcesRequestedCopyWith<SetSourcesRequested> get copyWith =>
      _$SetSourcesRequestedCopyWithImpl<SetSourcesRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return setSourcesRequested(trustedSources, excludedSources);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return setSourcesRequested?.call(trustedSources, excludedSources);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (setSourcesRequested != null) {
      return setSourcesRequested(trustedSources, excludedSources);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return setSourcesRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return setSourcesRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (setSourcesRequested != null) {
      return setSourcesRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$SetSourcesRequestedToJson(this);
  }
}

abstract class SetSourcesRequested implements ClientEvent, FeedClientEvent {
  const factory SetSourcesRequested(
      {required Set<Source> trustedSources,
      required Set<Source> excludedSources}) = _$SetSourcesRequested;

  factory SetSourcesRequested.fromJson(Map<String, dynamic> json) =
      _$SetSourcesRequested.fromJson;

  Set<Source> get trustedSources;
  Set<Source> get excludedSources;
  @JsonKey(ignore: true)
  $SetSourcesRequestedCopyWith<SetSourcesRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExcludedSourceAddedCopyWith<$Res> {
  factory $ExcludedSourceAddedCopyWith(
          ExcludedSourceAdded value, $Res Function(ExcludedSourceAdded) then) =
      _$ExcludedSourceAddedCopyWithImpl<$Res>;
  $Res call({Source source});
}

/// @nodoc
class _$ExcludedSourceAddedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $ExcludedSourceAddedCopyWith<$Res> {
  _$ExcludedSourceAddedCopyWithImpl(
      ExcludedSourceAdded _value, $Res Function(ExcludedSourceAdded) _then)
      : super(_value, (v) => _then(v as ExcludedSourceAdded));

  @override
  ExcludedSourceAdded get _value => super._value as ExcludedSourceAdded;

  @override
  $Res call({
    Object? source = freezed,
  }) {
    return _then(ExcludedSourceAdded(
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as Source,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$ExcludedSourceAdded implements ExcludedSourceAdded {
  const _$ExcludedSourceAdded(this.source, {String? $type})
      : $type = $type ?? 'excludedSourceAdded';

  factory _$ExcludedSourceAdded.fromJson(Map<String, dynamic> json) =>
      _$$ExcludedSourceAddedFromJson(json);

  @override
  final Source source;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.excludedSourceAdded(source: $source)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ExcludedSourceAdded &&
            const DeepCollectionEquality().equals(other.source, source));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(source));

  @JsonKey(ignore: true)
  @override
  $ExcludedSourceAddedCopyWith<ExcludedSourceAdded> get copyWith =>
      _$ExcludedSourceAddedCopyWithImpl<ExcludedSourceAdded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return excludedSourceAdded(source);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return excludedSourceAdded?.call(source);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (excludedSourceAdded != null) {
      return excludedSourceAdded(source);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return excludedSourceAdded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return excludedSourceAdded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (excludedSourceAdded != null) {
      return excludedSourceAdded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ExcludedSourceAddedToJson(this);
  }
}

abstract class ExcludedSourceAdded implements ClientEvent, FeedClientEvent {
  const factory ExcludedSourceAdded(Source source) = _$ExcludedSourceAdded;

  factory ExcludedSourceAdded.fromJson(Map<String, dynamic> json) =
      _$ExcludedSourceAdded.fromJson;

  Source get source;
  @JsonKey(ignore: true)
  $ExcludedSourceAddedCopyWith<ExcludedSourceAdded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExcludedSourceRemovedCopyWith<$Res> {
  factory $ExcludedSourceRemovedCopyWith(ExcludedSourceRemoved value,
          $Res Function(ExcludedSourceRemoved) then) =
      _$ExcludedSourceRemovedCopyWithImpl<$Res>;
  $Res call({Source source});
}

/// @nodoc
class _$ExcludedSourceRemovedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $ExcludedSourceRemovedCopyWith<$Res> {
  _$ExcludedSourceRemovedCopyWithImpl(
      ExcludedSourceRemoved _value, $Res Function(ExcludedSourceRemoved) _then)
      : super(_value, (v) => _then(v as ExcludedSourceRemoved));

  @override
  ExcludedSourceRemoved get _value => super._value as ExcludedSourceRemoved;

  @override
  $Res call({
    Object? source = freezed,
  }) {
    return _then(ExcludedSourceRemoved(
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as Source,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$ExcludedSourceRemoved implements ExcludedSourceRemoved {
  const _$ExcludedSourceRemoved(this.source, {String? $type})
      : $type = $type ?? 'excludedSourceRemoved';

  factory _$ExcludedSourceRemoved.fromJson(Map<String, dynamic> json) =>
      _$$ExcludedSourceRemovedFromJson(json);

  @override
  final Source source;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.excludedSourceRemoved(source: $source)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ExcludedSourceRemoved &&
            const DeepCollectionEquality().equals(other.source, source));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(source));

  @JsonKey(ignore: true)
  @override
  $ExcludedSourceRemovedCopyWith<ExcludedSourceRemoved> get copyWith =>
      _$ExcludedSourceRemovedCopyWithImpl<ExcludedSourceRemoved>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return excludedSourceRemoved(source);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return excludedSourceRemoved?.call(source);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (excludedSourceRemoved != null) {
      return excludedSourceRemoved(source);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return excludedSourceRemoved(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return excludedSourceRemoved?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (excludedSourceRemoved != null) {
      return excludedSourceRemoved(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ExcludedSourceRemovedToJson(this);
  }
}

abstract class ExcludedSourceRemoved implements ClientEvent, FeedClientEvent {
  const factory ExcludedSourceRemoved(Source source) = _$ExcludedSourceRemoved;

  factory ExcludedSourceRemoved.fromJson(Map<String, dynamic> json) =
      _$ExcludedSourceRemoved.fromJson;

  Source get source;
  @JsonKey(ignore: true)
  $ExcludedSourceRemovedCopyWith<ExcludedSourceRemoved> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExcludedSourcesListRequestedCopyWith<$Res> {
  factory $ExcludedSourcesListRequestedCopyWith(
          ExcludedSourcesListRequested value,
          $Res Function(ExcludedSourcesListRequested) then) =
      _$ExcludedSourcesListRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$ExcludedSourcesListRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $ExcludedSourcesListRequestedCopyWith<$Res> {
  _$ExcludedSourcesListRequestedCopyWithImpl(
      ExcludedSourcesListRequested _value,
      $Res Function(ExcludedSourcesListRequested) _then)
      : super(_value, (v) => _then(v as ExcludedSourcesListRequested));

  @override
  ExcludedSourcesListRequested get _value =>
      super._value as ExcludedSourcesListRequested;
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$ExcludedSourcesListRequested implements ExcludedSourcesListRequested {
  const _$ExcludedSourcesListRequested({String? $type})
      : $type = $type ?? 'excludedSourcesListRequested';

  factory _$ExcludedSourcesListRequested.fromJson(Map<String, dynamic> json) =>
      _$$ExcludedSourcesListRequestedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.excludedSourcesListRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ExcludedSourcesListRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return excludedSourcesListRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return excludedSourcesListRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (excludedSourcesListRequested != null) {
      return excludedSourcesListRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return excludedSourcesListRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return excludedSourcesListRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (excludedSourcesListRequested != null) {
      return excludedSourcesListRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ExcludedSourcesListRequestedToJson(this);
  }
}

abstract class ExcludedSourcesListRequested
    implements ClientEvent, FeedClientEvent {
  const factory ExcludedSourcesListRequested() = _$ExcludedSourcesListRequested;

  factory ExcludedSourcesListRequested.fromJson(Map<String, dynamic> json) =
      _$ExcludedSourcesListRequested.fromJson;
}

/// @nodoc
abstract class $TrustedSourceAddedCopyWith<$Res> {
  factory $TrustedSourceAddedCopyWith(
          TrustedSourceAdded value, $Res Function(TrustedSourceAdded) then) =
      _$TrustedSourceAddedCopyWithImpl<$Res>;
  $Res call({Source source});
}

/// @nodoc
class _$TrustedSourceAddedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $TrustedSourceAddedCopyWith<$Res> {
  _$TrustedSourceAddedCopyWithImpl(
      TrustedSourceAdded _value, $Res Function(TrustedSourceAdded) _then)
      : super(_value, (v) => _then(v as TrustedSourceAdded));

  @override
  TrustedSourceAdded get _value => super._value as TrustedSourceAdded;

  @override
  $Res call({
    Object? source = freezed,
  }) {
    return _then(TrustedSourceAdded(
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as Source,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$TrustedSourceAdded implements TrustedSourceAdded {
  const _$TrustedSourceAdded(this.source, {String? $type})
      : $type = $type ?? 'trustedSourceAdded';

  factory _$TrustedSourceAdded.fromJson(Map<String, dynamic> json) =>
      _$$TrustedSourceAddedFromJson(json);

  @override
  final Source source;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.trustedSourceAdded(source: $source)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is TrustedSourceAdded &&
            const DeepCollectionEquality().equals(other.source, source));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(source));

  @JsonKey(ignore: true)
  @override
  $TrustedSourceAddedCopyWith<TrustedSourceAdded> get copyWith =>
      _$TrustedSourceAddedCopyWithImpl<TrustedSourceAdded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return trustedSourceAdded(source);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return trustedSourceAdded?.call(source);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (trustedSourceAdded != null) {
      return trustedSourceAdded(source);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return trustedSourceAdded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return trustedSourceAdded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (trustedSourceAdded != null) {
      return trustedSourceAdded(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$TrustedSourceAddedToJson(this);
  }
}

abstract class TrustedSourceAdded implements ClientEvent, FeedClientEvent {
  const factory TrustedSourceAdded(Source source) = _$TrustedSourceAdded;

  factory TrustedSourceAdded.fromJson(Map<String, dynamic> json) =
      _$TrustedSourceAdded.fromJson;

  Source get source;
  @JsonKey(ignore: true)
  $TrustedSourceAddedCopyWith<TrustedSourceAdded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrustedSourceRemovedCopyWith<$Res> {
  factory $TrustedSourceRemovedCopyWith(TrustedSourceRemoved value,
          $Res Function(TrustedSourceRemoved) then) =
      _$TrustedSourceRemovedCopyWithImpl<$Res>;
  $Res call({Source source});
}

/// @nodoc
class _$TrustedSourceRemovedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $TrustedSourceRemovedCopyWith<$Res> {
  _$TrustedSourceRemovedCopyWithImpl(
      TrustedSourceRemoved _value, $Res Function(TrustedSourceRemoved) _then)
      : super(_value, (v) => _then(v as TrustedSourceRemoved));

  @override
  TrustedSourceRemoved get _value => super._value as TrustedSourceRemoved;

  @override
  $Res call({
    Object? source = freezed,
  }) {
    return _then(TrustedSourceRemoved(
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as Source,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$TrustedSourceRemoved implements TrustedSourceRemoved {
  const _$TrustedSourceRemoved(this.source, {String? $type})
      : $type = $type ?? 'trustedSourceRemoved';

  factory _$TrustedSourceRemoved.fromJson(Map<String, dynamic> json) =>
      _$$TrustedSourceRemovedFromJson(json);

  @override
  final Source source;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.trustedSourceRemoved(source: $source)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is TrustedSourceRemoved &&
            const DeepCollectionEquality().equals(other.source, source));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(source));

  @JsonKey(ignore: true)
  @override
  $TrustedSourceRemovedCopyWith<TrustedSourceRemoved> get copyWith =>
      _$TrustedSourceRemovedCopyWithImpl<TrustedSourceRemoved>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return trustedSourceRemoved(source);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return trustedSourceRemoved?.call(source);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (trustedSourceRemoved != null) {
      return trustedSourceRemoved(source);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return trustedSourceRemoved(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return trustedSourceRemoved?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (trustedSourceRemoved != null) {
      return trustedSourceRemoved(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$TrustedSourceRemovedToJson(this);
  }
}

abstract class TrustedSourceRemoved implements ClientEvent, FeedClientEvent {
  const factory TrustedSourceRemoved(Source source) = _$TrustedSourceRemoved;

  factory TrustedSourceRemoved.fromJson(Map<String, dynamic> json) =
      _$TrustedSourceRemoved.fromJson;

  Source get source;
  @JsonKey(ignore: true)
  $TrustedSourceRemovedCopyWith<TrustedSourceRemoved> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrustedSourcesListRequestedCopyWith<$Res> {
  factory $TrustedSourcesListRequestedCopyWith(
          TrustedSourcesListRequested value,
          $Res Function(TrustedSourcesListRequested) then) =
      _$TrustedSourcesListRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$TrustedSourcesListRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $TrustedSourcesListRequestedCopyWith<$Res> {
  _$TrustedSourcesListRequestedCopyWithImpl(TrustedSourcesListRequested _value,
      $Res Function(TrustedSourcesListRequested) _then)
      : super(_value, (v) => _then(v as TrustedSourcesListRequested));

  @override
  TrustedSourcesListRequested get _value =>
      super._value as TrustedSourcesListRequested;
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$TrustedSourcesListRequested implements TrustedSourcesListRequested {
  const _$TrustedSourcesListRequested({String? $type})
      : $type = $type ?? 'trustedSourcesListRequested';

  factory _$TrustedSourcesListRequested.fromJson(Map<String, dynamic> json) =>
      _$$TrustedSourcesListRequestedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.trustedSourcesListRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is TrustedSourcesListRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return trustedSourcesListRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return trustedSourcesListRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (trustedSourcesListRequested != null) {
      return trustedSourcesListRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return trustedSourcesListRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return trustedSourcesListRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (trustedSourcesListRequested != null) {
      return trustedSourcesListRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$TrustedSourcesListRequestedToJson(this);
  }
}

abstract class TrustedSourcesListRequested
    implements ClientEvent, FeedClientEvent {
  const factory TrustedSourcesListRequested() = _$TrustedSourcesListRequested;

  factory TrustedSourcesListRequested.fromJson(Map<String, dynamic> json) =
      _$TrustedSourcesListRequested.fromJson;
}

/// @nodoc
abstract class $AvailableSourcesListRequestedCopyWith<$Res> {
  factory $AvailableSourcesListRequestedCopyWith(
          AvailableSourcesListRequested value,
          $Res Function(AvailableSourcesListRequested) then) =
      _$AvailableSourcesListRequestedCopyWithImpl<$Res>;
  $Res call({String fuzzySearchTerm});
}

/// @nodoc
class _$AvailableSourcesListRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $AvailableSourcesListRequestedCopyWith<$Res> {
  _$AvailableSourcesListRequestedCopyWithImpl(
      AvailableSourcesListRequested _value,
      $Res Function(AvailableSourcesListRequested) _then)
      : super(_value, (v) => _then(v as AvailableSourcesListRequested));

  @override
  AvailableSourcesListRequested get _value =>
      super._value as AvailableSourcesListRequested;

  @override
  $Res call({
    Object? fuzzySearchTerm = freezed,
  }) {
    return _then(AvailableSourcesListRequested(
      fuzzySearchTerm == freezed
          ? _value.fuzzySearchTerm
          : fuzzySearchTerm // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<FeedClientEvent>()
class _$AvailableSourcesListRequested implements AvailableSourcesListRequested {
  const _$AvailableSourcesListRequested(this.fuzzySearchTerm, {String? $type})
      : $type = $type ?? 'availableSourcesListRequested';

  factory _$AvailableSourcesListRequested.fromJson(Map<String, dynamic> json) =>
      _$$AvailableSourcesListRequestedFromJson(json);

  @override
  final String fuzzySearchTerm;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.availableSourcesListRequested(fuzzySearchTerm: $fuzzySearchTerm)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is AvailableSourcesListRequested &&
            const DeepCollectionEquality()
                .equals(other.fuzzySearchTerm, fuzzySearchTerm));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(fuzzySearchTerm));

  @JsonKey(ignore: true)
  @override
  $AvailableSourcesListRequestedCopyWith<AvailableSourcesListRequested>
      get copyWith => _$AvailableSourcesListRequestedCopyWithImpl<
          AvailableSourcesListRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return availableSourcesListRequested(fuzzySearchTerm);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return availableSourcesListRequested?.call(fuzzySearchTerm);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (availableSourcesListRequested != null) {
      return availableSourcesListRequested(fuzzySearchTerm);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return availableSourcesListRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return availableSourcesListRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (availableSourcesListRequested != null) {
      return availableSourcesListRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$AvailableSourcesListRequestedToJson(this);
  }
}

abstract class AvailableSourcesListRequested
    implements ClientEvent, FeedClientEvent {
  const factory AvailableSourcesListRequested(String fuzzySearchTerm) =
      _$AvailableSourcesListRequested;

  factory AvailableSourcesListRequested.fromJson(Map<String, dynamic> json) =
      _$AvailableSourcesListRequested.fromJson;

  String get fuzzySearchTerm;
  @JsonKey(ignore: true)
  $AvailableSourcesListRequestedCopyWith<AvailableSourcesListRequested>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DocumentTimeSpentCopyWith<$Res> {
  factory $DocumentTimeSpentCopyWith(
          DocumentTimeSpent value, $Res Function(DocumentTimeSpent) then) =
      _$DocumentTimeSpentCopyWithImpl<$Res>;
  $Res call({DocumentId documentId, DocumentViewMode mode, int seconds});
}

/// @nodoc
class _$DocumentTimeSpentCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $DocumentTimeSpentCopyWith<$Res> {
  _$DocumentTimeSpentCopyWithImpl(
      DocumentTimeSpent _value, $Res Function(DocumentTimeSpent) _then)
      : super(_value, (v) => _then(v as DocumentTimeSpent));

  @override
  DocumentTimeSpent get _value => super._value as DocumentTimeSpent;

  @override
  $Res call({
    Object? documentId = freezed,
    Object? mode = freezed,
    Object? seconds = freezed,
  }) {
    return _then(DocumentTimeSpent(
      documentId == freezed
          ? _value.documentId
          : documentId // ignore: cast_nullable_to_non_nullable
              as DocumentId,
      mode == freezed
          ? _value.mode
          : mode // ignore: cast_nullable_to_non_nullable
              as DocumentViewMode,
      seconds == freezed
          ? _value.seconds
          : seconds // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<DocumentClientEvent>()
class _$DocumentTimeSpent implements DocumentTimeSpent {
  const _$DocumentTimeSpent(this.documentId, this.mode, this.seconds,
      {String? $type})
      : assert(seconds >= 0),
        $type = $type ?? 'documentTimeSpent';

  factory _$DocumentTimeSpent.fromJson(Map<String, dynamic> json) =>
      _$$DocumentTimeSpentFromJson(json);

  @override
  final DocumentId documentId;
  @override
  final DocumentViewMode mode;
  @override
  final int seconds;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.documentTimeSpent(documentId: $documentId, mode: $mode, seconds: $seconds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is DocumentTimeSpent &&
            const DeepCollectionEquality()
                .equals(other.documentId, documentId) &&
            const DeepCollectionEquality().equals(other.mode, mode) &&
            const DeepCollectionEquality().equals(other.seconds, seconds));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(documentId),
      const DeepCollectionEquality().hash(mode),
      const DeepCollectionEquality().hash(seconds));

  @JsonKey(ignore: true)
  @override
  $DocumentTimeSpentCopyWith<DocumentTimeSpent> get copyWith =>
      _$DocumentTimeSpentCopyWithImpl<DocumentTimeSpent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return documentTimeSpent(documentId, mode, seconds);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return documentTimeSpent?.call(documentId, mode, seconds);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (documentTimeSpent != null) {
      return documentTimeSpent(documentId, mode, seconds);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return documentTimeSpent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return documentTimeSpent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (documentTimeSpent != null) {
      return documentTimeSpent(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$DocumentTimeSpentToJson(this);
  }
}

abstract class DocumentTimeSpent implements ClientEvent, DocumentClientEvent {
  const factory DocumentTimeSpent(
          DocumentId documentId, DocumentViewMode mode, int seconds) =
      _$DocumentTimeSpent;

  factory DocumentTimeSpent.fromJson(Map<String, dynamic> json) =
      _$DocumentTimeSpent.fromJson;

  DocumentId get documentId;
  DocumentViewMode get mode;
  int get seconds;
  @JsonKey(ignore: true)
  $DocumentTimeSpentCopyWith<DocumentTimeSpent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserReactionChangedCopyWith<$Res> {
  factory $UserReactionChangedCopyWith(
          UserReactionChanged value, $Res Function(UserReactionChanged) then) =
      _$UserReactionChangedCopyWithImpl<$Res>;
  $Res call({DocumentId documentId, UserReaction userReaction});
}

/// @nodoc
class _$UserReactionChangedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $UserReactionChangedCopyWith<$Res> {
  _$UserReactionChangedCopyWithImpl(
      UserReactionChanged _value, $Res Function(UserReactionChanged) _then)
      : super(_value, (v) => _then(v as UserReactionChanged));

  @override
  UserReactionChanged get _value => super._value as UserReactionChanged;

  @override
  $Res call({
    Object? documentId = freezed,
    Object? userReaction = freezed,
  }) {
    return _then(UserReactionChanged(
      documentId == freezed
          ? _value.documentId
          : documentId // ignore: cast_nullable_to_non_nullable
              as DocumentId,
      userReaction == freezed
          ? _value.userReaction
          : userReaction // ignore: cast_nullable_to_non_nullable
              as UserReaction,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<DocumentClientEvent>()
class _$UserReactionChanged implements UserReactionChanged {
  const _$UserReactionChanged(this.documentId, this.userReaction,
      {String? $type})
      : $type = $type ?? 'userReactionChanged';

  factory _$UserReactionChanged.fromJson(Map<String, dynamic> json) =>
      _$$UserReactionChangedFromJson(json);

  @override
  final DocumentId documentId;
  @override
  final UserReaction userReaction;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.userReactionChanged(documentId: $documentId, userReaction: $userReaction)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is UserReactionChanged &&
            const DeepCollectionEquality()
                .equals(other.documentId, documentId) &&
            const DeepCollectionEquality()
                .equals(other.userReaction, userReaction));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(documentId),
      const DeepCollectionEquality().hash(userReaction));

  @JsonKey(ignore: true)
  @override
  $UserReactionChangedCopyWith<UserReactionChanged> get copyWith =>
      _$UserReactionChangedCopyWithImpl<UserReactionChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return userReactionChanged(documentId, userReaction);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return userReactionChanged?.call(documentId, userReaction);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (userReactionChanged != null) {
      return userReactionChanged(documentId, userReaction);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return userReactionChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return userReactionChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (userReactionChanged != null) {
      return userReactionChanged(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$UserReactionChangedToJson(this);
  }
}

abstract class UserReactionChanged implements ClientEvent, DocumentClientEvent {
  const factory UserReactionChanged(
      DocumentId documentId, UserReaction userReaction) = _$UserReactionChanged;

  factory UserReactionChanged.fromJson(Map<String, dynamic> json) =
      _$UserReactionChanged.fromJson;

  DocumentId get documentId;
  UserReaction get userReaction;
  @JsonKey(ignore: true)
  $UserReactionChangedCopyWith<UserReactionChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActiveSearchRequestedCopyWith<$Res> {
  factory $ActiveSearchRequestedCopyWith(ActiveSearchRequested value,
          $Res Function(ActiveSearchRequested) then) =
      _$ActiveSearchRequestedCopyWithImpl<$Res>;
  $Res call({String term, SearchBy searchBy});
}

/// @nodoc
class _$ActiveSearchRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $ActiveSearchRequestedCopyWith<$Res> {
  _$ActiveSearchRequestedCopyWithImpl(
      ActiveSearchRequested _value, $Res Function(ActiveSearchRequested) _then)
      : super(_value, (v) => _then(v as ActiveSearchRequested));

  @override
  ActiveSearchRequested get _value => super._value as ActiveSearchRequested;

  @override
  $Res call({
    Object? term = freezed,
    Object? searchBy = freezed,
  }) {
    return _then(ActiveSearchRequested(
      term == freezed
          ? _value.term
          : term // ignore: cast_nullable_to_non_nullable
              as String,
      searchBy == freezed
          ? _value.searchBy
          : searchBy // ignore: cast_nullable_to_non_nullable
              as SearchBy,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchClientEvent>()
class _$ActiveSearchRequested implements ActiveSearchRequested {
  const _$ActiveSearchRequested(this.term, this.searchBy, {String? $type})
      : assert(term != ""),
        $type = $type ?? 'activeSearchRequested';

  factory _$ActiveSearchRequested.fromJson(Map<String, dynamic> json) =>
      _$$ActiveSearchRequestedFromJson(json);

  @override
  final String term;
  @override
  final SearchBy searchBy;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.activeSearchRequested(term: $term, searchBy: $searchBy)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ActiveSearchRequested &&
            const DeepCollectionEquality().equals(other.term, term) &&
            const DeepCollectionEquality().equals(other.searchBy, searchBy));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(term),
      const DeepCollectionEquality().hash(searchBy));

  @JsonKey(ignore: true)
  @override
  $ActiveSearchRequestedCopyWith<ActiveSearchRequested> get copyWith =>
      _$ActiveSearchRequestedCopyWithImpl<ActiveSearchRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return activeSearchRequested(term, searchBy);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return activeSearchRequested?.call(term, searchBy);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (activeSearchRequested != null) {
      return activeSearchRequested(term, searchBy);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return activeSearchRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return activeSearchRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (activeSearchRequested != null) {
      return activeSearchRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ActiveSearchRequestedToJson(this);
  }
}

abstract class ActiveSearchRequested implements ClientEvent, SearchClientEvent {
  const factory ActiveSearchRequested(String term, SearchBy searchBy) =
      _$ActiveSearchRequested;

  factory ActiveSearchRequested.fromJson(Map<String, dynamic> json) =
      _$ActiveSearchRequested.fromJson;

  String get term;
  SearchBy get searchBy;
  @JsonKey(ignore: true)
  $ActiveSearchRequestedCopyWith<ActiveSearchRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NextActiveSearchBatchRequestedCopyWith<$Res> {
  factory $NextActiveSearchBatchRequestedCopyWith(
          NextActiveSearchBatchRequested value,
          $Res Function(NextActiveSearchBatchRequested) then) =
      _$NextActiveSearchBatchRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$NextActiveSearchBatchRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $NextActiveSearchBatchRequestedCopyWith<$Res> {
  _$NextActiveSearchBatchRequestedCopyWithImpl(
      NextActiveSearchBatchRequested _value,
      $Res Function(NextActiveSearchBatchRequested) _then)
      : super(_value, (v) => _then(v as NextActiveSearchBatchRequested));

  @override
  NextActiveSearchBatchRequested get _value =>
      super._value as NextActiveSearchBatchRequested;
}

/// @nodoc
@JsonSerializable()
@Implements<SearchClientEvent>()
class _$NextActiveSearchBatchRequested
    implements NextActiveSearchBatchRequested {
  const _$NextActiveSearchBatchRequested({String? $type})
      : $type = $type ?? 'nextActiveSearchBatchRequested';

  factory _$NextActiveSearchBatchRequested.fromJson(
          Map<String, dynamic> json) =>
      _$$NextActiveSearchBatchRequestedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.nextActiveSearchBatchRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is NextActiveSearchBatchRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return nextActiveSearchBatchRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return nextActiveSearchBatchRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (nextActiveSearchBatchRequested != null) {
      return nextActiveSearchBatchRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return nextActiveSearchBatchRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return nextActiveSearchBatchRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (nextActiveSearchBatchRequested != null) {
      return nextActiveSearchBatchRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NextActiveSearchBatchRequestedToJson(this);
  }
}

abstract class NextActiveSearchBatchRequested
    implements ClientEvent, SearchClientEvent {
  const factory NextActiveSearchBatchRequested() =
      _$NextActiveSearchBatchRequested;

  factory NextActiveSearchBatchRequested.fromJson(Map<String, dynamic> json) =
      _$NextActiveSearchBatchRequested.fromJson;
}

/// @nodoc
abstract class $RestoreActiveSearchRequestedCopyWith<$Res> {
  factory $RestoreActiveSearchRequestedCopyWith(
          RestoreActiveSearchRequested value,
          $Res Function(RestoreActiveSearchRequested) then) =
      _$RestoreActiveSearchRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$RestoreActiveSearchRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $RestoreActiveSearchRequestedCopyWith<$Res> {
  _$RestoreActiveSearchRequestedCopyWithImpl(
      RestoreActiveSearchRequested _value,
      $Res Function(RestoreActiveSearchRequested) _then)
      : super(_value, (v) => _then(v as RestoreActiveSearchRequested));

  @override
  RestoreActiveSearchRequested get _value =>
      super._value as RestoreActiveSearchRequested;
}

/// @nodoc
@JsonSerializable()
@Implements<SearchClientEvent>()
class _$RestoreActiveSearchRequested implements RestoreActiveSearchRequested {
  const _$RestoreActiveSearchRequested({String? $type})
      : $type = $type ?? 'restoreActiveSearchRequested';

  factory _$RestoreActiveSearchRequested.fromJson(Map<String, dynamic> json) =>
      _$$RestoreActiveSearchRequestedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.restoreActiveSearchRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is RestoreActiveSearchRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return restoreActiveSearchRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return restoreActiveSearchRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (restoreActiveSearchRequested != null) {
      return restoreActiveSearchRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return restoreActiveSearchRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return restoreActiveSearchRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (restoreActiveSearchRequested != null) {
      return restoreActiveSearchRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$RestoreActiveSearchRequestedToJson(this);
  }
}

abstract class RestoreActiveSearchRequested
    implements ClientEvent, SearchClientEvent {
  const factory RestoreActiveSearchRequested() = _$RestoreActiveSearchRequested;

  factory RestoreActiveSearchRequested.fromJson(Map<String, dynamic> json) =
      _$RestoreActiveSearchRequested.fromJson;
}

/// @nodoc
abstract class $ActiveSearchTermRequestedCopyWith<$Res> {
  factory $ActiveSearchTermRequestedCopyWith(ActiveSearchTermRequested value,
          $Res Function(ActiveSearchTermRequested) then) =
      _$ActiveSearchTermRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$ActiveSearchTermRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $ActiveSearchTermRequestedCopyWith<$Res> {
  _$ActiveSearchTermRequestedCopyWithImpl(ActiveSearchTermRequested _value,
      $Res Function(ActiveSearchTermRequested) _then)
      : super(_value, (v) => _then(v as ActiveSearchTermRequested));

  @override
  ActiveSearchTermRequested get _value =>
      super._value as ActiveSearchTermRequested;
}

/// @nodoc
@JsonSerializable()
@Implements<SearchClientEvent>()
class _$ActiveSearchTermRequested implements ActiveSearchTermRequested {
  const _$ActiveSearchTermRequested({String? $type})
      : $type = $type ?? 'activeSearchTermRequested';

  factory _$ActiveSearchTermRequested.fromJson(Map<String, dynamic> json) =>
      _$$ActiveSearchTermRequestedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.activeSearchTermRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ActiveSearchTermRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return activeSearchTermRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return activeSearchTermRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (activeSearchTermRequested != null) {
      return activeSearchTermRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return activeSearchTermRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return activeSearchTermRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (activeSearchTermRequested != null) {
      return activeSearchTermRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ActiveSearchTermRequestedToJson(this);
  }
}

abstract class ActiveSearchTermRequested
    implements ClientEvent, SearchClientEvent {
  const factory ActiveSearchTermRequested() = _$ActiveSearchTermRequested;

  factory ActiveSearchTermRequested.fromJson(Map<String, dynamic> json) =
      _$ActiveSearchTermRequested.fromJson;
}

/// @nodoc
abstract class $ActiveSearchClosedCopyWith<$Res> {
  factory $ActiveSearchClosedCopyWith(
          ActiveSearchClosed value, $Res Function(ActiveSearchClosed) then) =
      _$ActiveSearchClosedCopyWithImpl<$Res>;
}

/// @nodoc
class _$ActiveSearchClosedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $ActiveSearchClosedCopyWith<$Res> {
  _$ActiveSearchClosedCopyWithImpl(
      ActiveSearchClosed _value, $Res Function(ActiveSearchClosed) _then)
      : super(_value, (v) => _then(v as ActiveSearchClosed));

  @override
  ActiveSearchClosed get _value => super._value as ActiveSearchClosed;
}

/// @nodoc
@JsonSerializable()
@Implements<SearchClientEvent>()
class _$ActiveSearchClosed implements ActiveSearchClosed {
  const _$ActiveSearchClosed({String? $type})
      : $type = $type ?? 'activeSearchClosed';

  factory _$ActiveSearchClosed.fromJson(Map<String, dynamic> json) =>
      _$$ActiveSearchClosedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.activeSearchClosed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ActiveSearchClosed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return activeSearchClosed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return activeSearchClosed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (activeSearchClosed != null) {
      return activeSearchClosed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return activeSearchClosed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return activeSearchClosed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (activeSearchClosed != null) {
      return activeSearchClosed(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ActiveSearchClosedToJson(this);
  }
}

abstract class ActiveSearchClosed implements ClientEvent, SearchClientEvent {
  const factory ActiveSearchClosed() = _$ActiveSearchClosed;

  factory ActiveSearchClosed.fromJson(Map<String, dynamic> json) =
      _$ActiveSearchClosed.fromJson;
}

/// @nodoc
abstract class $DeepSearchRequestedCopyWith<$Res> {
  factory $DeepSearchRequestedCopyWith(
          DeepSearchRequested value, $Res Function(DeepSearchRequested) then) =
      _$DeepSearchRequestedCopyWithImpl<$Res>;
  $Res call({DocumentId id});
}

/// @nodoc
class _$DeepSearchRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $DeepSearchRequestedCopyWith<$Res> {
  _$DeepSearchRequestedCopyWithImpl(
      DeepSearchRequested _value, $Res Function(DeepSearchRequested) _then)
      : super(_value, (v) => _then(v as DeepSearchRequested));

  @override
  DeepSearchRequested get _value => super._value as DeepSearchRequested;

  @override
  $Res call({
    Object? id = freezed,
  }) {
    return _then(DeepSearchRequested(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as DocumentId,
    ));
  }
}

/// @nodoc
@JsonSerializable()
@Implements<SearchClientEvent>()
class _$DeepSearchRequested implements DeepSearchRequested {
  const _$DeepSearchRequested(this.id, {String? $type})
      : $type = $type ?? 'deepSearchRequested';

  factory _$DeepSearchRequested.fromJson(Map<String, dynamic> json) =>
      _$$DeepSearchRequestedFromJson(json);

  @override
  final DocumentId id;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.deepSearchRequested(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is DeepSearchRequested &&
            const DeepCollectionEquality().equals(other.id, id));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(id));

  @JsonKey(ignore: true)
  @override
  $DeepSearchRequestedCopyWith<DeepSearchRequested> get copyWith =>
      _$DeepSearchRequestedCopyWithImpl<DeepSearchRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return deepSearchRequested(id);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return deepSearchRequested?.call(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (deepSearchRequested != null) {
      return deepSearchRequested(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return deepSearchRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return deepSearchRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (deepSearchRequested != null) {
      return deepSearchRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$DeepSearchRequestedToJson(this);
  }
}

abstract class DeepSearchRequested implements ClientEvent, SearchClientEvent {
  const factory DeepSearchRequested(DocumentId id) = _$DeepSearchRequested;

  factory DeepSearchRequested.fromJson(Map<String, dynamic> json) =
      _$DeepSearchRequested.fromJson;

  DocumentId get id;
  @JsonKey(ignore: true)
  $DeepSearchRequestedCopyWith<DeepSearchRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrendingTopicsRequestedCopyWith<$Res> {
  factory $TrendingTopicsRequestedCopyWith(TrendingTopicsRequested value,
          $Res Function(TrendingTopicsRequested) then) =
      _$TrendingTopicsRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$TrendingTopicsRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $TrendingTopicsRequestedCopyWith<$Res> {
  _$TrendingTopicsRequestedCopyWithImpl(TrendingTopicsRequested _value,
      $Res Function(TrendingTopicsRequested) _then)
      : super(_value, (v) => _then(v as TrendingTopicsRequested));

  @override
  TrendingTopicsRequested get _value => super._value as TrendingTopicsRequested;
}

/// @nodoc
@JsonSerializable()
@Implements<SearchClientEvent>()
class _$TrendingTopicsRequested implements TrendingTopicsRequested {
  const _$TrendingTopicsRequested({String? $type})
      : $type = $type ?? 'trendingTopicsRequested';

  factory _$TrendingTopicsRequested.fromJson(Map<String, dynamic> json) =>
      _$$TrendingTopicsRequestedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.trendingTopicsRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is TrendingTopicsRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return trendingTopicsRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return trendingTopicsRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (trendingTopicsRequested != null) {
      return trendingTopicsRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return trendingTopicsRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return trendingTopicsRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (trendingTopicsRequested != null) {
      return trendingTopicsRequested(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$TrendingTopicsRequestedToJson(this);
  }
}

abstract class TrendingTopicsRequested
    implements ClientEvent, SearchClientEvent {
  const factory TrendingTopicsRequested() = _$TrendingTopicsRequested;

  factory TrendingTopicsRequested.fromJson(Map<String, dynamic> json) =
      _$TrendingTopicsRequested.fromJson;
}

/// @nodoc
abstract class $ResetAiRequestedCopyWith<$Res> {
  factory $ResetAiRequestedCopyWith(
          ResetAiRequested value, $Res Function(ResetAiRequested) then) =
      _$ResetAiRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class _$ResetAiRequestedCopyWithImpl<$Res>
    extends _$ClientEventCopyWithImpl<$Res>
    implements $ResetAiRequestedCopyWith<$Res> {
  _$ResetAiRequestedCopyWithImpl(
      ResetAiRequested _value, $Res Function(ResetAiRequested) _then)
      : super(_value, (v) => _then(v as ResetAiRequested));

  @override
  ResetAiRequested get _value => super._value as ResetAiRequested;
}

/// @nodoc
@JsonSerializable()
@Implements<SystemClientEvent>()
class _$ResetAiRequested implements ResetAiRequested {
  const _$ResetAiRequested({String? $type}) : $type = $type ?? 'resetAi';

  factory _$ResetAiRequested.fromJson(Map<String, dynamic> json) =>
      _$$ResetAiRequestedFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'ClientEvent.resetAi()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ResetAiRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Configuration configuration, String? deConfig)
        init,
    required TResult Function(Set<FeedMarket>? feedMarkets,
            int? maxItemsPerFeedBatch, int? maxItemsPerSearchBatch)
        configurationChanged,
    required TResult Function() restoreFeedRequested,
    required TResult Function() nextFeedBatchRequested,
    required TResult Function(Set<DocumentId> documentIds) feedDocumentsClosed,
    required TResult Function(
            Set<Source> trustedSources, Set<Source> excludedSources)
        setSourcesRequested,
    required TResult Function(Source source) excludedSourceAdded,
    required TResult Function(Source source) excludedSourceRemoved,
    required TResult Function() excludedSourcesListRequested,
    required TResult Function(Source source) trustedSourceAdded,
    required TResult Function(Source source) trustedSourceRemoved,
    required TResult Function() trustedSourcesListRequested,
    required TResult Function(String fuzzySearchTerm)
        availableSourcesListRequested,
    required TResult Function(
            DocumentId documentId, DocumentViewMode mode, int seconds)
        documentTimeSpent,
    required TResult Function(DocumentId documentId, UserReaction userReaction)
        userReactionChanged,
    required TResult Function(String term, SearchBy searchBy)
        activeSearchRequested,
    required TResult Function() nextActiveSearchBatchRequested,
    required TResult Function() restoreActiveSearchRequested,
    required TResult Function() activeSearchTermRequested,
    required TResult Function() activeSearchClosed,
    required TResult Function(DocumentId id) deepSearchRequested,
    required TResult Function() trendingTopicsRequested,
    required TResult Function() resetAi,
  }) {
    return resetAi();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
  }) {
    return resetAi?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Configuration configuration, String? deConfig)? init,
    TResult Function(Set<FeedMarket>? feedMarkets, int? maxItemsPerFeedBatch,
            int? maxItemsPerSearchBatch)?
        configurationChanged,
    TResult Function()? restoreFeedRequested,
    TResult Function()? nextFeedBatchRequested,
    TResult Function(Set<DocumentId> documentIds)? feedDocumentsClosed,
    TResult Function(Set<Source> trustedSources, Set<Source> excludedSources)?
        setSourcesRequested,
    TResult Function(Source source)? excludedSourceAdded,
    TResult Function(Source source)? excludedSourceRemoved,
    TResult Function()? excludedSourcesListRequested,
    TResult Function(Source source)? trustedSourceAdded,
    TResult Function(Source source)? trustedSourceRemoved,
    TResult Function()? trustedSourcesListRequested,
    TResult Function(String fuzzySearchTerm)? availableSourcesListRequested,
    TResult Function(DocumentId documentId, DocumentViewMode mode, int seconds)?
        documentTimeSpent,
    TResult Function(DocumentId documentId, UserReaction userReaction)?
        userReactionChanged,
    TResult Function(String term, SearchBy searchBy)? activeSearchRequested,
    TResult Function()? nextActiveSearchBatchRequested,
    TResult Function()? restoreActiveSearchRequested,
    TResult Function()? activeSearchTermRequested,
    TResult Function()? activeSearchClosed,
    TResult Function(DocumentId id)? deepSearchRequested,
    TResult Function()? trendingTopicsRequested,
    TResult Function()? resetAi,
    required TResult orElse(),
  }) {
    if (resetAi != null) {
      return resetAi();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Init value) init,
    required TResult Function(ConfigurationChanged value) configurationChanged,
    required TResult Function(RestoreFeedRequested value) restoreFeedRequested,
    required TResult Function(NextFeedBatchRequested value)
        nextFeedBatchRequested,
    required TResult Function(FeedDocumentsClosed value) feedDocumentsClosed,
    required TResult Function(SetSourcesRequested value) setSourcesRequested,
    required TResult Function(ExcludedSourceAdded value) excludedSourceAdded,
    required TResult Function(ExcludedSourceRemoved value)
        excludedSourceRemoved,
    required TResult Function(ExcludedSourcesListRequested value)
        excludedSourcesListRequested,
    required TResult Function(TrustedSourceAdded value) trustedSourceAdded,
    required TResult Function(TrustedSourceRemoved value) trustedSourceRemoved,
    required TResult Function(TrustedSourcesListRequested value)
        trustedSourcesListRequested,
    required TResult Function(AvailableSourcesListRequested value)
        availableSourcesListRequested,
    required TResult Function(DocumentTimeSpent value) documentTimeSpent,
    required TResult Function(UserReactionChanged value) userReactionChanged,
    required TResult Function(ActiveSearchRequested value)
        activeSearchRequested,
    required TResult Function(NextActiveSearchBatchRequested value)
        nextActiveSearchBatchRequested,
    required TResult Function(RestoreActiveSearchRequested value)
        restoreActiveSearchRequested,
    required TResult Function(ActiveSearchTermRequested value)
        activeSearchTermRequested,
    required TResult Function(ActiveSearchClosed value) activeSearchClosed,
    required TResult Function(DeepSearchRequested value) deepSearchRequested,
    required TResult Function(TrendingTopicsRequested value)
        trendingTopicsRequested,
    required TResult Function(ResetAiRequested value) resetAi,
  }) {
    return resetAi(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
  }) {
    return resetAi?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Init value)? init,
    TResult Function(ConfigurationChanged value)? configurationChanged,
    TResult Function(RestoreFeedRequested value)? restoreFeedRequested,
    TResult Function(NextFeedBatchRequested value)? nextFeedBatchRequested,
    TResult Function(FeedDocumentsClosed value)? feedDocumentsClosed,
    TResult Function(SetSourcesRequested value)? setSourcesRequested,
    TResult Function(ExcludedSourceAdded value)? excludedSourceAdded,
    TResult Function(ExcludedSourceRemoved value)? excludedSourceRemoved,
    TResult Function(ExcludedSourcesListRequested value)?
        excludedSourcesListRequested,
    TResult Function(TrustedSourceAdded value)? trustedSourceAdded,
    TResult Function(TrustedSourceRemoved value)? trustedSourceRemoved,
    TResult Function(TrustedSourcesListRequested value)?
        trustedSourcesListRequested,
    TResult Function(AvailableSourcesListRequested value)?
        availableSourcesListRequested,
    TResult Function(DocumentTimeSpent value)? documentTimeSpent,
    TResult Function(UserReactionChanged value)? userReactionChanged,
    TResult Function(ActiveSearchRequested value)? activeSearchRequested,
    TResult Function(NextActiveSearchBatchRequested value)?
        nextActiveSearchBatchRequested,
    TResult Function(RestoreActiveSearchRequested value)?
        restoreActiveSearchRequested,
    TResult Function(ActiveSearchTermRequested value)?
        activeSearchTermRequested,
    TResult Function(ActiveSearchClosed value)? activeSearchClosed,
    TResult Function(DeepSearchRequested value)? deepSearchRequested,
    TResult Function(TrendingTopicsRequested value)? trendingTopicsRequested,
    TResult Function(ResetAiRequested value)? resetAi,
    required TResult orElse(),
  }) {
    if (resetAi != null) {
      return resetAi(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ResetAiRequestedToJson(this);
  }
}

abstract class ResetAiRequested implements ClientEvent, SystemClientEvent {
  const factory ResetAiRequested() = _$ResetAiRequested;

  factory ResetAiRequested.fromJson(Map<String, dynamic> json) =
      _$ResetAiRequested.fromJson;
}
